# jeemes

#### 介绍
jeewms兄弟产品jeemes，已经发布，微服务版本，包含大屏（改造后开源），PDA，WEB。本软件也是经过商用之后开源，软件还有很多不足之处，我们会第一季度优化，第二季度大家可以使用，有什么问题联系私信联系本人，谢谢大家的批评指正，如果想参与贡献直接联系我，开源不易，且行且珍惜，你的star是我们持续前进的动力。

#### 软件架构
底层架构基于jeecg-cloud，详细的技术描述参考jeecg-cloud

技术体系

1、Nacos 服务注册和发现
2、Nacos 统一配置中心
3、熔断降级限流 sentinel
4、feign配合sentinel使用
5、SpringCloud Gateway
6、服务监控 actuator
7、Shiro+Jwt 权限控制
8、分布式文件系统 minio、阿里OSS
9、Spring Boot Admin服务监控
10、链路跟踪 skywalking

技术架构

基础框架：Spring Boot 2.1.3.RELEASE
Spring Cloud Greenwich.SR3
Spring Cloud Alibaba 2.1.0.RELEASE
持久层框架：Mybatis-plus_3.1.2
安全框架：Apache Shiro 1.4.0，Jwt_3.7.0
数据库连接池：阿里巴巴Druid 1.1.10
缓存框架：redis
日志打印：logback
其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。

开发环境

语言：Java 8
IDE： IDEA 或 Eclipse （安装lombok插件）
依赖管理：Maven
数据库：MySQL5.7 & Oracle 11g
缓存：Redis

组织管理：工厂管理，仓库管理，公司管理，组织架构管理。
主数据管理：物料主数据，，客户主数据，供应商主数据，价格主数据，工艺主数据，BOM主数据，设备主数据，检验主数据，员工主数据，工序标准价格和工时，规则管理。
仓库管理：入库管理，出库管理，盘点管理，调拨管理，湿敏零件管理，锡膏红胶管理。
品质管理：来料检验，首件检测，巡检记录，开转线管理，出货检验，良率管理，不良品管理。
生产管理：SMT防错，指令单，生产追溯，异常查询，智能转产，ATP（齐套和缺料），在线料表，订单预留管理，工艺文件。
标签打印：物料标签（ReelID），设备标签，人员标签，打印模板管理。
订单管理：采购订单，生产订单，销售订单，销售发货。

系统目标


闭环式SMT制造解決方案，在SMT制造产业中,以智能制造为核心理念，提供一系列独特的管理方式和管理工具，提高生产效率、提高产
品品质、缩短生产周期、降低制造成本、全面防错，实现全面科学的可追溯管理。


开发计划

计划引入APS排程




#### 安装教程

1.  启动nacos
2.  启动minio
3.  依次启动java，vue，uniapp
4.  详细的启动步骤参考各个子项目的启动步骤

#### 使用说明

1.  适合与SMT行业
2.  离散制造的待开源
3.  快消行业MES正在实施中，期待2023

![输入图片说明](%E5%9B%BE%E7%89%871.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request





