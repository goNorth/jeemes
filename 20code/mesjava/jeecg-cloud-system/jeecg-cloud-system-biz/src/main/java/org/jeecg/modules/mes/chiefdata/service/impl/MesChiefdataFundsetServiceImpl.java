package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataFundset;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataFundsetMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataFundsetService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—费用设置
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataFundsetServiceImpl extends ServiceImpl<MesChiefdataFundsetMapper, MesChiefdataFundset> implements IMesChiefdataFundsetService {

}
