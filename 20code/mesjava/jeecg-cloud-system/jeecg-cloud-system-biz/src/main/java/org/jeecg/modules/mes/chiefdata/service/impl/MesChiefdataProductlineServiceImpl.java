package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataProductline;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataProductlineMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataProductlineService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—产线
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataProductlineServiceImpl extends ServiceImpl<MesChiefdataProductlineMapper, MesChiefdataProductline> implements IMesChiefdataProductlineService {

}
