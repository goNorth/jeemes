package org.jeecg.modules.mes.organize.mapper;

import java.util.List;
import org.jeecg.modules.mes.organize.entity.MesOrganizeHoliday;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 组织—日历（假期）
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface MesOrganizeHolidayMapper extends BaseMapper<MesOrganizeHoliday> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesOrganizeHoliday> selectByMainId(@Param("mainId") String mainId);

}
