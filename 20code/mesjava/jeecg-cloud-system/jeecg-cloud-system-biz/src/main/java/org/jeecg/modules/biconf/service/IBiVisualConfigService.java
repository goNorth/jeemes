package org.jeecg.modules.biconf.service;

import org.jeecg.modules.biconf.entity.BiVisualConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 大屏页面配置数据
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
public interface IBiVisualConfigService extends IService<BiVisualConfig> {

}
