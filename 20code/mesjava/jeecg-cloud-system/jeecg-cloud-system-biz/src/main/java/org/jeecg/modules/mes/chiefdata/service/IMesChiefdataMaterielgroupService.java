package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMaterielgroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—物料组
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataMaterielgroupService extends IService<MesChiefdataMaterielgroup> {

}
