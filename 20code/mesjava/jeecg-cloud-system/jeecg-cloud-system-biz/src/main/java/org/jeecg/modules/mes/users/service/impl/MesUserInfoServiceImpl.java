package org.jeecg.modules.mes.users.service.impl;

import org.jeecg.modules.mes.users.entity.MesUserInfo;
import org.jeecg.modules.mes.users.mapper.MesUserInfoMapper;
import org.jeecg.modules.mes.users.service.IMesUserInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 用户管理
 * @Author: jeecg-boot
 * @Date: 2021-02-24
 * @Version: V1.0
 */
@Service
public class MesUserInfoServiceImpl extends ServiceImpl<MesUserInfoMapper, MesUserInfo> implements IMesUserInfoService {

}
