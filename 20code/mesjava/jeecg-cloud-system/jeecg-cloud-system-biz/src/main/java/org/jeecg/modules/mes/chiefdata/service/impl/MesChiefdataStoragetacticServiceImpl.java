package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataStoragetactic;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataStoragetacticMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataStoragetacticService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—存储策略
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataStoragetacticServiceImpl extends ServiceImpl<MesChiefdataStoragetacticMapper, MesChiefdataStoragetactic> implements IMesChiefdataStoragetacticService {

}
