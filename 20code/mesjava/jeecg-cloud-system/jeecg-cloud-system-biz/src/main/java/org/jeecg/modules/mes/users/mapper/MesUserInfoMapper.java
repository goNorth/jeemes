package org.jeecg.modules.mes.users.mapper;

import org.jeecg.modules.mes.users.entity.MesUserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 用户管理
 * @Author: jeecg-boot
 * @Date: 2021-02-24
 * @Version: V1.0
 */
public interface MesUserInfoMapper extends BaseMapper<MesUserInfo> {

}
