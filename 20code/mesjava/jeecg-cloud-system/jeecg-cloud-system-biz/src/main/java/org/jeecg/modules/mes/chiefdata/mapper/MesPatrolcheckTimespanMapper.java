package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesPatrolcheckTimespan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 主数据—巡检方案时段
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface MesPatrolcheckTimespanMapper extends BaseMapper<MesPatrolcheckTimespan> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesPatrolcheckTimespan> selectByMainId(@Param("mainId") String mainId);
}
