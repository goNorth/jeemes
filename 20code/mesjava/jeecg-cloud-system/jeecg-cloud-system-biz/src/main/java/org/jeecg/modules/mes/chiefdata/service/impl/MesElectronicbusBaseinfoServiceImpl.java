package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesElectronicbusBaseinfo;
import org.jeecg.modules.mes.chiefdata.mapper.MesElectronicbusBaseinfoMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesElectronicbusBaseinfoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 主数据—电子料车基本信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesElectronicbusBaseinfoServiceImpl extends ServiceImpl<MesElectronicbusBaseinfoMapper, MesElectronicbusBaseinfo> implements IMesElectronicbusBaseinfoService {
	
	@Autowired
	private MesElectronicbusBaseinfoMapper mesElectronicbusBaseinfoMapper;
	
	@Override
	public List<MesElectronicbusBaseinfo> selectByMainId(String mainId) {
		return mesElectronicbusBaseinfoMapper.selectByMainId(mainId);
	}
}
