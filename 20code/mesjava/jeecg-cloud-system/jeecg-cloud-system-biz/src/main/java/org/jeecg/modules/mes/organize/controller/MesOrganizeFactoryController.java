package org.jeecg.modules.mes.organize.controller;

import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.organize.vo.MesFactorySitePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.organize.entity.MesOrganizeStoresite;
import org.jeecg.modules.mes.organize.entity.MesOrganizeFactory;
import org.jeecg.modules.mes.organize.service.IMesOrganizeFactoryService;
import org.jeecg.modules.mes.organize.service.IMesOrganizeStoresiteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 组织—工厂
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Api(tags="组织—工厂")
@RestController
@RequestMapping("/organize/mesOrganizeFactory")
@Slf4j
public class MesOrganizeFactoryController extends JeecgController<MesOrganizeFactory, IMesOrganizeFactoryService> {

	@Autowired
	private IMesOrganizeFactoryService mesOrganizeFactoryService;

	@Autowired
	private IMesOrganizeStoresiteService mesOrganizeStoresiteService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param mesOrganizeFactory
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "组织—工厂-分页列表查询")
	@ApiOperation(value="组织—工厂-分页列表查询", notes="组织—工厂-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesOrganizeFactory mesOrganizeFactory,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesOrganizeFactory> queryWrapper = QueryGenerator.initQueryWrapper(mesOrganizeFactory, req.getParameterMap());
		Page<MesOrganizeFactory> page = new Page<MesOrganizeFactory>(pageNo, pageSize);
		IPage<MesOrganizeFactory> pageList = mesOrganizeFactoryService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
     *   添加
     * @param mesOrganizeFactory
     * @return
     */
    @AutoLog(value = "组织—工厂-添加")
    @ApiOperation(value="组织—工厂-添加", notes="组织—工厂-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesOrganizeFactory mesOrganizeFactory) {
        mesOrganizeFactoryService.save(mesOrganizeFactory);
        return Result.ok("添加成功！");
    }

    /**
     *  编辑
     * @param mesOrganizeFactory
     * @return
     */
    @AutoLog(value = "组织—工厂-编辑")
    @ApiOperation(value="组织—工厂-编辑", notes="组织—工厂-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesOrganizeFactory mesOrganizeFactory) {
        mesOrganizeFactoryService.updateById(mesOrganizeFactory);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "组织—工厂-通过id删除")
    @ApiOperation(value="组织—工厂-通过id删除", notes="组织—工厂-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        mesOrganizeFactoryService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "组织—工厂-批量删除")
    @ApiOperation(value="组织—工厂-批量删除", notes="组织—工厂-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.mesOrganizeFactoryService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesOrganizeFactory mesOrganizeFactory) {
        return super.exportXls(request, mesOrganizeFactory, MesOrganizeFactory.class, "组织—工厂");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesOrganizeFactory.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/
	 /**
	  * 所有集合
	  * @return
	  */
	 @AutoLog(value = "组织—工厂库存点-所有集合")
	 @ApiOperation(value="组织—工厂库存点-所有集合", notes="组织—工厂库存点-所有集合")
	 @GetMapping(value = "/listMesFactoryStoresite")
	 public Result<?> listMesFactoryStoresite() {
		 List<MesFactorySitePage> factorySitePages = new ArrayList<>();
		 List<MesOrganizeFactory> factories = mesOrganizeFactoryService.list();
		 for (MesOrganizeFactory factory : factories) {
			 String factoryId = factory.getId();
			 String facCode = factory.getFacCode();
			 String facNameone = factory.getFacNameone();
			 QueryWrapper<MesOrganizeStoresite> wrapper = new QueryWrapper<>();
			 wrapper.eq("fac_id",factoryId);
			 List<MesOrganizeStoresite> storesites = mesOrganizeStoresiteService.list(wrapper);
			 for (MesOrganizeStoresite storesite : storesites) {
				 MesFactorySitePage mesFactorySitePage = new MesFactorySitePage();
				 String storeCode = storesite.getStoreCode();
				 String storeName = storesite.getStoreName();
				 mesFactorySitePage.setFacCode(facCode);
				 mesFactorySitePage.setFacNameone(facNameone);
				 mesFactorySitePage.setStoreCode(storeCode);
				 mesFactorySitePage.setStoreName(storeName);
				 factorySitePages.add(mesFactorySitePage);
			 }
		 }
		 return Result.ok(factorySitePages);
	 }

    /*--------------------------------子表处理-组织—工厂库存点-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "组织—工厂库存点-通过主表ID查询")
	@ApiOperation(value="组织—工厂库存点-通过主表ID查询", notes="组织—工厂库存点-通过主表ID查询")
	@GetMapping(value = "/listMesOrganizeStoresiteByMainId")
    public Result<?> listMesOrganizeStoresiteByMainId(MesOrganizeStoresite mesOrganizeStoresite,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesOrganizeStoresite> queryWrapper = QueryGenerator.initQueryWrapper(mesOrganizeStoresite, req.getParameterMap());
        Page<MesOrganizeStoresite> page = new Page<MesOrganizeStoresite>(pageNo, pageSize);
        IPage<MesOrganizeStoresite> pageList = mesOrganizeStoresiteService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesOrganizeStoresite
	 * @return
	 */
	@AutoLog(value = "组织—工厂库存点-添加")
	@ApiOperation(value="组织—工厂库存点-添加", notes="组织—工厂库存点-添加")
	@PostMapping(value = "/addMesOrganizeStoresite")
	public Result<?> addMesOrganizeStoresite(@RequestBody MesOrganizeStoresite mesOrganizeStoresite) {
		mesOrganizeStoresiteService.save(mesOrganizeStoresite);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesOrganizeStoresite
	 * @return
	 */
	@AutoLog(value = "组织—工厂库存点-编辑")
	@ApiOperation(value="组织—工厂库存点-编辑", notes="组织—工厂库存点-编辑")
	@PutMapping(value = "/editMesOrganizeStoresite")
	public Result<?> editMesOrganizeStoresite(@RequestBody MesOrganizeStoresite mesOrganizeStoresite) {
		mesOrganizeStoresiteService.updateById(mesOrganizeStoresite);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "组织—工厂库存点-通过id删除")
	@ApiOperation(value="组织—工厂库存点-通过id删除", notes="组织—工厂库存点-通过id删除")
	@DeleteMapping(value = "/deleteMesOrganizeStoresite")
	public Result<?> deleteMesOrganizeStoresite(@RequestParam(name="id",required=true) String id) {
		mesOrganizeStoresiteService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "组织—工厂库存点-批量删除")
	@ApiOperation(value="组织—工厂库存点-批量删除", notes="组织—工厂库存点-批量删除")
	@DeleteMapping(value = "/deleteBatchMesOrganizeStoresite")
	public Result<?> deleteBatchMesOrganizeStoresite(@RequestParam(name="ids",required=true) String ids) {
	    this.mesOrganizeStoresiteService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesOrganizeStoresite")
    public ModelAndView exportMesOrganizeStoresite(HttpServletRequest request, MesOrganizeStoresite mesOrganizeStoresite) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesOrganizeStoresite> queryWrapper = QueryGenerator.initQueryWrapper(mesOrganizeStoresite, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesOrganizeStoresite> pageList = mesOrganizeStoresiteService.list(queryWrapper);
		 List<MesOrganizeStoresite> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "组织—工厂库存点"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesOrganizeStoresite.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("组织—工厂库存点报表", "导出人:" + sysUser.getRealname(), "组织—工厂库存点"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesOrganizeStoresite/{mainId}")
    public Result<?> importMesOrganizeStoresite(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesOrganizeStoresite> list = ExcelImportUtil.importExcel(file.getInputStream(), MesOrganizeStoresite.class, params);
				 for (MesOrganizeStoresite temp : list) {
                    temp.setFacId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesOrganizeStoresiteService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-组织—工厂库存点-end----------------------------------------------*/




}
