package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataReplacematerial;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—替代料
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataReplacematerialService extends IService<MesChiefdataReplacematerial> {

    /**
     * 根据成品料号删除所有替代料数据
     * @param machineSort
     * @return
     */
    public boolean deleteMachineSort(String machineSort);
}
