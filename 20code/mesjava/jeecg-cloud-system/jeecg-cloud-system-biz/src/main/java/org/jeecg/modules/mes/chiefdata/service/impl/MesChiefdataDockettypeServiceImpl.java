package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataDockettype;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataDockettypeMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataDockettypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—单据类型配置
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataDockettypeServiceImpl extends ServiceImpl<MesChiefdataDockettypeMapper, MesChiefdataDockettype> implements IMesChiefdataDockettypeService {

}
