package org.jeecg.modules.mes.organize.service.impl;

import org.jeecg.modules.mes.organize.entity.MesOrganizeStoresite;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeStoresiteMapper;
import org.jeecg.modules.mes.organize.service.IMesOrganizeStoresiteService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 组织—工厂库存点
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesOrganizeStoresiteServiceImpl extends ServiceImpl<MesOrganizeStoresiteMapper, MesOrganizeStoresite> implements IMesOrganizeStoresiteService {
	
	@Autowired
	private MesOrganizeStoresiteMapper mesOrganizeStoresiteMapper;
	
	@Override
	public List<MesOrganizeStoresite> selectByMainId(String mainId) {
		return mesOrganizeStoresiteMapper.selectByMainId(mainId);
	}
}
