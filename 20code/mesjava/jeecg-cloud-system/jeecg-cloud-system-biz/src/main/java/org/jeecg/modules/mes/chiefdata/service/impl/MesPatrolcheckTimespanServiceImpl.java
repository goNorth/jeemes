package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesPatrolcheckTimespan;
import org.jeecg.modules.mes.chiefdata.mapper.MesPatrolcheckTimespanMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesPatrolcheckTimespanService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 主数据—巡检方案时段
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesPatrolcheckTimespanServiceImpl extends ServiceImpl<MesPatrolcheckTimespanMapper, MesPatrolcheckTimespan> implements IMesPatrolcheckTimespanService {
	
	@Autowired
	private MesPatrolcheckTimespanMapper mesPatrolcheckTimespanMapper;
	
	@Override
	public List<MesPatrolcheckTimespan> selectByMainId(String mainId) {
		return mesPatrolcheckTimespanMapper.selectByMainId(mainId);
	}
}
