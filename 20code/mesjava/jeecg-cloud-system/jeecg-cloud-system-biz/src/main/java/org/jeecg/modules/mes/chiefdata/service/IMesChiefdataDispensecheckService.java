package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataDispensecheck;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—免检管理
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataDispensecheckService extends IService<MesChiefdataDispensecheck> {

}
