package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBomitem;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @Description: BOM—数据项
 * @Author: jeecg-boot
 * @Date:   2020-10-20
 * @Version: V1.0
 */
public interface IMesChiefdataBomitemService extends IService<MesChiefdataBomitem> {

	public List<MesChiefdataBomitem> selectByMainId(String mainId);

	public Result<?> importMesChiefdataBomitem(Map<String, MultipartFile> fileMap, String mainId);

	/**
	 * 根据成品物料id查询bomid，再查询出bomitem数据
	 * @param materialId
	 * @return
	 */
	public List<MesChiefdataBomitem> selectByMaterialId(String materialId);

	/**
	 * 根据物料成品料号和详细物料料号，查询物料详细信息
	 * @param machinesortCode
	 * @param materielCode
	 * @return
	 */
	public MesChiefdataBomitem getsfg(String machinesortCode,String materielCode);
}
