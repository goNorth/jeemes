package org.jeecg.modules.mes.organize.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.organize.entity.MesOrganizeWarehouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 组织—仓库
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface MesOrganizeWarehouseMapper extends BaseMapper<MesOrganizeWarehouse> {

}
