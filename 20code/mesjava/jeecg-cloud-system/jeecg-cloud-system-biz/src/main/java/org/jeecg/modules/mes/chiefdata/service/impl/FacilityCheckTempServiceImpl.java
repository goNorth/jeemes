package org.jeecg.modules.mes.chiefdata.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.chiefdata.entity.FacilityCheckTemp;
import org.jeecg.modules.mes.chiefdata.mapper.FacilityCheckTempMapper;
import org.jeecg.modules.mes.chiefdata.service.IFacilityCheckTempService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 设备检验项目模板
 * @Author: jeecg-boot
 * @Date:   2021-06-02
 * @Version: V1.0
 */
@Service
public class FacilityCheckTempServiceImpl extends ServiceImpl<FacilityCheckTempMapper, FacilityCheckTemp> implements IFacilityCheckTempService {

    @Autowired
    private FacilityCheckTempMapper facilityCheckTempMapper;

    @Override
    public Result<?> queryByFacilityNameAndTempSort(String facilityName, String tempSort) {
        QueryWrapper<FacilityCheckTemp> queryWrapper = new QueryWrapper();
        queryWrapper.eq("facility_name", facilityName);
        queryWrapper.eq("temp_sort", tempSort);
        List<FacilityCheckTemp> list = facilityCheckTempMapper.selectList(queryWrapper);
        /*if (list.size() == 0) {
            return Result.error("未找到对应数据");
        }*/
        return Result.ok(list);
    }
}
