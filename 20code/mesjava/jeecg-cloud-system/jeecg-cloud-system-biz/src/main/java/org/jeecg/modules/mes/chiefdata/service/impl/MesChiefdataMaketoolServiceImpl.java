package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMaketool;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataMaketoolMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaketoolService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—制具信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataMaketoolServiceImpl extends ServiceImpl<MesChiefdataMaketoolMapper, MesChiefdataMaketool> implements IMesChiefdataMaketoolService {

}
