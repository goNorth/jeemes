package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesCertificatePerk;
import org.jeecg.modules.mes.chiefdata.entity.MesCertificateItem;
import org.jeecg.modules.mes.chiefdata.mapper.MesCertificateItemMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesCertificatePerkMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesCertificatePerkService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 物料凭证抬头
 * @Author: jeecg-boot
 * @Date:   2020-10-12
 * @Version: V1.0
 */
@Service
public class MesCertificatePerkServiceImpl extends ServiceImpl<MesCertificatePerkMapper, MesCertificatePerk> implements IMesCertificatePerkService {

	@Autowired
	private MesCertificatePerkMapper mesCertificatePerkMapper;
	@Autowired
	private MesCertificateItemMapper mesCertificateItemMapper;
	
	@Override
	@Transactional
	public void delMain(String id) {
		mesCertificateItemMapper.deleteByMainId(id);
		mesCertificatePerkMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesCertificateItemMapper.deleteByMainId(id.toString());
			mesCertificatePerkMapper.deleteById(id);
		}
	}
	
}
