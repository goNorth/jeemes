package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesClassbatchTimetable;
import org.jeecg.modules.mes.chiefdata.mapper.MesClassbatchTimetableMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesClassbatchTimetableService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 班组—时间
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesClassbatchTimetableServiceImpl extends ServiceImpl<MesClassbatchTimetableMapper, MesClassbatchTimetable> implements IMesClassbatchTimetableService {
	
	@Autowired
	private MesClassbatchTimetableMapper mesClassbatchTimetableMapper;
	
	@Override
	public List<MesClassbatchTimetable> selectByMainId(String mainId) {
		return mesClassbatchTimetableMapper.selectByMainId(mainId);
	}
}
