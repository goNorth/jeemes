package org.jeecg.modules.biconf.service;

import org.jeecg.modules.biconf.entity.BiVisualMap;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 地图主数据
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
public interface IBiVisualMapService extends IService<BiVisualMap> {

}
