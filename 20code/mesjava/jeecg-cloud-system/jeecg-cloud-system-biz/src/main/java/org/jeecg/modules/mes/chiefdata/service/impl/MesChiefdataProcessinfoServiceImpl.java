package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataProcessinfo;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataProcessinfoMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataProcessinfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—工序信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataProcessinfoServiceImpl extends ServiceImpl<MesChiefdataProcessinfoMapper, MesChiefdataProcessinfo> implements IMesChiefdataProcessinfoService {

}
