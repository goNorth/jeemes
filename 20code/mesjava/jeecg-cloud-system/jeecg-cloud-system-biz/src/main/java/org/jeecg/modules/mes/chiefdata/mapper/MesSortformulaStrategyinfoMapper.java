package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesSortformulaStrategyinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 主数据—拣货方案策略
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface MesSortformulaStrategyinfoMapper extends BaseMapper<MesSortformulaStrategyinfo> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesSortformulaStrategyinfo> selectByMainId(@Param("mainId") String mainId);
}
