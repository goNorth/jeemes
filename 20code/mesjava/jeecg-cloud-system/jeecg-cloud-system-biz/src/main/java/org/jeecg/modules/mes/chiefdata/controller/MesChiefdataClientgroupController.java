package org.jeecg.modules.mes.chiefdata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataClientgroup;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataClientgroupService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 主数据—客户组
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Api(tags="主数据—客户组")
@RestController
@RequestMapping("/chiefdata/mesChiefdataClientgroup")
@Slf4j
public class MesChiefdataClientgroupController extends JeecgController<MesChiefdataClientgroup, IMesChiefdataClientgroupService> {
	@Autowired
	private IMesChiefdataClientgroupService mesChiefdataClientgroupService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataClientgroup
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—客户组-分页列表查询")
	@ApiOperation(value="主数据—客户组-分页列表查询", notes="主数据—客户组-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataClientgroup mesChiefdataClientgroup,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataClientgroup> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataClientgroup, req.getParameterMap());
		Page<MesChiefdataClientgroup> page = new Page<MesChiefdataClientgroup>(pageNo, pageSize);
		IPage<MesChiefdataClientgroup> pageList = mesChiefdataClientgroupService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesChiefdataClientgroup
	 * @return
	 */
	@AutoLog(value = "主数据—客户组-添加")
	@ApiOperation(value="主数据—客户组-添加", notes="主数据—客户组-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataClientgroup mesChiefdataClientgroup) {
		mesChiefdataClientgroupService.save(mesChiefdataClientgroup);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesChiefdataClientgroup
	 * @return
	 */
	@AutoLog(value = "主数据—客户组-编辑")
	@ApiOperation(value="主数据—客户组-编辑", notes="主数据—客户组-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataClientgroup mesChiefdataClientgroup) {
		mesChiefdataClientgroupService.updateById(mesChiefdataClientgroup);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—客户组-通过id删除")
	@ApiOperation(value="主数据—客户组-通过id删除", notes="主数据—客户组-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataClientgroupService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—客户组-批量删除")
	@ApiOperation(value="主数据—客户组-批量删除", notes="主数据—客户组-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataClientgroupService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—客户组-通过id查询")
	@ApiOperation(value="主数据—客户组-通过id查询", notes="主数据—客户组-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataClientgroup mesChiefdataClientgroup = mesChiefdataClientgroupService.getById(id);
		if(mesChiefdataClientgroup==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataClientgroup);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataClientgroup
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataClientgroup mesChiefdataClientgroup) {
        return super.exportXls(request, mesChiefdataClientgroup, MesChiefdataClientgroup.class, "主数据—客户组");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataClientgroup.class);
    }

}
