package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSupplier;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataSupplierMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataSupplierService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—供应商
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataSupplierServiceImpl extends ServiceImpl<MesChiefdataSupplierMapper, MesChiefdataSupplier> implements IMesChiefdataSupplierService {

}
