package org.jeecg.modules.mes.organize.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.organize.entity.MesOrganizeFactory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 组织—工厂
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface MesOrganizeFactoryMapper extends BaseMapper<MesOrganizeFactory> {

}
