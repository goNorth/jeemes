package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesTransactionLock;
import org.jeecg.modules.mes.chiefdata.mapper.MesTransactionLockMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesTransactionLockService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 业务锁定
 * @Author: jeecg-boot
 * @Date:   2020-10-24
 * @Version: V1.0
 */
@Service
public class MesTransactionLockServiceImpl extends ServiceImpl<MesTransactionLockMapper, MesTransactionLock> implements IMesTransactionLockService {

}
