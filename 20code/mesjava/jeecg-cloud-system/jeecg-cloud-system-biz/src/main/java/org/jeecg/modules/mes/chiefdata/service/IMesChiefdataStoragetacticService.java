package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataStoragetactic;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—存储策略
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataStoragetacticService extends IService<MesChiefdataStoragetactic> {

}
