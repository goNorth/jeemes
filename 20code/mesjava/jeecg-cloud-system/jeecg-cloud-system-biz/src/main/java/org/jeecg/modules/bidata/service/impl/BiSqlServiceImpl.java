package org.jeecg.modules.bidata.service.impl;

import org.jeecg.modules.bidata.entity.BiSql;
import org.jeecg.modules.bidata.mapper.BiSqlMapper;
import org.jeecg.modules.bidata.service.IBiSqlService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 数据配置接口
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
@Service
public class BiSqlServiceImpl extends ServiceImpl<BiSqlMapper, BiSql> implements IBiSqlService {

}
