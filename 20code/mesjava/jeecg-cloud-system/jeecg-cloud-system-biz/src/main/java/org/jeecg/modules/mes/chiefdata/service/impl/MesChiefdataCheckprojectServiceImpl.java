package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataCheckproject;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataCheckprojectMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataCheckprojectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—检测项目
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataCheckprojectServiceImpl extends ServiceImpl<MesChiefdataCheckprojectMapper, MesChiefdataCheckproject> implements IMesChiefdataCheckprojectService {

}
