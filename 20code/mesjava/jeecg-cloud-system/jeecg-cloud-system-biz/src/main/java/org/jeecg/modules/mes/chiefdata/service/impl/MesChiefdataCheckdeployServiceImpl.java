package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataCheckdeploy;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataCheckdeployMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataCheckdeployService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—检验配置
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataCheckdeployServiceImpl extends ServiceImpl<MesChiefdataCheckdeployMapper, MesChiefdataCheckdeploy> implements IMesChiefdataCheckdeployService {

}
