package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesCertificateItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 物料凭证项目
 * @Author: jeecg-boot
 * @Date:   2020-10-12
 * @Version: V1.0
 */
public interface MesCertificateItemMapper extends BaseMapper<MesCertificateItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesCertificateItem> selectByMainId(@Param("mainId") String mainId);

}
