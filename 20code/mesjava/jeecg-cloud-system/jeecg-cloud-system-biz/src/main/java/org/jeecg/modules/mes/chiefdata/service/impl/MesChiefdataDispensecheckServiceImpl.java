package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataDispensecheck;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataDispensecheckMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataDispensecheckService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—免检管理
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataDispensecheckServiceImpl extends ServiceImpl<MesChiefdataDispensecheckMapper, MesChiefdataDispensecheck> implements IMesChiefdataDispensecheckService {

}
