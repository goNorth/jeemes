package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataProduct;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataProductMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataProductService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—商品
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataProductServiceImpl extends ServiceImpl<MesChiefdataProductMapper, MesChiefdataProduct> implements IMesChiefdataProductService {

}
