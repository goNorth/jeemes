package org.jeecg.modules.mes.organize.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.organize.entity.MesOrganizeCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 组织—公司
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface MesOrganizeCompanyMapper extends BaseMapper<MesOrganizeCompany> {

}
