package org.jeecg.modules.mes.chiefdata.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.ApplyBillDBUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBom;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBomitem;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMateriel;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataBomService;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataBomitemService;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaterielService;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 主数据—BOM
 * @Author: jeecg-boot
 * @Date: 2020-10-20
 * @Version: V1.0
 */
@Api(tags = "主数据—BOM")
@RestController
@RequestMapping("/chiefdata/mesChiefdataBom")
@Slf4j
public class MesChiefdataBomController extends JeecgController<MesChiefdataBom, IMesChiefdataBomService> {

    @Autowired
    private IMesChiefdataBomService mesChiefdataBomService;

    @Autowired
    private IMesChiefdataBomitemService mesChiefdataBomitemService;

    @Autowired
    private IMesChiefdataMaterielService mesChiefdataMaterielService;

    /*---------------------------------主表处理-begin-------------------------------------*/

    /**
     * 分页列表查询
     *
     * @param mesChiefdataBom
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "主数据—BOM-分页列表查询")
    @ApiOperation(value = "主数据—BOM-分页列表查询", notes = "主数据—BOM-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(MesChiefdataBom mesChiefdataBom,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<MesChiefdataBom> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataBom, req.getParameterMap());
        Page<MesChiefdataBom> page = new Page<MesChiefdataBom>(pageNo, pageSize);
        IPage<MesChiefdataBom> pageList = mesChiefdataBomService.page(page, queryWrapper);
        List<MesChiefdataBom> bomList = pageList.getRecords();
        if (bomList.size() != 0) {
            for (MesChiefdataBom bom : bomList) {
                if (StringUtils.isBlank(bom.getImportState())) {
                    bom.setImportState("0");
                    mesChiefdataBomService.updateById(bom);
                }
            }
        }
        IPage<MesChiefdataBom> pageList1 = mesChiefdataBomService.page(page, queryWrapper);
        return Result.ok(pageList1);
    }


    /**
     * 料号查询
     *
     * @param
     * @return
     */
    @AutoLog(value = "主数据—BOM-料号查询")
    @ApiOperation(value = "主数据—BOM-料号查询", notes = "主数据—BOM-料号查询")
    @GetMapping(value = "/queryByMcode")
    public MesChiefdataBom queryByMcode(@RequestParam(name = "Mcode", required = true) String Mcode) {
        QueryWrapper<MesChiefdataBom> wrapper = new QueryWrapper<>();
        wrapper.eq("machinesort_code", Mcode);
        List<MesChiefdataBom> list = mesChiefdataBomService.list(wrapper);
        if(list.size()>0){
            return list.get(0);
        }else{
            return null;
        }
    }

    /**
     * 根据主表物料料号查询子表信息
     * @param Mcode
     * @return
     */
    @GetMapping(value = "/queryByMcodeZu")
    public List<MesChiefdataBomitem> queryByMcodeZu(@RequestParam(name = "Mcode", required = true) String Mcode) {
        QueryWrapper<MesChiefdataBom> wrapper = new QueryWrapper<>();
        wrapper.eq("machinesort_code", Mcode);
        List<MesChiefdataBom> list = mesChiefdataBomService.list(wrapper);
        if(list.size()>0){
            QueryWrapper<MesChiefdataBomitem> wrapper2 = new QueryWrapper<>();
            wrapper2.eq("bom_id", list.get(0).getId());
            return mesChiefdataBomitemService.list(wrapper2);
        }else{
            return null;
        }
    }

    @AutoLog(value = "主数据—BOM-导入关务系统")
    @ApiOperation(value = "主数据—BOM-导入关务系统", notes = "主数据—BOM-导入关务系统")
    @GetMapping(value = "/importBOMSqlServer")
    public Result<?> importBOMSqlServer(@RequestParam(name = "id", required = true) String id) {
        QueryWrapper<MesChiefdataBom> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("import_state", "0");//是否导入关务系统，若已导入，则为1,反之为0
        queryWrapper.eq("id", id);
        List<MesChiefdataBom> bomList = mesChiefdataBomService.list(queryWrapper);
        System.err.println("BOM导入关务系统:"+bomList);
        System.err.println("BOM导入关务系统数量:"+bomList.size());
        if (bomList.size() != 0) {
            for (MesChiefdataBom bom : bomList) {
                String bomId = bom.getId();
                QueryWrapper<MesChiefdataBomitem> wrapper = new QueryWrapper<>();
                wrapper.eq("bom_id", bomId);
                List<MesChiefdataBomitem> bomitemList = mesChiefdataBomitemService.list(wrapper);
                if (bomitemList.size() != 0) {
                    for (MesChiefdataBomitem bomitem : bomitemList) {
                         ApplyBillDBUtil.insertBOMSqlServer(bom, bomitem);
                    }
                    //修改bom  import_state状态为1
                    bom.setImportState("1");
                    mesChiefdataBomService.updateById(bom);
                } else {
                    return Result.error("没有找到BOM子表的数据！请检查！");
                }
            }
        } else {
            return Result.ok("import_state导入状态为0，的该数据未查到！请检查！");
        }
        return Result.ok("关务系统导入成功！");
    }

    /**
     * 添加
     *
     * @param mesChiefdataBom
     * @return
     */
    @AutoLog(value = "主数据—BOM-添加")
    @ApiOperation(value = "主数据—BOM-添加", notes = "主数据—BOM-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesChiefdataBom mesChiefdataBom) {
        QueryWrapper<MesChiefdataBom> mesqueryWrapper = new QueryWrapper<>();
        mesqueryWrapper.eq("machinesort_code",mesChiefdataBom.getMachinesortCode());
        List<MesChiefdataBom> list = mesChiefdataBomService.list(mesqueryWrapper);
        if(list.size()>0){
            return Result.error(mesChiefdataBom.getMachinesortCode()+"不能重复添加！");
        }

        mesChiefdataBom.setImportState("0");
        mesChiefdataBomService.save(mesChiefdataBom);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param mesChiefdataBom
     * @return
     */
    @AutoLog(value = "主数据—BOM-编辑")
    @ApiOperation(value = "主数据—BOM-编辑", notes = "主数据—BOM-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesChiefdataBom mesChiefdataBom) {
        QueryWrapper<MesChiefdataBom> mesqueryWrapper = new QueryWrapper<>();
        mesqueryWrapper.eq("machinesort_code",mesChiefdataBom.getMachinesortCode());
        mesqueryWrapper.ne("id",mesChiefdataBom.getId());
        List<MesChiefdataBom> list = mesChiefdataBomService.list(mesqueryWrapper);
        if(list.size()>0){
            return Result.error(mesChiefdataBom.getMachinesortCode()+"不能重复添加！");
        }
        mesChiefdataBomService.updateById(mesChiefdataBom);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "主数据—BOM-通过id删除")
    @ApiOperation(value = "主数据—BOM-通过id删除", notes = "主数据—BOM-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        mesChiefdataBomService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "主数据—BOM-通过主表id更新替代料信息")
    @ApiOperation(value = "主数据—BOM-通过主表id更新替代料信息", notes = "主数据—BOM-通过主表id更新替代料信息")
    @GetMapping(value = "/replacematerial")
    public Result<?> replacematerial(@RequestParam(name = "id", required = true) String id) {
        boolean mark=mesChiefdataBomService.replacematerial(id);
        if(mark) {
            return Result.ok("更新成功!");
        }else {
            return Result.error("更新失败！");
        }
    }



    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "主数据—BOM-批量删除")
    @ApiOperation(value = "主数据—BOM-批量删除", notes = "主数据—BOM-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.mesChiefdataBomService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     *
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataBom mesChiefdataBom) {
        return super.exportXls(request, mesChiefdataBom, MesChiefdataBom.class, "主数据—BOM");
    }

    /**
     * 导入
     *
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        boolean mark = mesChiefdataBomService.importMesChiefdataBom(fileMap);
        if(mark){
            return Result.ok("文件导入成功！");
        }
        return Result.error("文件导入失败！");
    }
    /*---------------------------------主表处理-end-------------------------------------*/


    /*--------------------------------子表处理-BOM—数据项-begin----------------------------------------------*/

    /**
     * 通过BOMID查询
     *
     * @return
     */
    @AutoLog(value = "BOM—数据项-通过BOMID查询")
    @ApiOperation(value = "BOM—数据项-通过BOMID查询", notes = "BOM—数据项-通过BOMID查询")
    @GetMapping(value = "/queryMesBomitemByMainId")
    public List<MesChiefdataBomitem> queryMesBomitemByMainId(@RequestParam(name = "id", required = true) String id) {
        QueryWrapper<MesChiefdataBomitem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("bom_id", id);
        List<MesChiefdataBomitem> bomitems = mesChiefdataBomitemService.list(queryWrapper);
        return bomitems;
    }
    /**
     * 通过BOMID查询半成品数据
     *
     * @return
     */
    @AutoLog(value = "BOM—数据项-通过BOMID查询半成品数据")
    @ApiOperation(value = "BOM—数据项-通过BOMID查询半成品数据", notes = "BOM—数据项-通过BOMID查询半成品数据")
    @GetMapping(value = "/queryMesBomitemBCPMainId")
    public List<MesChiefdataBomitem> queryMesBomitemBCPMainId(@RequestParam(name = "id", required = true) String id) {
        QueryWrapper<MesChiefdataBomitem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("bom_id", id);
        queryWrapper.eq("material_type", "BCP");
        return mesChiefdataBomitemService.list(queryWrapper);
    }


    /**
     * 根据成品物料id查询bomid，再查询出bomitem数据 远程调用
     * @param materialId
     * @return
     */
    @GetMapping(value = "/selectByMaterialId")
    public List<MesChiefdataBomitem> selectByMaterialId(@RequestParam(name = "materialId", required = true)String materialId){
        return mesChiefdataBomitemService.selectByMaterialId(materialId);
    }


    @GetMapping(value = "/selectByMaterialCode")
    public List<MesChiefdataBomitem> selectByMaterialCode(@RequestParam(name = "materialId", required = true)String materialId){
        QueryWrapper<MesChiefdataBom> w1 =new QueryWrapper<>();
        w1.eq("machinesort_code",materialId);
        List<MesChiefdataBom> list = mesChiefdataBomService.list(w1);
        if(list==null || list.size()==0){
            return null;
        }
        MesChiefdataBom bom = list.get(0);
        return mesChiefdataBomitemService.selectByMainId(bom.getId());
    }

    /**
     * 根据物料成品料号和详细物料料号，查询物料详细信息 远程调用
     * @param machinesortCode
     * @param materielCode
     * @return
     */
    @GetMapping(value = "/getsfg")
    public MesChiefdataBomitem getsfg(@RequestParam(name = "machinesortCode", required = true)String machinesortCode,@RequestParam(name = "materielCode", required = true)String materielCode){
        return mesChiefdataBomitemService.getsfg(machinesortCode,materielCode);
    }

    /**
     * 通过主表ID查询
     *
     * @return
     */
    @AutoLog(value = "BOM—数据项-通过主表ID查询")
    @ApiOperation(value = "BOM—数据项-通过主表ID查询", notes = "BOM—数据项-通过主表ID查询")
    @GetMapping(value = "/listMesChiefdataBomitemByMainId")
    public Result<?> listMesChiefdataBomitemByMainId(MesChiefdataBomitem mesChiefdataBomitem,
                                                     @RequestParam(name = "materielGrade",required = false) String materielGrade,
                                                     @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                     @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                     HttpServletRequest req) {
        if(StringUtils.isNotBlank(materielGrade)) {
            mesChiefdataBomitem.setMaterielGrade(materielGrade);//新增物料阶别查询
        }
        QueryWrapper<MesChiefdataBomitem> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataBomitem, req.getParameterMap());
        Page<MesChiefdataBomitem> page = new Page<MesChiefdataBomitem>(pageNo, pageSize);
        IPage<MesChiefdataBomitem> pageList = mesChiefdataBomitemService.page(page, queryWrapper);
        /*if (pageList.getTotal() != 0) {
            List<MesChiefdataBomitem> bomitemList = pageList.getRecords();
            for (MesChiefdataBomitem bomitem : bomitemList) {
                if (StringUtils.isBlank(bomitem.getProductCode())) {
                    String mCode = bomitem.getMaterielCode();
                    QueryWrapper<MesChiefdataMateriel> wrapper = new QueryWrapper<>();
                    wrapper.eq("materiel_code", mCode);
                    MesChiefdataMateriel materiel = mesChiefdataMaterielService.getOne(wrapper);
                    System.err.println(materiel);
                    //如果物料的商品编码已录入，则在BOM页面刷新时，更新BOM的商品编码
                    if (StringUtils.isNotBlank(materiel.getProductCode())) {
                        bomitem.setProductCode(materiel.getProductCode());
                        mesChiefdataBomitemService.updateById(bomitem);
                    }
                }
            }
        }
        IPage<MesChiefdataBomitem> pageList1 = mesChiefdataBomitemService.page(page, queryWrapper);*/
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param mesChiefdataBomitem
     * @return
     */
    @AutoLog(value = "BOM—数据项-添加")
    @ApiOperation(value = "BOM—数据项-添加", notes = "BOM—数据项-添加")
    @PostMapping(value = "/addMesChiefdataBomitem")
    public Result<?> addMesChiefdataBomitem(@RequestBody MesChiefdataBomitem mesChiefdataBomitem) {
        QueryWrapper<MesChiefdataBomitem> mesqueryWrapper = new QueryWrapper<>();
        mesqueryWrapper.eq("materiel_code",mesChiefdataBomitem.getMaterielCode());
        mesqueryWrapper.eq("bom_id",mesChiefdataBomitem.getBomId());
        List<MesChiefdataBomitem> list = mesChiefdataBomitemService.list(mesqueryWrapper);
        if(list.size()>0){
            return Result.error(mesChiefdataBomitem.getMaterielCode()+"不能重复添加！");
        }
        mesChiefdataBomitemService.save(mesChiefdataBomitem);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param mesChiefdataBomitem
     * @return
     */
    @AutoLog(value = "BOM—数据项-编辑")
    @ApiOperation(value = "BOM—数据项-编辑", notes = "BOM—数据项-编辑")
    @PutMapping(value = "/editMesChiefdataBomitem")
    public Result<?> editMesChiefdataBomitem(@RequestBody MesChiefdataBomitem mesChiefdataBomitem) {
        QueryWrapper<MesChiefdataBomitem> mesqueryWrapper = new QueryWrapper<>();
        mesqueryWrapper.eq("materiel_code",mesChiefdataBomitem.getMaterielCode());
        mesqueryWrapper.eq("bom_id",mesChiefdataBomitem.getBomId());
        mesqueryWrapper.ne("id",mesChiefdataBomitem.getId());
        List<MesChiefdataBomitem> list = mesChiefdataBomitemService.list(mesqueryWrapper);
        if(list.size()>0){
            return Result.error(mesChiefdataBomitem.getMaterielCode()+"不能重复！");
        }
        if (StringUtils.isNotBlank(mesChiefdataBomitem.getClientCode())) {
            MesChiefdataBomitem item = mesChiefdataBomitemService.getById(mesChiefdataBomitem.getId());
            if (!mesChiefdataBomitem.getClientCode().equals(item.getClientCode())) {
                //更新物料管理中的客户料号
                QueryWrapper<MesChiefdataMateriel> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("materiel_code", mesChiefdataBomitem.getMaterielCode());
                MesChiefdataMateriel mesChiefdataMateriel = mesChiefdataMaterielService.getOne(queryWrapper);
                mesChiefdataMateriel.setQuery2(mesChiefdataBomitem.getClientCode());
                mesChiefdataMaterielService.updateById(mesChiefdataMateriel);
            }
        }
        mesChiefdataBomitemService.updateById(mesChiefdataBomitem);
        return Result.ok("编辑成功!");
    }

    /**
     * BOM—数据项-根据物料编号查询最新的一个物料信息
     *
     * @param materielCode 物料料号
     * @return
     */
    @AutoLog(value = "BOM—数据项-根据物料编号查询最新的一个物料信息")
    @ApiOperation(value = "BOM—数据项-根据物料编号查询最新的一个物料信息", notes = "BOM—数据项-根据物料编号查询最新的一个物料信息,用来查出原值")
    @GetMapping(value = "/getMCodebomitem")
    public Result<?> getMaterielCodeitem(@RequestParam(name = "materielCode", required = true) String materielCode) {
        QueryWrapper<MesChiefdataBomitem> mesqueryWrapper = new QueryWrapper<>();
        mesqueryWrapper.eq("materiel_code",materielCode);
        List<MesChiefdataBomitem> list = mesChiefdataBomitemService.list(mesqueryWrapper);
        if(list.size()>0){
            return Result.ok(list.get(0));
        }
        return Result.error("没有在bom中找到该物料信息!");
    }

    /**
     * BOM—数据项-根据物料编号查询该物料属于那些BOM
     */
    @AutoLog(value = "BOM—数据项-根据物料编号查询该物料属于那些BOM")
    @ApiOperation(value = "BOM—数据项-根据物料编号查询该物料属于那些BOM", notes = "BOM—数据项-根据物料编号查询该物料属于那些BOM,用来查出原值")
    @GetMapping(value = "/getChiefDataBomItemByMaterielCode")
    public Result<?> getChiefDataBomItemByMaterielCode(@RequestParam(name = "materielCode") String materielCode) {
        QueryWrapper<MesChiefdataBomitem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("materiel_code",materielCode);
        List<MesChiefdataBomitem> list = mesChiefdataBomitemService.list(queryWrapper);
        return Result.ok(list);
    }


    @AutoLog(value = "BOM—数据项-根据物料编号查询最新的一个物料信息")
    @ApiOperation(value = "BOM—数据项-根据物料编号查询最新的一个物料信息", notes = "BOM—数据项-根据物料编号查询最新的一个物料信息,用来查出原值")
    @GetMapping(value = "/getMCodebomitemForIn")
    public MesChiefdataBomitem getMCodebomitemForIn(@RequestParam(name = "materielCode", required = true) String materielCode) {
        QueryWrapper<MesChiefdataBomitem> mesqueryWrapper = new QueryWrapper<>();
        mesqueryWrapper.eq("materiel_code",materielCode);
        mesqueryWrapper.orderByDesc("create_time");
        List<MesChiefdataBomitem> list = mesChiefdataBomitemService.list(mesqueryWrapper);
        if(list.size()>0){
            return list.get(0);
        }
        return new MesChiefdataBomitem();
    }

    /**
     *
     * @param zmaterielCode 成品料号
     * @param materielCode 物料料号
     * @param zfstate 是正是反
     * @param fmnum 用量
     * @return
     */
    @GetMapping(value = "/getMCodebomitemForInS")
    public boolean getMCodebomitemForInS(@RequestParam(name = "zmaterielCode", required = true) String zmaterielCode,
                                                     @RequestParam(name = "materielCode", required = true) String materielCode,
                                                     @RequestParam(name = "zfstate", required = true) String zfstate,
                                                     @RequestParam(name = "fmnum", required = true) String fmnum) {
        QueryWrapper<MesChiefdataBom> mcdbonqueryWrapper = new QueryWrapper<>();
        mcdbonqueryWrapper.eq("machinesort_code",zmaterielCode);
        mcdbonqueryWrapper.orderByDesc("create_time");
        List<MesChiefdataBom> list1 = mesChiefdataBomService.list(mcdbonqueryWrapper);
        if (list1.size()>0){
            QueryWrapper<MesChiefdataBomitem> mesqueryWrapper = new QueryWrapper<>();
            mesqueryWrapper.eq("bom_id",list1.get(0).getId());
            mesqueryWrapper.eq("materiel_code",materielCode);
            List<MesChiefdataBomitem> list = mesChiefdataBomitemService.list(mesqueryWrapper);
            for(MesChiefdataBomitem bomitem:list){
                if("正面".equals(zfstate)){
                    bomitem.setQuantity(fmnum);
                    mesChiefdataBomitemService.updateById(bomitem);
                }
                if("反面".equals(zfstate)){
                    bomitem.setInverseQuantity(fmnum);
                    mesChiefdataBomitemService.updateById(bomitem);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "BOM—数据项-通过id删除")
    @ApiOperation(value = "BOM—数据项-通过id删除", notes = "BOM—数据项-通过id删除")
    @DeleteMapping(value = "/deleteMesChiefdataBomitem")
    public Result<?> deleteMesChiefdataBomitem(@RequestParam(name = "id", required = true) String id) {
        mesChiefdataBomitemService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "BOM—数据项-批量删除")
    @ApiOperation(value = "BOM—数据项-批量删除", notes = "BOM—数据项-批量删除")
    @DeleteMapping(value = "/deleteBatchMesChiefdataBomitem")
    public Result<?> deleteBatchMesChiefdataBomitem(@RequestParam(name = "ids", required = true) String ids) {
        this.mesChiefdataBomitemService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     *
     * @return
     */
    @RequestMapping(value = "/exportMesChiefdataBomitem")
    public ModelAndView exportMesChiefdataBomitem(HttpServletRequest request, MesChiefdataBomitem mesChiefdataBomitem) {
        // Step.1 组装查询条件
        QueryWrapper<MesChiefdataBomitem> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataBomitem, request.getParameterMap());
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

        // Step.2 获取导出数据
        List<MesChiefdataBomitem> pageList = mesChiefdataBomitemService.list(queryWrapper);
        List<MesChiefdataBomitem> exportList = null;

        // 过滤选中数据
        String selections = request.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
        } else {
            exportList = pageList;
        }

        // Step.3 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "BOM—数据项"); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, MesChiefdataBomitem.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("BOM—数据项报表", "导出人:" + sysUser.getRealname(), "BOM—数据项"));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        return mv;
    }

    /**
     * 导入
     *
     * @return
     */
    @RequestMapping(value = "/importMesChiefdataBomitem/{mainId}", method = RequestMethod.POST)
    public Result<?> importMesChiefdataBomitem(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        return mesChiefdataBomitemService.importMesChiefdataBomitem(fileMap,mainId);
        /*if(result.isSuccess()){
            return Result.ok("文件导入成功！");
        }
        return Result.error("文件导入失败！");*/
    }

    /*--------------------------------子表处理-BOM—数据项-end----------------------------------------------*/


}
