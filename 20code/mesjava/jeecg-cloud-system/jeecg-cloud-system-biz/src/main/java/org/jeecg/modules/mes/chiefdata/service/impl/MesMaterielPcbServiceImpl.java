package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielPcb;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielPcbMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielPcbService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 物料—PCB
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesMaterielPcbServiceImpl extends ServiceImpl<MesMaterielPcbMapper, MesMaterielPcb> implements IMesMaterielPcbService {
	
	@Autowired
	private MesMaterielPcbMapper mesMaterielPcbMapper;
	
	@Override
	public List<MesMaterielPcb> selectByMainId(String mainId) {
		return mesMaterielPcbMapper.selectByMainId(mainId);
	}
}
