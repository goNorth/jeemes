package org.jeecg.modules.mes.organize.mapper;

import java.util.List;
import org.jeecg.modules.mes.organize.entity.MesOrganizeClasstype;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 组织—日历（班别）
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface MesOrganizeClasstypeMapper extends BaseMapper<MesOrganizeClasstype> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesOrganizeClasstype> selectByMainId(@Param("mainId") String mainId);

}
