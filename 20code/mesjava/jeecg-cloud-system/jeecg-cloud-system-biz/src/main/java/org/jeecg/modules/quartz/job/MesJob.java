package org.jeecg.modules.quartz.job;


import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.mes.client.ProduceClient;
import org.jeecg.modules.mes.produce.entity.MesAncillarytoolHeat;
import org.jeecg.modules.message.websocket.WebSocket;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 示例不带参定时任务
 *
 * @Author Scott
 */
@Slf4j
public class MesJob implements Job {
	@Resource
	private WebSocket webSocket;

	@Autowired
	private ISysBaseAPI sysBaseAPI;

	@Autowired
	ProduceClient produceClient;

	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		System.err.println("运行到了1号这里");
		System.err.println(produceClient);

		List<MesAncillarytoolHeat> ancillarytoolHeats = produceClient.listByFinishTime();
		if(ancillarytoolHeats == null){
			System.err.println("调用了服务，但是找不到数据！");
		}
		if(ancillarytoolHeats != null){
			System.err.println("找到数据，但是调用失败！");
		}
		System.err.println("运行到了2号这里");

		for (MesAncillarytoolHeat heat : ancillarytoolHeats) {
			System.err.println("运行到了3号这里");
			//系统当前时间
			Date date = new Date();
			if(date.after(heat.getExpectFinishtime())){
				sysBaseAPI.sendSysAnnouncement("admin",heat.getCreateBy(),"回温结束","已经到达辅料回温的预计结束时间！请注意！");
				log.info(String.format(" Jeecg-Boot 普通定时任务 SampleJob !  时间:" + DateUtils.getTimestamp()));
			}
		}
//		sysBaseAPI.sendSysAnnouncement("admin","orz","测试","内容测试");
//		log.info(String.format(" Jeecg-Boot 普通定时任务 SampleJob !  时间:" + DateUtils.getTimestamp()));
	}
}
