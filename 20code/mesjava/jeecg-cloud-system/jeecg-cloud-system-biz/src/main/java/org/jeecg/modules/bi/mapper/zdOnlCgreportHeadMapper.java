package org.jeecg.modules.bi.mapper;

import org.jeecg.modules.bi.entity.zdOnlCgreportHead;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: onl_cgreport_head
 * @Author: wms-cloud
 * @Date:   2020-12-09
 * @Version: V1.0
 */
public interface zdOnlCgreportHeadMapper extends BaseMapper<zdOnlCgreportHead> {

}
