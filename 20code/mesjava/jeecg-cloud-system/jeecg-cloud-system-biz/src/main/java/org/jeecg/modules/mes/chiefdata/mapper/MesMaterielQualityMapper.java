package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielQuality;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 物料—品质
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface MesMaterielQualityMapper extends BaseMapper<MesMaterielQuality> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesMaterielQuality> selectByMainId(@Param("mainId") String mainId);
}
