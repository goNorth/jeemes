package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBadcode;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataBadcodeMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataBadcodeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—不良代码
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataBadcodeServiceImpl extends ServiceImpl<MesChiefdataBadcodeMapper, MesChiefdataBadcode> implements IMesChiefdataBadcodeService {

}
