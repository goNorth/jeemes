package org.jeecg.modules.mes.organize.service;

import org.jeecg.modules.mes.organize.entity.MesOrganizeSale;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 组织—销售
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesOrganizeSaleService extends IService<MesOrganizeSale> {

}
