package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataPatrolcheck;
import org.jeecg.modules.mes.chiefdata.entity.MesPatrolcheckTimespan;
import org.jeecg.modules.mes.chiefdata.mapper.MesPatrolcheckTimespanMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataPatrolcheckMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataPatrolcheckService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 主数据—巡检方案
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataPatrolcheckServiceImpl extends ServiceImpl<MesChiefdataPatrolcheckMapper, MesChiefdataPatrolcheck> implements IMesChiefdataPatrolcheckService {

	@Autowired
	private MesChiefdataPatrolcheckMapper mesChiefdataPatrolcheckMapper;
	@Autowired
	private MesPatrolcheckTimespanMapper mesPatrolcheckTimespanMapper;
	
	@Override
	@Transactional
	public void saveMain(MesChiefdataPatrolcheck mesChiefdataPatrolcheck, List<MesPatrolcheckTimespan> mesPatrolcheckTimespanList) {
		mesChiefdataPatrolcheckMapper.insert(mesChiefdataPatrolcheck);
		if(mesPatrolcheckTimespanList!=null && mesPatrolcheckTimespanList.size()>0) {
			for(MesPatrolcheckTimespan entity:mesPatrolcheckTimespanList) {
				//外键设置
				entity.setPatrolId(mesChiefdataPatrolcheck.getId());
				mesPatrolcheckTimespanMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesChiefdataPatrolcheck mesChiefdataPatrolcheck,List<MesPatrolcheckTimespan> mesPatrolcheckTimespanList) {
		mesChiefdataPatrolcheckMapper.updateById(mesChiefdataPatrolcheck);
		
		//1.先删除子表数据
		mesPatrolcheckTimespanMapper.deleteByMainId(mesChiefdataPatrolcheck.getId());
		
		//2.子表数据重新插入
		if(mesPatrolcheckTimespanList!=null && mesPatrolcheckTimespanList.size()>0) {
			for(MesPatrolcheckTimespan entity:mesPatrolcheckTimespanList) {
				//外键设置
				entity.setPatrolId(mesChiefdataPatrolcheck.getId());
				mesPatrolcheckTimespanMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesPatrolcheckTimespanMapper.deleteByMainId(id);
		mesChiefdataPatrolcheckMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesPatrolcheckTimespanMapper.deleteByMainId(id.toString());
			mesChiefdataPatrolcheckMapper.deleteById(id);
		}
	}
	
}
