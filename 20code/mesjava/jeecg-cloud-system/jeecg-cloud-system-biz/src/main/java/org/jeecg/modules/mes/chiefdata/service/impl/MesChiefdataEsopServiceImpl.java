package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataEsop;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataEsopMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataEsopService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—ESOP
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesChiefdataEsopServiceImpl extends ServiceImpl<MesChiefdataEsopMapper, MesChiefdataEsop> implements IMesChiefdataEsopService {

}
