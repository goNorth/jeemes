package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesPrintModel;
import org.jeecg.modules.mes.chiefdata.mapper.MesPrintModelMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesPrintModelService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—打印模版
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesPrintModelServiceImpl extends ServiceImpl<MesPrintModelMapper, MesPrintModel> implements IMesPrintModelService {

}
