package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataEsop;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—ESOP
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface IMesChiefdataEsopService extends IService<MesChiefdataEsop> {

}
