package org.jeecg.modules.mes.storage.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBomitem;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMateriel;
import org.jeecg.modules.mes.client.ProduceClient;
import org.jeecg.modules.mes.client.SystemClient;
import org.jeecg.modules.mes.client.TransactionClient;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.jeecg.modules.mes.order.entity.MesOrderPurchase;
import org.jeecg.modules.mes.order.entity.MesOrderSale;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.jeecg.modules.mes.storage.entity.MesStockManage;
import org.jeecg.modules.mes.storage.entity.MesStorageWholesale;
import org.jeecg.modules.mes.storage.service.IMesStockManageService;
import org.jeecg.modules.mes.storage.service.IMesStorageWholesaleService;
import org.jeecg.modules.mes.storage.vo.*;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.epms.util.ObjectHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "仓库报表api")
@RestController
@RequestMapping("/storage/reportapi")
@Slf4j
public class ReportApi {

    @Autowired
    private IMesStorageWholesaleService mesStorageWholesaleService;

    @Autowired
    SystemClient systemClient;

    @Autowired
    TransactionClient transactionClient;

    @Autowired
    ProduceClient produceClient;

    @Autowired
    private IMesStockManageService mesStockManageService;


    @AutoLog(value = "仓库报表api-生产物料使用报表")
    @ApiOperation(value = "仓库报表api-生产物料使用报表", notes = "仓库报表api-生产物料使用报表")
    @GetMapping(value = "/materUseReport")
    public Result<?> materUseReport(MesStorageWholesale mesStorageWholesale,
                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        QueryWrapper<MesStorageWholesale> w1 = new QueryWrapper<>();
        w1.select("id", "base_dockettype", "product_code", "product_name", "ware_site", "inware_num", "unit", "query2", "create_time", "query4", "create_by", "case base_dockettype when '扫描上料' then glaze_id else query5 end as glaze_id");
        w1.in("base_dockettype", "扫描上料", "扫描发料");
        if (StringUtils.isNotEmpty(mesStorageWholesale.getQuery4())) {
            //制令单id
            w1.eq("query4", mesStorageWholesale.getQuery4());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getProductCode())) {
            w1.eq("product_code", mesStorageWholesale.getProductCode());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getGlazeId())) {
            //物料二维码
            w1.and(i -> i.eq("glaze_id", mesStorageWholesale.getGlazeId()).or().eq("query5", mesStorageWholesale.getGlazeId()));
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getBaseDockettype())) {
            w1.eq("base_dockettype", mesStorageWholesale.getBaseDockettype());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getQuery1())) {
            //开始时间
            w1.ge("create_time", mesStorageWholesale.getQuery1());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getQuery2())) {
            //结束时间
            w1.le("create_time", mesStorageWholesale.getQuery2());
        }
        w1.orderByDesc("create_time");
        Page<MesStorageWholesale> page = new Page<MesStorageWholesale>(pageNo, pageSize);
        IPage<MesStorageWholesale> pageList = mesStorageWholesaleService.page(page, w1);
        for (MesStorageWholesale item : pageList.getRecords()) {
            MesCommandbillInfo entity = produceClient.getById(item.getQuery4());
            if (ObjectHelper.isNotEmpty(entity)) {
                item.setQuery4(entity.getCommandbillCode());
            }
        }
        return Result.ok(pageList);
    }


    @AutoLog(value = "仓库报表api-物料库存列表")
    @ApiOperation(value = "仓库报表api-物料库存列表", notes = "仓库报表api-物料库存列表")
    @GetMapping(value = "/MaterWareList")
    public Result<?> MaterWareList(MesStorageWholesale mesStorageWholesale,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        QueryWrapper<MesStorageWholesale> w1 = new QueryWrapper<>();
        w1.select("id", "product_code", "product_name", "ware_site", "inware_num", "unit", "batch_num", "client_code", "shelf_code", "case shelf_state when '0' then '未出库' else '已出库' end as shelf_state",
                "case when update_by is null then create_by else update_by end as update_by", "case when update_time is null then create_time else update_time end as update_time");
        w1.eq("base_dockettype", "收货打印");
        w1.isNotNull("shelf_state");
        if (StringUtils.isNotEmpty(mesStorageWholesale.getProductCode())) {
            //物料代码
            w1.eq("product_code", mesStorageWholesale.getProductCode());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getId())) {
            //物料二维码
            w1.eq("id", mesStorageWholesale.getId());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getClientCode())) {
            //客户物料号
            w1.like("client_code", mesStorageWholesale.getClientCode());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getBatchNum())) {
            //批次号
            w1.like("batch_num", mesStorageWholesale.getBatchNum());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getWareSite())) {
            //仓库
            w1.like("ware_site", mesStorageWholesale.getWareSite());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getCreateBy())) {
            //开始时间
            w1.ge("create_time", mesStorageWholesale.getCreateBy());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getUpdateBy())) {
            //结束时间
            w1.le("create_time", mesStorageWholesale.getUpdateBy());
        }
        w1.orderByDesc("case shelf_state when '0' then 1 else 0 end");
        w1.orderByDesc("create_time");
        Page<MesStorageWholesale> page = new Page<MesStorageWholesale>(pageNo, pageSize);
        IPage<MesStorageWholesale> pageList = mesStorageWholesaleService.page(page, w1);
        return Result.ok(pageList);
    }

    @AutoLog(value = "仓库报表api-生产物料使用报表导出")
    @ApiOperation(value = "仓库报表api-生产物料使用报表导出", notes = "仓库报表api-生产物料使用报表导出")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesStorageWholesale mesStorageWholesale, HttpServletResponse response) {
        // Step.1 组装查询条件查询数据
        QueryWrapper<MesStorageWholesale> w1 = new QueryWrapper<>();
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        w1.select("id", "base_dockettype", "product_code", "product_name", "ware_site", "inware_num", "unit", "query2", "create_time", "query4", "create_by", "case base_dockettype when '扫描上料' then glaze_id else query5 end as glaze_id");
        w1.in("base_dockettype", "扫描上料", "扫描发料");
        if (StringUtils.isNotEmpty(mesStorageWholesale.getQuery4())) {
            //制令单id
            w1.eq("query4", mesStorageWholesale.getQuery4());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getProductCode())) {
            w1.eq("product_code", mesStorageWholesale.getProductCode());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getGlazeId())) {
            //物料二维码
            w1.and(i -> i.eq("glaze_id", mesStorageWholesale.getGlazeId()).or().eq("query5", mesStorageWholesale.getGlazeId()));
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getBaseDockettype())) {
            w1.eq("base_dockettype", mesStorageWholesale.getBaseDockettype());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getQuery1())) {
            //开始时间
            w1.ge("create_time", mesStorageWholesale.getQuery1());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getQuery2())) {
            //结束时间
            w1.le("create_time", mesStorageWholesale.getQuery2());
        }
        w1.orderByDesc("create_time");

        //Step.2 获取导出数据
        List<MesStorageWholesale> queryList = mesStorageWholesaleService.list(w1);

        List<MaterUseReport> exlist = new ArrayList<MaterUseReport>();
        for (MesStorageWholesale m : queryList) {
            MaterUseReport m1 = new MaterUseReport();
            BeanUtils.copyProperties(m, m1);
            exlist.add(m1);
        }

        // Step.4 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "生产物料使用报表");
        mv.addObject(NormalExcelConstants.CLASS, MaterUseReport.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("生产物料使用报表", "导出人:" + sysUser.getRealname(), "生产物料使用报表"));
        mv.addObject(NormalExcelConstants.DATA_LIST, exlist);
        return mv;
    }

    @AutoLog(value = "仓库报表api-原材料入库报表导出")
    @ApiOperation(value = "仓库报表api-原材料入库报表导出", notes = "仓库报表api-原材料入库报表导出")
    @RequestMapping(value = "/exportYclLsBbXls")
    public ModelAndView exportYclLsBbXls(HttpServletRequest request, MesStorageWholesale mesStorageWholesale, HttpServletResponse response) {
        // Step.1 组装查询条件查询数据
        QueryWrapper<MesStorageWholesale> w1 = new QueryWrapper<>();
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //w1.select("id", "product_code", "product_name", "ware_site", "inware_num", "unit", "create_time", "base_rownum", "create_by", "ifnull(query1,'PASS') as query1", "query3", "ifnull(query2,'PASS') as query2", "query4");
        if (StringUtils.isNotEmpty(mesStorageWholesale.getProductCode())) {
            w1.eq("product_code", mesStorageWholesale.getProductCode());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getAreaCode())) {
            w1.eq("area_code", mesStorageWholesale.getAreaCode());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getLocationCode())) {
            w1.like("location_code", mesStorageWholesale.getLocationCode());
        }
        w1.orderByDesc("create_time");

        //Step.2 获取导出数据
        List<MesStorageWholesale> queryList = mesStorageWholesaleService.list(w1);

        /*List<PcbUseReport> exlist = new ArrayList<PcbUseReport>();
        for (MesStorageWholesale m : queryList) {
            PcbUseReport m1 = new PcbUseReport();
            BeanUtils.copyProperties(m, m1);
            exlist.add(m1);
        }*/

        // Step.4 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "原材料入库报表");
        mv.addObject(NormalExcelConstants.CLASS, MesStorageWholesale.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("原材料入库报表", "导出人:" + sysUser.getRealname(), "原材料入库报表"));
        mv.addObject(NormalExcelConstants.DATA_LIST, queryList);
        return mv;
    }

    @AutoLog(value = "仓库报表api-PCB使用报表汇总")
    @ApiOperation(value = "仓库报表api-PCB使用报表汇总", notes = "仓库报表api-PCB使用报表汇总")
    @GetMapping(value = "/pcbUseTotalReport")
    public Result<?> pcbUseTotalReport(MesStorageWholesale mesStorageWholesale,
                                       @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                       @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        QueryWrapper<MesStorageWholesale> w1 = new QueryWrapper<>();
        w1.select("base_rownum", "product_code", "product_name", "query5", "count(1) as query1");
        w1.eq("base_dockettype", "制令单线程扫描");
        if (StringUtils.isNotEmpty(mesStorageWholesale.getBaseRownum())) {
            //制令单id
            w1.eq("base_rownum", mesStorageWholesale.getBaseRownum());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getProductCode())) {
            w1.eq("product_code", mesStorageWholesale.getProductCode());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getId())) {
            //PCB二维码
            w1.like("id", mesStorageWholesale.getId());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getQuery5())) {
            //线别
            w1.like("query5", mesStorageWholesale.getQuery5());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getCreateBy())) {
            //开始时间
            w1.ge("create_time", mesStorageWholesale.getCreateBy());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getUpdateBy())) {
            //结束时间
            w1.le("create_time", mesStorageWholesale.getUpdateBy());
        }
        w1.groupBy("query5", "base_rownum", "product_code", "product_name");
        Page<MesStorageWholesale> page = new Page<MesStorageWholesale>(pageNo, pageSize);
        IPage<MesStorageWholesale> pageList = mesStorageWholesaleService.page(page, w1);
        List<MesStorageWholesale> ret = pageList.getRecords();
        Map<String, String> comap = new HashMap<>();
        for (MesStorageWholesale sale : ret) {
            if (comap.get(sale.getBaseRownum()) == null) {
                MesCommandbillInfo info = produceClient.getById(sale.getBaseRownum());
                if (info != null) {
                    comap.put(sale.getBaseRownum(), info.getCommandbillCode());
                    sale.setBaseRownum(info.getCommandbillCode());
                }
            } else {
                sale.setBaseRownum(comap.get(sale.getBaseRownum()));
            }
        }
        pageList.setRecords(ret);
        return Result.ok(pageList);
    }


    @AutoLog(value = "仓库报表api-PCB使用报表")
    @ApiOperation(value = "仓库报表api-PCB使用报表", notes = "仓库报表api-PCB使用报表")
    @GetMapping(value = "/pcbUseReport")
    public Result<?> pcbUseReport(MesStorageWholesale mesStorageWholesale,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        QueryWrapper<MesStorageWholesale> w1 = new QueryWrapper<>();
        w1.select("id", "product_code", "product_name", "ware_site", "inware_num", "unit", "create_time", "base_rownum", "create_by", "ifnull(query1,'PASS') as query1", "query3", "ifnull(query2,'PASS') as query2", "query4", "query5");
        w1.eq("base_dockettype", "制令单线程扫描");
        if (StringUtils.isNotEmpty(mesStorageWholesale.getBaseRownum())) {
            //制令单id
            w1.eq("base_rownum", mesStorageWholesale.getBaseRownum());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getProductCode())) {
            w1.eq("product_code", mesStorageWholesale.getProductCode());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getId())) {
            //PCB二维码
            w1.like("id", mesStorageWholesale.getId());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getQuery5())) {
            //线别
            w1.like("query5", mesStorageWholesale.getQuery5());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getCreateBy())) {
            //开始时间
            w1.ge("create_time", mesStorageWholesale.getCreateBy());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getUpdateBy())) {
            //结束时间
            w1.le("create_time", mesStorageWholesale.getUpdateBy());
        }
        w1.orderByDesc("create_time");
        Page<MesStorageWholesale> page = new Page<MesStorageWholesale>(pageNo, pageSize);
        IPage<MesStorageWholesale> pageList = mesStorageWholesaleService.page(page, w1);
        List<MesStorageWholesale> ret = pageList.getRecords();
        Map<String, String> comap = new HashMap<>();
        for (MesStorageWholesale sale : ret) {
            if (comap.get(sale.getBaseRownum()) == null) {
                MesCommandbillInfo info = produceClient.getById(sale.getBaseRownum());
                if (info != null) {
                    comap.put(sale.getBaseRownum(), info.getCommandbillCode());
                    sale.setBaseRownum(info.getCommandbillCode());
                }
            } else {
                sale.setBaseRownum(comap.get(sale.getBaseRownum()));
            }
        }
        pageList.setRecords(ret);

//        w1= new QueryWrapper<>();
//        w1.select("query5","count(1) as query1");
//        w1.eq("base_dockettype","制令单线程扫描");
//        if(StringUtils.isNotEmpty(mesStorageWholesale.getBaseRownum())){
//            //制令单id
//            w1.eq("base_rownum",mesStorageWholesale.getBaseRownum());
//        }
//        if(StringUtils.isNotEmpty(mesStorageWholesale.getProductCode())){
//            w1.eq("product_code",mesStorageWholesale.getProductCode());
//        }
//        if(StringUtils.isNotEmpty(mesStorageWholesale.getId())){
//            //PCB二维码
//            w1.like("id",mesStorageWholesale.getId());
//        }
//        if(StringUtils.isNotEmpty(mesStorageWholesale.getQuery5())){
//            //线别
//            w1.like("query5",mesStorageWholesale.getQuery5());
//        }
//        if(StringUtils.isNotEmpty(mesStorageWholesale.getCreateBy())){
//            //开始时间
//            w1.ge("create_time",mesStorageWholesale.getCreateBy());
//        }
//        if(StringUtils.isNotEmpty(mesStorageWholesale.getUpdateBy())){
//            //结束时间
//            w1.le("create_time",mesStorageWholesale.getUpdateBy());
//        }
//        w1.groupBy("query5");
//
//        List<MesStorageWholesale> tjlist = mesStorageWholesaleService.list(w1);
//        String tjstr = "";
//        for(MesStorageWholesale sale :tjlist){
//            tjstr+=sale.getQuery5()+":"+sale.getQuery1()+"  ";
//        }
//        Map retmap = new HashMap<>();
//        retmap.put("list",pageList);
//        retmap.put("tjstr",tjstr);
        return Result.ok(pageList);
    }

    @AutoLog(value = "仓库报表api-PCB扫描记录修改")
    @ApiOperation(value = "仓库报表api-PCB扫描记录修改", notes = "仓库报表api-PCB扫描记录修改")
    @PostMapping(value = "/pcbUseEdit")
    public Result<?> pcbUseEdit(@RequestBody MesStorageWholesale mesStorageWholesale) {
        MesStorageWholesale sto = mesStorageWholesaleService.getById(mesStorageWholesale.getId());
        if (sto == null) {
            return Result.error("修改失败，无记录");
        }
        sto.setQuery1(mesStorageWholesale.getQuery1());
        sto.setQuery2(mesStorageWholesale.getQuery2());
        sto.setQuery3(mesStorageWholesale.getQuery3());
        sto.setQuery4(mesStorageWholesale.getQuery4());
        mesStorageWholesaleService.updateById(sto);
        return Result.ok("修改成功");
    }


    @AutoLog(value = "仓库报表api-PCB使用报表导出")
    @ApiOperation(value = "仓库报表api-PCB使用报表导出", notes = "仓库报表api-PCB使用报表导出")
    @RequestMapping(value = "/exportPcbXls")
    public ModelAndView exportPcbXls(HttpServletRequest request, MesStorageWholesale mesStorageWholesale, HttpServletResponse response) {
        // Step.1 组装查询条件查询数据
        QueryWrapper<MesStorageWholesale> w1 = new QueryWrapper<>();
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        w1.select("id", "product_code", "product_name", "ware_site", "inware_num", "unit", "create_time", "base_rownum", "create_by", "ifnull(query1,'PASS') as query1", "query3", "ifnull(query2,'PASS') as query2", "query4");
        w1.eq("base_dockettype", "制令单线程扫描");
        if (StringUtils.isNotEmpty(mesStorageWholesale.getBaseRownum())) {
            //制令单id
            w1.eq("base_rownum", mesStorageWholesale.getBaseRownum());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getProductCode())) {
            w1.eq("product_code", mesStorageWholesale.getProductCode());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getId())) {
            //PCB二维码
            w1.like("id", mesStorageWholesale.getId());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getCreateBy())) {
            //开始时间
            w1.ge("create_time", mesStorageWholesale.getCreateBy());
        }
        if (StringUtils.isNotEmpty(mesStorageWholesale.getUpdateBy())) {
            //结束时间
            w1.le("create_time", mesStorageWholesale.getUpdateBy());
        }
        w1.orderByDesc("create_time");

        //Step.2 获取导出数据
        List<MesStorageWholesale> queryList = mesStorageWholesaleService.list(w1);

        List<PcbUseReport> exlist = new ArrayList<PcbUseReport>();
        for (MesStorageWholesale m : queryList) {
            PcbUseReport m1 = new PcbUseReport();
            BeanUtils.copyProperties(m, m1);
            exlist.add(m1);
        }

        // Step.4 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "PCB使用报表");
        mv.addObject(NormalExcelConstants.CLASS, PcbUseReport.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("PCB使用报表", "导出人:" + sysUser.getRealname(), "PCB使用报表"));
        mv.addObject(NormalExcelConstants.DATA_LIST, exlist);
        return mv;
    }

    @AutoLog(value = "仓库报表api-atp计算缺料情况")
    @ApiOperation(value = "仓库报表api-atp计算缺料情况", notes = "仓库报表api-atp计算缺料情况")
    @GetMapping(value = "/atpCount")
    public Result<?> atpCount(@RequestParam(name = "mater", required = true) String mater,
                              @RequestParam(name = "num", required = true) String num) {
//        String mater = mesStorageWholesale.getProductCode();
//        String num = mesStorageWholesale.getInwareNum();
        if (StringUtils.isEmpty(mater) || StringUtils.isEmpty(num)) {
            return Result.error("请选择物料和生产数量");
        }
        BigDecimal pronum = new BigDecimal(num);
        List<MesChiefdataBomitem> list = systemClient.selectByMaterialCode(mater);
        if (list == null || list.size() == 0) {
            return Result.error("物料没有配置bom");
        }
        List<String> maters = new ArrayList<>();
        for (MesChiefdataBomitem item : list) {
            maters.add(item.getMaterielCode());
            BigDecimal xnum = pronum.multiply(new BigDecimal(item.getQuantity()));
            item.setFrontQuantity(xnum + "");
        }
        QueryWrapper<MesStockManage> w1 = new QueryWrapper<>();
        w1.in("materiel_code", maters);
        List<MesStockManage> slist = mesStockManageService.list(w1);
        BigDecimal maxpronum = new BigDecimal("0");
        String flag = "正常";

        for (MesChiefdataBomitem item : list) {
            item.setPositionNum("0");
            item.setSpot("缺料");
        }
        if (slist.size() == 0) {
            flag = "缺料";
        } else {
            for (MesChiefdataBomitem item : list) {
                for (MesStockManage stock : slist) {
                    if (item.getMaterielCode().equals(stock.getMaterielCode())) {
                        item.setPositionNum(stock.getStockNum());
                        if (new BigDecimal(stock.getStockNum()).compareTo(new BigDecimal(item.getFrontQuantity())) < 0) {
                            flag = "缺料";
                            item.setSpot("缺料");
                        } else {
                            item.setSpot("正常");
                        }
                        BigDecimal nn = BigDecimal.ZERO;
                        if (!"0".equals(item.getQuantity().trim())) {
                            nn = new BigDecimal(stock.getStockNum()).divide(new BigDecimal(item.getQuantity()), 2, BigDecimal.ROUND_HALF_UP);
                        }
                        if (maxpronum.compareTo(new BigDecimal("0")) == 0) {
                            maxpronum = nn;
                        } else if (nn.compareTo(maxpronum) < 0) {
                            maxpronum = nn;
                        }
                        break;
                    }
                }
            }
        }

        Map ret = new HashMap();
        ret.put("flag", flag);
        ret.put("maxnum", maxpronum + "");
        ret.put("retlist", list);
        return Result.ok(ret);
    }

    @AutoLog(value = "仓库报表api-成品入库单")
    @ApiOperation(value = "仓库报表api-成品入库单", notes = "仓库报表api-成品入库单")
    @GetMapping(value = "/selectCpRkd")
    public Result<?> selectCpRkd(@RequestParam(name = "begindate", required = false) String begindate,
                                 @RequestParam(name = "attr1", required = false) String attr1,
                                 @RequestParam(name = "attr2", required = false) String attr2,
                                 @RequestParam(name = "attr4", required = false) String attr4,
                                 @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                 @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        List<String> attrs = new ArrayList<>();
        if (StringUtils.isNotBlank(attr1)) {
            List<MesCommandbillInfo> infos = produceClient.queryCommandBillInfoCode(attr1);
            if (infos.size() > 0) {
                for (int i = 0; i < infos.size(); i++) {
                    attrs.add(infos.get(i).getId());
                }
            } else {
                return Result.error("未找到该单号信息！");
            }
        }
        Page<ReportVo> page = new Page<ReportVo>(pageNo, pageSize);
        Page<ReportVo> retpage = mesStorageWholesaleService.selectCpRkd(page, begindate, attrs, attr2, attr4);
        List<ReportVo> ret = retpage.getRecords();
        for (int i = 0; i < ret.size(); i++) {
            MesChiefdataBomitem item = systemClient.getMCodebomitemForIn(ret.get(i).getAttr2());
            if (item != null) {
                ret.get(i).setAttr3(item.getClientCode());
            }
            MesCommandbillInfo command = produceClient.getById(ret.get(i).getAttr1());
            if (command != null) {
                ret.get(i).setAttr1(command.getCommandbillCode());
                ret.get(i).setAttr9(command.getClientName());
            }
        }
        retpage.setRecords(ret);
        return Result.ok(retpage);
    }


    @AutoLog(value = "仓库报表api-成品出库单")
    @ApiOperation(value = "仓库报表api-成品出库单", notes = "仓库报表api-成品出库单")
    @GetMapping(value = "/selectCpChd")
    public Result<?> selectCpChd(@RequestParam(name = "attr1", required = false) String attr1,
                                 @RequestParam(name = "attr2", required = false) String attr2,
                                 @RequestParam(name = "attr4", required = false) String attr4,
                                 @RequestParam(name = "begindate", required = false) String begindate,
                                 @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                 @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        List<String> attrs = new ArrayList<>();
        if (StringUtils.isNotBlank(attr1)) {
            List<MesOrderSale> infos = transactionClient.queryMesSaleOrderByCode(attr1);
            if (infos.size() > 0) {
                for (int i = 0; i < infos.size(); i++) {
                    attrs.add(infos.get(i).getId());
                }
            } else {
                return Result.error("未找到该单号信息！");
            }
        }
        Page<ReportVo> page = new Page<ReportVo>(pageNo, pageSize);
        Page<ReportVo> retpage = mesStorageWholesaleService.selectCpChd(page, begindate, attrs, attr2, attr4);
        List<ReportVo> ret = retpage.getRecords();
        for (int i = 0; i < ret.size(); i++) {
            MesChiefdataMateriel item = systemClient.queryByMcode(ret.get(i).getAttr2());
            if (item != null) {
                ret.get(i).setAttr3(item.getQuery2());
                ret.get(i).setAttr4(item.getProductName());
                ret.get(i).setAttr5(item.getGauge());
                ret.get(i).setAttr6(item.getUnit());
            }
            MesOrderSale sale = transactionClient.queryMesSaleOrderById(ret.get(i).getAttr1());
            if (sale != null) {
                ret.get(i).setAttr1(sale.getOrderCode());
                ret.get(i).setAttr11(sale.getSaleName());
            }
        }
        retpage.setRecords(ret);
        return Result.ok(retpage);
    }

    @AutoLog(value = "仓库报表api-生产领料单")
    @ApiOperation(value = "仓库报表api-生产领料单", notes = "仓库报表api-生产领料单")
    @GetMapping(value = "/selectLld")
    public Result<?> selectLld(@RequestParam(name = "commadid", required = false) String commadid,
                               @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                               @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {

        Page<ReportVo> page = new Page<ReportVo>(pageNo, pageSize);
        Page<ReportVo> retpage = mesStorageWholesaleService.selectLld(page, commadid);
        List<ReportVo> ret = retpage.getRecords();
        for (int i = 0; i < ret.size(); i++) {
            MesChiefdataBomitem item = systemClient.getMCodebomitemForIn(ret.get(i).getAttr2());
            if (item != null) {
                ret.get(i).setAttr2(item.getClientCode());
                ret.get(i).setAttr4(item.getMaterielGauge());
                ret.get(i).setAttr5(item.getQuantity());
            }
            MesCommandbillInfo command = produceClient.getById(ret.get(i).getAttr8());
            if (command != null && StringUtils.isNotEmpty(ret.get(i).getAttr5())) {
                BigDecimal num = new BigDecimal(ret.get(i).getAttr5()).multiply(new BigDecimal(command.getPlantNum()));
                ret.get(i).setAttr6(num + "");
                ret.get(i).setAttr8(command.getCommandbillCode());

                MesOrderProduce pro = transactionClient.queryMesProduceById(command.getProduceId());
                if (pro != null) {
                    ret.get(i).setAttr9(pro.getClient());
                    ret.get(i).setAttr10(pro.getOrderCode());
                    ret.get(i).setAttr11(pro.getMaterielName());
                    ret.get(i).setAttr12(pro.getInputNum());
                }
            }
        }
        retpage.setRecords(ret);
        return Result.ok(retpage);
    }


    @AutoLog(value = "仓库报表api-退料单")
    @ApiOperation(value = "仓库报表api-退料单", notes = "仓库报表api-退料单")
    @GetMapping(value = "/selectTld")
    public Result<?> selectTld(@RequestParam(name = "commadid", required = false) String commadid,
                               @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                               @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {

        return Result.ok("");
    }

    @AutoLog(value = "仓库报表api-原材料流水报表")
    @ApiOperation(value = "仓库报表api-原材料流水报表", notes = "仓库报表api-原材料流水报表")
    @GetMapping(value = "/selectLsbb")
    public Result<?> selectLsbb(@RequestParam(name = "materid", required = false) String materid,
                                @RequestParam(name = "areaCode", required = false) String areaCode,
                                @RequestParam(name = "locationCode", required = false) String locationCode,
                                @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {

        Page<ReportVo> page = new Page<ReportVo>(pageNo, pageSize);
        Page<ReportVo> retpage = mesStorageWholesaleService.selectLsbb(page, materid, areaCode, locationCode);
        List<ReportVo> ret = retpage.getRecords();
        for (int i = 0; i < ret.size(); i++) {
            MesChiefdataBomitem item = systemClient.getMCodebomitemForIn(ret.get(i).getAttr1());
            if (item != null) {
                ret.get(i).setAttr2(item.getClientCode());
                ret.get(i).setAttr4(item.getMaterielGauge());
                ret.get(i).setAttr5(item.getVoltage());
                ret.get(i).setAttr6(item.getDifferences());
                ret.get(i).setAttr9(item.getMaterialType());
            }
            MesOrderPurchase pur = transactionClient.getOrderPurchaseById(ret.get(i).getAttr10());
            if (pur != null) {
                ret.get(i).setAttr10(pur.getOrderCode());
            }
        }
        retpage.setRecords(ret);
        return Result.ok(retpage);
    }

    @AutoLog(value = "仓库报表api-成品进销存")
    @ApiOperation(value = "仓库报表api-成品进销存", notes = "仓库报表api-成品进销存")
    @GetMapping(value = "/selectCpJxc")
    public Result<?> selectCpJxc(@RequestParam(name = "materid", required = false) String materid,
                                 @RequestParam(name = "areaCode", required = false) String areaCode,
                                 @RequestParam(name = "locationCode", required = false) String locationCode,
                                 @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                 @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {

        Page<ReportVo> page = new Page<ReportVo>(pageNo, pageSize);
        Page<ReportVo> retpage = mesStorageWholesaleService.selectCpJxc(page, materid, areaCode, locationCode);
        List<ReportVo> ret = retpage.getRecords();
        for (int i = 0; i < ret.size(); i++) {
            MesChiefdataMateriel item = systemClient.queryByMcode(ret.get(i).getAttr1());
            if (item != null) {
                ret.get(i).setAttr2(item.getQuery2());
                ret.get(i).setAttr4(item.getGauge());
//                ret.get(i).setAttr5(item.getVoltage());
//                ret.get(i).setAttr6(item.getDifferences());
                ret.get(i).setAttr13(item.getMaterielType());
            }
        }
        retpage.setRecords(ret);
        return Result.ok(retpage);
    }

    @AutoLog(value = "仓库报表api-原材料进销存")
    @ApiOperation(value = "仓库报表api-原材料进销存", notes = "仓库报表api-原材料进销存")
    @GetMapping(value = "/selectYclJxc")
    public Result<?> selectYclJxc(@RequestParam(name = "materid", required = false) String materid,
                                  @RequestParam(name = "areaCode", required = false) String areaCode,
                                  @RequestParam(name = "locationCode", required = false) String locationCode,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {

        Page<ReportVo> page = new Page<ReportVo>(pageNo, pageSize);
        Page<ReportVo> retPage = mesStorageWholesaleService.selectYclJxc(page, materid, areaCode, locationCode);
        List<ReportVo> ret = retPage.getRecords();
        for (int i = 0; i < ret.size(); i++) {
            MesChiefdataBomitem item = systemClient.getMCodebomitemForIn(ret.get(i).getAttr1());
            if (item != null) {
                ret.get(i).setAttr2(item.getClientCode());
                ret.get(i).setAttr4(item.getMaterielGauge());
                ret.get(i).setAttr5(item.getVoltage());
                ret.get(i).setAttr6(item.getDifferences());
                ret.get(i).setAttr13(item.getMaterialType());
            }
        }
        retPage.setRecords(ret);
        return Result.ok(retPage);
    }

    @AutoLog(value = "仓库报表api-原材料进销存报表导出")
    @ApiOperation(value = "仓库报表api-原材料进销存报表导出", notes = "仓库报表api-原材料进销存报表导出")
    @PostMapping(value = "/exportXlsYclJxc")
    public ModelAndView exportXlsYclJxc(@RequestBody YclJxcReportVO vo) {
        // Step.1 组装查询条件查询数据
        List<ReportVo> queryList = mesStorageWholesaleService.exportXlsYclJxc(vo);

        for (int i = 0; i < queryList.size(); i++) {
            MesChiefdataBomitem item = systemClient.getMCodebomitemForIn(queryList.get(i).getAttr1());
            if (item != null) {
                queryList.get(i).setAttr2(item.getClientCode());
                queryList.get(i).setAttr4(item.getMaterielGauge());
                queryList.get(i).setAttr5(item.getVoltage());
                queryList.get(i).setAttr6(item.getDifferences());
                queryList.get(i).setAttr13(item.getMaterialType());
            }
        }

        //Step.2 获取导出数据
        List<YclJxcReport> exList = new ArrayList<YclJxcReport>();
        for (ReportVo m : queryList) {
            YclJxcReport m1 = new YclJxcReport();
            BeanUtils.copyProperties(m, m1);
            exList.add(m1);
        }
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // Step.4 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "原材料进销存报表");
        mv.addObject(NormalExcelConstants.CLASS, YclJxcReport.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("原材料进销存报表", "导出人:" + sysUser.getRealname(), "原材料进销存报表"));
        mv.addObject(NormalExcelConstants.DATA_LIST, exList);
        return mv;
    }

    @AutoLog(value = "仓库报表api-原材料入库单")
    @ApiOperation(value = "仓库报表api-原材料入库单", notes = "仓库报表api-原材料入库单")
    @GetMapping(value = "/selectYclRkd")
    public Result<?> selectYclRkd(@RequestParam(name = "attr1", required = false) String attr1,
                                  @RequestParam(name = "begindate", required = false) String begindate,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        long startTime = System.currentTimeMillis();

        Page<ReportVo> page = new Page<ReportVo>(pageNo, pageSize);
        List<String> ids = new ArrayList<>();
        //单号不为空，则模糊查询出来
        if (StringUtils.isNotBlank(attr1)) {
            ids = transactionClient.getOrderPurchaseLikeCode(attr1);
            if (ObjectHelper.isNotEmpty(ids)) {
                attr1 = null;
            }
        }
        Page<ReportVo> retpage = mesStorageWholesaleService.selectYclRkd(page, begindate, ids, attr1);
        List<ReportVo> ret = retpage.getRecords();
        for (int i = 0; i < ret.size(); i++) {
            MesChiefdataBomitem item = systemClient.getMCodebomitemForIn(ret.get(i).getAttr2());
            if (item != null) {
                ret.get(i).setAttr6(item.getVoltage());
                ret.get(i).setAttr7(item.getDifferences());
            }
            MesOrderPurchase pur = transactionClient.getOrderPurchaseById(ret.get(i).getAttr1());
            if (pur != null) {
                ret.get(i).setAttr1(pur.getOrderCode());
                ret.get(i).setAttr12(pur.getSupplierName());
            }
        }
        retpage.setRecords(ret);

        long endTime = System.currentTimeMillis();
        float excTime = (float) (endTime - startTime) / 1000;
        System.out.println("执行selectYclRkd controller时间：" + excTime + "s");
        return Result.ok(retpage);
    }

    @AutoLog(value = "仓库报表api-入库单报表导出")
    @ApiOperation(value = "仓库报表api-入库单报表导出", notes = "仓库报表api-入库单报表导出")
    @PostMapping(value = "/exportYclRkd")
    public ModelAndView exportYclRkd(@RequestBody YclRkdReportVO vo) {
        // Step.1 组装查询条件查询数据
        //Step.2 获取导出数据
        List<YclRkdReport> exList = mesStorageWholesaleService.exportYclRkd(vo);
        for (int i = 0; i < exList.size(); i++) {
            MesChiefdataBomitem item = systemClient.getMCodebomitemForIn(exList.get(i).getAttr2());
            if (item != null) {
                exList.get(i).setAttr6(item.getVoltage());
                exList.get(i).setAttr7(item.getDifferences());
            }
            MesOrderPurchase pur = transactionClient.getOrderPurchaseById(exList.get(i).getAttr1());
            if (pur != null) {
                exList.get(i).setAttr1(pur.getOrderCode());
                exList.get(i).setAttr12(pur.getSupplierName());
            }
        }
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // Step.4 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "入库单报表");
        mv.addObject(NormalExcelConstants.CLASS, YclRkdReport.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("入库单报表", "导出人:" + sysUser.getRealname(), "入库单报表"));
        mv.addObject(NormalExcelConstants.DATA_LIST, exList);
        return mv;
    }

}
