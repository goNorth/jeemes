package org.jeecg.modules.mes.storage.mapper;

import java.util.List;
import org.jeecg.modules.mes.storage.entity.MesCountItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 仓库管理—盘点子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface MesCountItemMapper extends BaseMapper<MesCountItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesCountItem> selectByMainId(@Param("mainId") String mainId);
}
