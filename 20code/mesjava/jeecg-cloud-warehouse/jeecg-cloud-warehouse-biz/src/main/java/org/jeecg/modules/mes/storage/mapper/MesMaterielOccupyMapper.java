package org.jeecg.modules.mes.storage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.storage.entity.MesMaterielOccupy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 仓库管理-领料清单
 * @Author: jeecg-boot
 * @Date:   2020-11-16
 * @Version: V1.0
 */
public interface MesMaterielOccupyMapper extends BaseMapper<MesMaterielOccupy> {

}
