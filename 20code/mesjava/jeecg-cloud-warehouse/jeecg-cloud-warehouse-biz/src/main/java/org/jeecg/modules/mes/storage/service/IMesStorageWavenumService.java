package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesWavenumItem;
import org.jeecg.modules.mes.storage.entity.MesStorageWavenum;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 仓库管理—波次单
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesStorageWavenumService extends IService<MesStorageWavenum> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesStorageWavenum mesStorageWavenum,List<MesWavenumItem> mesWavenumItemList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesStorageWavenum mesStorageWavenum,List<MesWavenumItem> mesWavenumItemList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
