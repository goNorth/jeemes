package org.jeecg.modules.mes.storage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.mes.client.TransactionClient;
import org.jeecg.modules.mes.order.entity.MesPurchaseItem;
import org.jeecg.modules.mes.storage.entity.MesCertificateItem;
import org.jeecg.modules.mes.storage.entity.MesCertificatePerk;
import org.jeecg.modules.mes.storage.mapper.MesCertificateItemMapper;
import org.jeecg.modules.mes.storage.mapper.MesCertificatePerkMapper;
import org.jeecg.modules.mes.storage.service.IMesCertificateItemService;
import org.jeecg.modules.mes.storage.service.IMesCertificatePerkService;
import org.jeecg.modules.mes.storage.service.IMesStorageWholesaleService;
import org.jeecg.modules.mes.storage.utils.AsyncUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 物料凭证抬头
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesCertificatePerkServiceImpl extends ServiceImpl<MesCertificatePerkMapper, MesCertificatePerk> implements IMesCertificatePerkService {

	@Autowired
	private MesCertificatePerkMapper mesCertificatePerkMapper;
	@Autowired
	private MesCertificateItemMapper mesCertificateItemMapper;
	@Autowired
	private IMesCertificateItemService mesCertificateItemService;
	@Autowired
	private IMesStorageWholesaleService storageWholesaleService;
	@Autowired
	TransactionClient transactionClient;

	@Autowired
	private AsyncUtils asyncUtils;
	
	@Override
	@Transactional
	public void delMain(String id) {
		mesCertificateItemMapper.deleteByMainId(id);
		mesCertificatePerkMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesCertificateItemMapper.deleteByMainId(id.toString());
			mesCertificatePerkMapper.deleteById(id);
		}
	}
	/**
	 * 根据原制令单号和新制令单号，查询凭证抬头
	 * @param oldCode
	 * @param nowCode
	 * @return
	 */
	@Override
	public List<MesCertificatePerk> getChangeProduce(String oldCode,String nowCode){
		return mesCertificatePerkMapper.getChangeProduce(oldCode,nowCode);
	}

	/**
	 * 没有该物料的凭证项目，就新增物料凭证项目，否则修改
	 * @param mesCertificateItem
	 * @return
	 */
	@Override
	public MesCertificateItem addReceiveItem(MesCertificateItem mesCertificateItem){
		String perkId = mesCertificateItem.getPerkId();
		String mobileType = mesCertificateItem.getMobileType();
		String materielCode = mesCertificateItem.getMaterielCode();
		System.out.println("addReceiveItem:"+mesCertificateItem);
		if (StringUtils.isNotBlank(perkId) && StringUtils.isNotBlank(mobileType)) {
			//查询凭证抬头信息，判断是否质检完成，完成的话，就修改为质检未完成，入库未完成
			/*MesCertificatePerk perkinfo = this.getById(perkId);
			if(perkinfo!=null) {
				if (StringUtils.isBlank(perkinfo.getIfFinish())) {
					perkinfo.setIfFinish("未完成");
				}
				if (StringUtils.isBlank(perkinfo.getIfInput())) {
					perkinfo.setIfInput("未完成");
				}
				if ("质检完成".equals(perkinfo.getIfFinish())) {
					perkinfo.setIfFinish("未完成");
					perkinfo.setIfInput("未完成");
				}
				this.updateById(perkinfo);
			}*/
			//查询凭证抬头子表信息
			QueryWrapper<MesCertificateItem> wrapper = new QueryWrapper<>();
			if (StringUtils.isNotBlank(materielCode)) {
				wrapper.eq("perk_id", perkId).eq("mobile_type", mobileType).eq("materiel_code", materielCode);
			} else {
				wrapper.eq("perk_id", perkId).eq("mobile_type", mobileType);
			}
			MesCertificateItem certificateItem = mesCertificateItemService.getOne(wrapper);
			String mobileCode = "103";
			if (certificateItem == null) {
				//103 收货
				if (mesCertificateItem.getMobileCode().equals(mobileCode)) {
					//通过预留编号（采购订单子表ID），获取采购订单子表的相应数据
					MesPurchaseItem purchaseItem = transactionClient.queryMesPurchaseItemById(mesCertificateItem.getReserveCode());
					if (purchaseItem != null) {
						//获取未收货数量，与收货数量做对比判断
						BigDecimal unreceiveNum = new BigDecimal(purchaseItem.getUnreceiveNum());//未收货数量
						if (StringUtils.isNotBlank(mesCertificateItem.getInputNum())) {

							BigDecimal receiveNum = new BigDecimal(mesCertificateItem.getInputNum());//收货数量
							BigDecimal remainNum = unreceiveNum.subtract(receiveNum);// 剩余数量=未收货数量-收货数量
							if (remainNum.compareTo(BigDecimal.ZERO) == -1) {
								throw new RuntimeException("请检查收货数量是否正确！");
							} else if (remainNum.compareTo(BigDecimal.ZERO) == 0) {
//								purchaseItem.setUnreceiveNum(remainNum.toString());
//								purchaseItem.setIfFinish("收货完成");
//								transactionClient.editPurchaseItem(purchaseItem);
								storageWholesaleService.updateNumStatePurchaseItemId(remainNum.toString(),purchaseItem.getId());
								//收货完成，向工作人员发送质检提醒；目前测试环境，向当前登录用户发送消息提醒，上线后修改
//								LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//								systemClient.sendCheckMessage(sysUser.getUsername(), purchaseItem.getMaterielName(), purchaseItem.getPurchaseNum(), purchaseItem.getOrderUnit());
								asyncUtils.asyncSend(purchaseItem);

								//收货完成，改变凭证状态，开放质检模块
								MesCertificatePerk mesCertificatePerk = this.getById(perkId);
								mesCertificatePerk.setIfFinish("未完成");
								this.updateById(mesCertificatePerk);
							} else {
//								purchaseItem.setUnreceiveNum(remainNum.toString());
//								transactionClient.editPurchaseItem(purchaseItem);
								storageWholesaleService.updateNumPurchaseItemId(remainNum.toString(),purchaseItem.getId());
							}
						}
						mesCertificateItem.setIfInspect("否");
						mesCertificateItem.setIfStorage("否");
						String inputNum = mesCertificateItem.getInputNum();//数量
						String unit = purchaseItem.getOrderUnit();//单位
						BeanUtils.copyProperties(purchaseItem, mesCertificateItem);
						mesCertificateItem.setId(null);//主键设置为null
						mesCertificateItem.setUnstorageNum(inputNum);// 未入库数量 = 录入数量
//						mesCertificateItem.setInputNum(purchaseItem.getPurchaseNum());//录入数量 = 采购数量
						mesCertificateItem.setInputUnit(unit);// 单位
						mesCertificateItemService.save(mesCertificateItem);
						return mesCertificateItem;
					} else {
						throw new RuntimeException("没有找到采购订单子表的数据！请检查预留编号是否正确！");
					}
				} else {
					throw new RuntimeException("收货时移动编号不等于103！请检查");
				}
			} else {
				if (certificateItem.getMobileCode().equals(mobileCode)) {
					//通过预留编号（采购订单子表ID），获取采购订单子表的相应数据
					MesPurchaseItem purchaseItem = transactionClient.queryMesPurchaseItemById(mesCertificateItem.getReserveCode());
					if (purchaseItem != null) {
						//获取未收货数量，与收货数量做对比判断
						BigDecimal unreceiveNum = new BigDecimal(purchaseItem.getUnreceiveNum());//未收货数量
						if (StringUtils.isNotBlank(mesCertificateItem.getInputNum())) {
							BigDecimal oldNum = new BigDecimal(certificateItem.getInputNum());//已有数量
							BigDecimal receiveNum = new BigDecimal(mesCertificateItem.getInputNum());//收货数量
							BigDecimal remainNum = unreceiveNum.subtract(receiveNum);
							BigDecimal inputNumNew = oldNum.add(receiveNum);
							if (remainNum.compareTo(BigDecimal.ZERO) == -1) {
								throw new RuntimeException("请检查收货数量是否正确！");
							} else if (remainNum.compareTo(BigDecimal.ZERO) == 0) {
//								purchaseItem.setUnreceiveNum(remainNum.toString());
//								purchaseItem.setIfFinish("收货完成");
//								transactionClient.editPurchaseItem(purchaseItem);
								storageWholesaleService.updateNumStatePurchaseItemId(remainNum.toString(),purchaseItem.getId());
								certificateItem.setInputNum(inputNumNew.toString());//入库数量
								certificateItem.setUnstorageNum(inputNumNew.toString());//未入库数量
								//收货完成，向工作人员发送质检提醒；目前测试环境，向当前登录用户发送消息提醒，上线后修改
//								LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//								systemClient.sendCheckMessage(sysUser.getUsername(), purchaseItem.getMaterielName(), purchaseItem.getPurchaseNum(), purchaseItem.getOrderUnit());
								asyncUtils.asyncSend(purchaseItem);

								//收货完成，改变凭证状态，开放质检模块
								MesCertificatePerk mesCertificatePerk = this.getById(perkId);
								mesCertificatePerk.setIfFinish("未完成");
								this.updateById(mesCertificatePerk);

							} else {
//								purchaseItem.setUnreceiveNum(remainNum.toString());
//								transactionClient.editPurchaseItem(purchaseItem);
								storageWholesaleService.updateNumPurchaseItemId(remainNum.toString(),purchaseItem.getId());
								certificateItem.setInputNum(inputNumNew.toString());//入库数量
								certificateItem.setUnstorageNum(inputNumNew.toString());//未入库数量
							}
						}
						mesCertificateItemService.updateById(certificateItem);
						return certificateItem;
					} else {
						throw new RuntimeException("没有找到采购订单子表的数据！请检查预留编号是否正确！");
					}
				} else {
					throw new RuntimeException("收货时移动编号不等于103！请检查");
				}
			}
		} else {
			throw new RuntimeException("没有找到凭证抬头ID和移动类型！请检查");
		}
	}

	/**
	 * 没有该物料的凭证项目，就新增物料凭证项目，否则修改 扫描收货使用
	 * @param mesCertificateItem
	 * @return
	 */
	@Override
	public MesCertificateItem addReceiveItem2(MesCertificateItem mesCertificateItem){
		String perkId = mesCertificateItem.getPerkId();
		String mobileType = mesCertificateItem.getMobileType();
		String materielCode = mesCertificateItem.getMaterielCode();
		System.out.println("addReceiveItem:"+mesCertificateItem);
		if (StringUtils.isNotBlank(perkId) && StringUtils.isNotBlank(mobileType)) {
			//查询凭证抬头信息，判断是否质检完成，完成的话，就修改为质检未完成，入库未完成
			/*MesCertificatePerk perkinfo = this.getById(perkId);
			if(perkinfo!=null) {
				if (StringUtils.isBlank(perkinfo.getIfFinish())) {
					perkinfo.setIfFinish("未完成");
				}
				if (StringUtils.isBlank(perkinfo.getIfInput())) {
					perkinfo.setIfInput("未完成");
				}
				if ("质检完成".equals(perkinfo.getIfFinish())) {
					perkinfo.setIfFinish("未完成");
					perkinfo.setIfInput("未完成");
				}
				this.updateById(perkinfo);
			}*/
			//查询凭证抬头子表信息
			QueryWrapper<MesCertificateItem> wrapper = new QueryWrapper<>();
			wrapper.eq("perk_id", perkId).eq("mobile_type", mobileType).eq("materiel_code", materielCode).eq("if_inspect","否");
			List<MesCertificateItem> list = mesCertificateItemService.list(wrapper);

			MesCertificateItem certificateItem = null;
			if (list.size()>0){
				certificateItem=list.get(0);
			}

			String mobileCode = "103";
			if (certificateItem == null) {
				//103 收货
				if (mesCertificateItem.getMobileCode().equals(mobileCode)) {
					//通过预留编号（采购订单子表ID），获取采购订单子表的相应数据
					MesPurchaseItem purchaseItem = transactionClient.queryMesPurchaseItemById(mesCertificateItem.getReserveCode());
					if (purchaseItem != null) {
						//获取未收货数量，与收货数量做对比判断
						BigDecimal unreceiveNum = new BigDecimal(purchaseItem.getUnreceiveNum());//未收货数量
						if (StringUtils.isNotBlank(mesCertificateItem.getInputNum())) {

							BigDecimal receiveNum = new BigDecimal(mesCertificateItem.getInputNum());//收货数量
							BigDecimal remainNum = unreceiveNum.subtract(receiveNum);// 剩余数量=未收货数量-收货数量
							if (remainNum.compareTo(BigDecimal.ZERO) == -1) {
								throw new RuntimeException("请检查收货数量是否正确！");
							} else if (remainNum.compareTo(BigDecimal.ZERO) == 0) {
//								purchaseItem.setUnreceiveNum(remainNum.toString());
//								purchaseItem.setIfFinish("收货完成");
//								transactionClient.editPurchaseItem(purchaseItem);
								storageWholesaleService.updateNumStatePurchaseItemId(remainNum.toString(),purchaseItem.getId());
								//收货完成，向工作人员发送质检提醒；目前测试环境，向当前登录用户发送消息提醒，上线后修改
//								LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//								systemClient.sendCheckMessage(sysUser.getUsername(), purchaseItem.getMaterielName(), purchaseItem.getPurchaseNum(), purchaseItem.getOrderUnit());
								asyncUtils.asyncSend(purchaseItem);

								//收货完成，改变凭证状态，开放质检模块
								MesCertificatePerk mesCertificatePerk = this.getById(perkId);
								mesCertificatePerk.setIfFinish("未完成");
								this.updateById(mesCertificatePerk);
							} else {
//								purchaseItem.setUnreceiveNum(remainNum.toString());
//								transactionClient.editPurchaseItem(purchaseItem);
								storageWholesaleService.updateNumPurchaseItemId(remainNum.toString(),purchaseItem.getId());
							}
						}
						mesCertificateItem.setIfInspect("否");
						mesCertificateItem.setIfStorage("否");
						String inputNum = mesCertificateItem.getInputNum();//数量
						String unit = purchaseItem.getOrderUnit();//单位
						BeanUtils.copyProperties(purchaseItem, mesCertificateItem);
						mesCertificateItem.setId(null);//主键设置为null
						mesCertificateItem.setUnstorageNum(inputNum);// 未入库数量 = 录入数量
//						mesCertificateItem.setInputNum(purchaseItem.getPurchaseNum());//录入数量 = 采购数量
						mesCertificateItem.setInputUnit(unit);// 单位
						mesCertificateItemService.save(mesCertificateItem);
						return mesCertificateItem;
					} else {
						throw new RuntimeException("没有找到采购订单子表的数据！请检查预留编号是否正确！");
					}
				} else {
					throw new RuntimeException("收货时移动编号不等于103！请检查");
				}
			} else {
				if (certificateItem.getMobileCode().equals(mobileCode)) {
					//通过预留编号（采购订单子表ID），获取采购订单子表的相应数据
					MesPurchaseItem purchaseItem = transactionClient.queryMesPurchaseItemById(mesCertificateItem.getReserveCode());
					if (purchaseItem != null) {
						//获取未收货数量，与收货数量做对比判断
						BigDecimal unreceiveNum = new BigDecimal(purchaseItem.getUnreceiveNum());//未收货数量
						if (StringUtils.isNotBlank(mesCertificateItem.getInputNum())) {
							BigDecimal oldNum = new BigDecimal(certificateItem.getInputNum());//已有数量
							BigDecimal receiveNum = new BigDecimal(mesCertificateItem.getInputNum());//收货数量
							BigDecimal remainNum = unreceiveNum.subtract(receiveNum);
							BigDecimal inputNumNew = oldNum.add(receiveNum);
							if (remainNum.compareTo(BigDecimal.ZERO) == -1) {
								throw new RuntimeException("请检查收货数量是否正确！");
							} else if (remainNum.compareTo(BigDecimal.ZERO) == 0) {
//								purchaseItem.setUnreceiveNum(remainNum.toString());
//								purchaseItem.setIfFinish("收货完成");
//								transactionClient.editPurchaseItem(purchaseItem);
								storageWholesaleService.updateNumStatePurchaseItemId(remainNum.toString(),purchaseItem.getId());
								certificateItem.setInputNum(inputNumNew.toString());//入库数量
								certificateItem.setUnstorageNum(inputNumNew.toString());//未入库数量
								//收货完成，向工作人员发送质检提醒；目前测试环境，向当前登录用户发送消息提醒，上线后修改
//								LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//								systemClient.sendCheckMessage(sysUser.getUsername(), purchaseItem.getMaterielName(), purchaseItem.getPurchaseNum(), purchaseItem.getOrderUnit());
								asyncUtils.asyncSend(purchaseItem);

								//收货完成，改变凭证状态，开放质检模块
								MesCertificatePerk mesCertificatePerk = this.getById(perkId);
								mesCertificatePerk.setIfFinish("未完成");
								this.updateById(mesCertificatePerk);

							} else {
//								purchaseItem.setUnreceiveNum(remainNum.toString());
//								transactionClient.editPurchaseItem(purchaseItem);
								storageWholesaleService.updateNumPurchaseItemId(remainNum.toString(),purchaseItem.getId());
								certificateItem.setInputNum(inputNumNew.toString());//入库数量
								certificateItem.setUnstorageNum(inputNumNew.toString());//未入库数量
							}
							asyncUtils.asyncSend(purchaseItem);
						}
						mesCertificateItemService.updateById(certificateItem);
						return certificateItem;
					} else {
						throw new RuntimeException("没有找到采购订单子表的数据！请检查预留编号是否正确！");
					}
				} else {
					throw new RuntimeException("收货时移动编号不等于103！请检查");
				}
			}
		} else {
			throw new RuntimeException("没有找到凭证抬头ID和移动类型！请检查");
		}
	}

}
