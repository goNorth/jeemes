package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesStorageBatchwaresite;
import org.jeecg.modules.mes.storage.mapper.MesStorageBatchwaresiteMapper;
import org.jeecg.modules.mes.storage.service.IMesStorageBatchwaresiteService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 仓库管理—批次库位库存
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesStorageBatchwaresiteServiceImpl extends ServiceImpl<MesStorageBatchwaresiteMapper, MesStorageBatchwaresite> implements IMesStorageBatchwaresiteService {

}
