package org.jeecg.modules.mes.storage.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.storage.entity.MesMaterielTraceability;
import org.jeecg.modules.mes.storage.service.IMesMaterielTraceabilityService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 物料追溯表
 * @Author: jeecg-boot
 * @Date:   2021-06-02
 * @Version: V1.0
 */
@Api(tags="物料追溯表")
@RestController
@RequestMapping("/storage/mesMaterielTraceability")
@Slf4j
public class MesMaterielTraceabilityController extends JeecgController<MesMaterielTraceability, IMesMaterielTraceabilityService> {
	@Autowired
	private IMesMaterielTraceabilityService mesMaterielTraceabilityService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesMaterielTraceability
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@AutoLog(value = "物料追溯表-分页列表查询")
	@ApiOperation(value="物料追溯表-分页列表查询", notes="物料追溯表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesMaterielTraceability mesMaterielTraceability,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize) {

		return mesMaterielTraceabilityService.queryPageList(mesMaterielTraceability, pageNo, pageSize);

		/*QueryWrapper<MesMaterielTraceability> queryWrapper = QueryGenerator.initQueryWrapper(mesMaterielTraceability, req.getParameterMap());
		if (StringUtils.isNotBlank(mesMaterielTraceability.getProductName())) {
			queryWrapper.like("product_name",mesMaterielTraceability.getProductName());
		}
		if (StringUtils.isNotBlank(mesMaterielTraceability.getMaterielGauge())) {
			queryWrapper.like("materiel_gauge",mesMaterielTraceability.getMaterielGauge());
		}
		if (StringUtils.isNotBlank(mesMaterielTraceability.getSupplierName())) {
			queryWrapper.like("supplier_name",mesMaterielTraceability.getSupplierName());
		}
		Page<MesMaterielTraceability> page = new Page<MesMaterielTraceability>(pageNo, pageSize);
		IPage<MesMaterielTraceability> pageList = mesMaterielTraceabilityService.page(page, queryWrapper);
		return Result.ok(pageList);*/
	}
	
	/**
	 *   添加
	 *
	 * @param mesMaterielTraceability
	 * @return
	 */
	@AutoLog(value = "物料追溯表-添加")
	@ApiOperation(value="物料追溯表-添加", notes="物料追溯表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesMaterielTraceability mesMaterielTraceability) {
		mesMaterielTraceabilityService.save(mesMaterielTraceability);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesMaterielTraceability
	 * @return
	 */
	@AutoLog(value = "物料追溯表-编辑")
	@ApiOperation(value="物料追溯表-编辑", notes="物料追溯表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesMaterielTraceability mesMaterielTraceability) {
		mesMaterielTraceabilityService.updateById(mesMaterielTraceability);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料追溯表-通过id删除")
	@ApiOperation(value="物料追溯表-通过id删除", notes="物料追溯表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesMaterielTraceabilityService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "物料追溯表-批量删除")
	@ApiOperation(value="物料追溯表-批量删除", notes="物料追溯表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesMaterielTraceabilityService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料追溯表-通过id查询")
	@ApiOperation(value="物料追溯表-通过id查询", notes="物料追溯表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesMaterielTraceability mesMaterielTraceability = mesMaterielTraceabilityService.getById(id);
		if(mesMaterielTraceability==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesMaterielTraceability);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesMaterielTraceability
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesMaterielTraceability mesMaterielTraceability) {
        return super.exportXls(request, mesMaterielTraceability, MesMaterielTraceability.class, "物料追溯表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesMaterielTraceability.class);
    }

}
