package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesCountItem;
import org.jeecg.modules.mes.storage.mapper.MesCountItemMapper;
import org.jeecg.modules.mes.storage.service.IMesCountItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 仓库管理—盘点子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesCountItemServiceImpl extends ServiceImpl<MesCountItemMapper, MesCountItem> implements IMesCountItemService {
	
	@Autowired
	private MesCountItemMapper mesCountItemMapper;
	
	@Override
	public List<MesCountItem> selectByMainId(String mainId) {
		return mesCountItemMapper.selectByMainId(mainId);
	}
}
