package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesWavenumItem;
import org.jeecg.modules.mes.storage.mapper.MesWavenumItemMapper;
import org.jeecg.modules.mes.storage.service.IMesWavenumItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 仓库管理—波次单子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesWavenumItemServiceImpl extends ServiceImpl<MesWavenumItemMapper, MesWavenumItem> implements IMesWavenumItemService {
	
	@Autowired
	private MesWavenumItemMapper mesWavenumItemMapper;
	
	@Override
	public List<MesWavenumItem> selectByMainId(String mainId) {
		return mesWavenumItemMapper.selectByMainId(mainId);
	}
}
