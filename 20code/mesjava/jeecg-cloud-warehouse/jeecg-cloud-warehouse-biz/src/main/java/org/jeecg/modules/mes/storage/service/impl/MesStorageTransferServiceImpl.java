package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesStorageTransfer;
import org.jeecg.modules.mes.storage.entity.MesTransferItem;
import org.jeecg.modules.mes.storage.mapper.MesTransferItemMapper;
import org.jeecg.modules.mes.storage.mapper.MesStorageTransferMapper;
import org.jeecg.modules.mes.storage.service.IMesStorageTransferService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 仓库管理—调拨移动
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesStorageTransferServiceImpl extends ServiceImpl<MesStorageTransferMapper, MesStorageTransfer> implements IMesStorageTransferService {

	@Autowired
	private MesStorageTransferMapper mesStorageTransferMapper;
	@Autowired
	private MesTransferItemMapper mesTransferItemMapper;
	
	@Override
	@Transactional
	public void saveMain(MesStorageTransfer mesStorageTransfer, List<MesTransferItem> mesTransferItemList) {
		mesStorageTransferMapper.insert(mesStorageTransfer);
		if(mesTransferItemList!=null && mesTransferItemList.size()>0) {
			for(MesTransferItem entity:mesTransferItemList) {
				//外键设置
				entity.setTransferId(mesStorageTransfer.getId());
				mesTransferItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesStorageTransfer mesStorageTransfer,List<MesTransferItem> mesTransferItemList) {
		mesStorageTransferMapper.updateById(mesStorageTransfer);
		
		//1.先删除子表数据
		mesTransferItemMapper.deleteByMainId(mesStorageTransfer.getId());
		
		//2.子表数据重新插入
		if(mesTransferItemList!=null && mesTransferItemList.size()>0) {
			for(MesTransferItem entity:mesTransferItemList) {
				//外键设置
				entity.setTransferId(mesStorageTransfer.getId());
				mesTransferItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesTransferItemMapper.deleteByMainId(id);
		mesStorageTransferMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesTransferItemMapper.deleteByMainId(id.toString());
			mesStorageTransferMapper.deleteById(id);
		}
	}
	
}
