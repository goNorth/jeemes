package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesShelfIpConfig;
import org.jeecg.modules.mes.storage.mapper.MesShelfIpConfigMapper;
import org.jeecg.modules.mes.storage.service.IMesShelfIpConfigService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: mes_shelf_ip_config
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
@Service
public class MesShelfIpConfigServiceImpl extends ServiceImpl<MesShelfIpConfigMapper, MesShelfIpConfig> implements IMesShelfIpConfigService {

}
