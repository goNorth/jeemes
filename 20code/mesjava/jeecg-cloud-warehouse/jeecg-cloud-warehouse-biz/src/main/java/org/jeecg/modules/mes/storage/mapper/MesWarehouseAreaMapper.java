package org.jeecg.modules.mes.storage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.storage.entity.MesWarehouseArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 仓库区域表
 * @Author: jeecg-boot
 * @Date:   2021-05-13
 * @Version: V1.0
 */
public interface MesWarehouseAreaMapper extends BaseMapper<MesWarehouseArea> {
    List<MesWarehouseArea> selectByAreaCode(@Param("areaCode") String areaCode);
}
