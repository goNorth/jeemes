package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesStorageAssign;
import org.jeecg.modules.mes.storage.mapper.MesStorageAssignMapper;
import org.jeecg.modules.mes.storage.service.IMesStorageAssignService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 仓库管理—拣配单
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesStorageAssignServiceImpl extends ServiceImpl<MesStorageAssignMapper, MesStorageAssign> implements IMesStorageAssignService {

}
