package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesTransferItem;
import org.jeecg.modules.mes.storage.entity.MesStorageTransfer;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 仓库管理—调拨移动
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesStorageTransferService extends IService<MesStorageTransfer> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesStorageTransfer mesStorageTransfer,List<MesTransferItem> mesTransferItemList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesStorageTransfer mesStorageTransfer,List<MesTransferItem> mesTransferItemList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
