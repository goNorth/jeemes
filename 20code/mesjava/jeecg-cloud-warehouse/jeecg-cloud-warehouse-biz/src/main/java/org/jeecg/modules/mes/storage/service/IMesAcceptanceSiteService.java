package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesAcceptanceSite;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 仓库管理—库位子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesAcceptanceSiteService extends IService<MesAcceptanceSite> {

	public List<MesAcceptanceSite> selectByMainId(String mainId);
}
