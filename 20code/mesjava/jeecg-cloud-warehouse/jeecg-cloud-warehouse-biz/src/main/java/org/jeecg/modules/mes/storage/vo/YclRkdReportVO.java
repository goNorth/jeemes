package org.jeecg.modules.mes.storage.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class YclRkdReportVO implements Serializable {

    @ApiModelProperty(value = "入库时间")
    private String begindate;

    @ApiModelProperty(value = "订单编号")
    private String attr1;

    @ApiModelProperty(value = "ids")
    private List<String> ids;
}
