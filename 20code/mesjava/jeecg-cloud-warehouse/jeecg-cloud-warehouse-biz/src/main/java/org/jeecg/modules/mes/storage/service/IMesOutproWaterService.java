package org.jeecg.modules.mes.storage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.order.entity.MesOutproWater;

/**
 * @Description: 出货流水报表
 * @Author: jeecg-boot
 * @Date:   2021-03-08
 * @Version: V1.0
 */
public interface IMesOutproWaterService extends IService<MesOutproWater> {

}
