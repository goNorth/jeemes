package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesStoragePreassign;
import org.jeecg.modules.mes.storage.mapper.MesStoragePreassignMapper;
import org.jeecg.modules.mes.storage.service.IMesStoragePreassignService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 仓库管理—预配明细
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesStoragePreassignServiceImpl extends ServiceImpl<MesStoragePreassignMapper, MesStoragePreassign> implements IMesStoragePreassignService {

}
