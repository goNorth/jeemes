package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesCheckprojectType;
import org.jeecg.modules.mes.inspect.mapper.MesCheckprojectTypeMapper;
import org.jeecg.modules.mes.inspect.service.IMesCheckprojectTypeService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 检测类型信息
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesCheckprojectTypeServiceImpl extends ServiceImpl<MesCheckprojectTypeMapper, MesCheckprojectType> implements IMesCheckprojectTypeService {
	
	@Autowired
	private MesCheckprojectTypeMapper mesCheckprojectTypeMapper;
	
	@Override
	public List<MesCheckprojectType> selectByMainId(String mainId) {
		return mesCheckprojectTypeMapper.selectByMainId(mainId);
	}
}
