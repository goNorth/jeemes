package org.jeecg.modules.mes.iqc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mes.iqc.entity.MesIqcMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 来料检验物料详情
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
public interface MesIqcMaterialMapper extends BaseMapper<MesIqcMaterial> {

    @Select("select * from mes_iqc_material where iqc_id=#{mainId}")
    public List<MesIqcMaterial> selectMaterialByMainId(@Param("mainId") String mainId);
}
