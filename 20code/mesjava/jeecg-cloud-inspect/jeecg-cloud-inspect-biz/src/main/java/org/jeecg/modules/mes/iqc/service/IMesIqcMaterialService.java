package org.jeecg.modules.mes.iqc.service;

import org.jeecg.modules.mes.iqc.entity.MesIqcMaterial;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 来料检验物料详情
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
public interface IMesIqcMaterialService extends IService<MesIqcMaterial> {

}
