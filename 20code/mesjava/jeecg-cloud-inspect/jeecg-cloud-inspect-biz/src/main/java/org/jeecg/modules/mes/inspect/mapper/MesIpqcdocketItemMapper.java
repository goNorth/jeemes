package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;
import org.jeecg.modules.mes.inspect.entity.MesIpqcdocketItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 单据明细
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface MesIpqcdocketItemMapper extends BaseMapper<MesIpqcdocketItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesIpqcdocketItem> selectByMainId(@Param("mainId") String mainId);

}
