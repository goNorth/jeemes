package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesIpqcdocketItem;
import org.jeecg.modules.mes.inspect.mapper.MesIpqcdocketItemMapper;
import org.jeecg.modules.mes.inspect.service.IMesIpqcdocketItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 单据明细
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesIpqcdocketItemServiceImpl extends ServiceImpl<MesIpqcdocketItemMapper, MesIpqcdocketItem> implements IMesIpqcdocketItemService {
	
	@Autowired
	private MesIpqcdocketItemMapper mesIpqcdocketItemMapper;
	
	@Override
	public List<MesIpqcdocketItem> selectByMainId(String mainId) {
		return mesIpqcdocketItemMapper.selectByMainId(mainId);
	}
}
