package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesQualityAbnormal;
import org.jeecg.modules.mes.inspect.mapper.MesQualityAbnormalMapper;
import org.jeecg.modules.mes.inspect.service.IMesQualityAbnormalService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 质检中心-品质异常单
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesQualityAbnormalServiceImpl extends ServiceImpl<MesQualityAbnormalMapper, MesQualityAbnormal> implements IMesQualityAbnormalService {

}
