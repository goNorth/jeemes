package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesIqcReceive;
import org.jeecg.modules.mes.inspect.mapper.MesIqcReceiveMapper;
import org.jeecg.modules.mes.inspect.service.IMesIqcReceiveService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 品质报表—IQC来料检验
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
@Service
public class MesIqcReceiveServiceImpl extends ServiceImpl<MesIqcReceiveMapper, MesIqcReceive> implements IMesIqcReceiveService {

}
