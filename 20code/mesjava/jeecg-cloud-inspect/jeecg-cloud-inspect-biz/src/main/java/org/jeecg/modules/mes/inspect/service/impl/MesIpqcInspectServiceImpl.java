package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesIpqcInspect;
import org.jeecg.modules.mes.inspect.mapper.MesIpqcInspectMapper;
import org.jeecg.modules.mes.inspect.service.IMesIpqcInspectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 质检中心-IPQC检测项
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesIpqcInspectServiceImpl extends ServiceImpl<MesIpqcInspectMapper, MesIpqcInspect> implements IMesIpqcInspectService {

    @Autowired
    private MesIpqcInspectMapper mesIpqcInspectMapper;

    public String inspectTotle(){
        return mesIpqcInspectMapper.inspectTotle();
    }
    public String inspectOutTotle(){
        return mesIpqcInspectMapper.inspectOutTotle();
    }
    public String inspectIqcOutSum(){
        return mesIpqcInspectMapper.inspectIqcOutSum();
    }
}
