package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.inspect.entity.PatrolInspectLog;
import org.jeecg.modules.mes.inspect.service.IPatrolInspectLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: 巡检模块记录
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
@Api(tags="巡检模块记录")
@RestController
@RequestMapping("/inspect/patrolInspectLog")
@Slf4j
public class PatrolInspectLogController extends JeecgController<PatrolInspectLog, IPatrolInspectLogService> {

	@Autowired
	private IPatrolInspectLogService patrolInspectLogService;
	
	/**
	 * 分页列表查询
	 *
	 * @param patrolInspectLog
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "巡检模块记录-分页列表查询")
	@ApiOperation(value="巡检模块记录-分页列表查询", notes="巡检模块记录-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PatrolInspectLog patrolInspectLog,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PatrolInspectLog> queryWrapper = QueryGenerator.initQueryWrapper(patrolInspectLog, req.getParameterMap());
		Page<PatrolInspectLog> page = new Page<PatrolInspectLog>(pageNo, pageSize);
		IPage<PatrolInspectLog> pageList = patrolInspectLogService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param patrolInspectLog
	 * @return
	 */
	@AutoLog(value = "巡检模块记录-添加")
	@ApiOperation(value="巡检模块记录-添加", notes="巡检模块记录-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PatrolInspectLog patrolInspectLog) {
		patrolInspectLogService.save(patrolInspectLog);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param patrolInspectLog
	 * @return
	 */
	@AutoLog(value = "巡检模块记录-编辑")
	@ApiOperation(value="巡检模块记录-编辑", notes="巡检模块记录-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PatrolInspectLog patrolInspectLog) {
		patrolInspectLogService.updateById(patrolInspectLog);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "巡检模块记录-通过id删除")
	@ApiOperation(value="巡检模块记录-通过id删除", notes="巡检模块记录-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		patrolInspectLogService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "巡检模块记录-批量删除")
	@ApiOperation(value="巡检模块记录-批量删除", notes="巡检模块记录-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.patrolInspectLogService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "巡检模块记录-通过id查询")
	@ApiOperation(value="巡检模块记录-通过id查询", notes="巡检模块记录-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PatrolInspectLog patrolInspectLog = patrolInspectLogService.getById(id);
		if(patrolInspectLog==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(patrolInspectLog);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param patrolInspectLog
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PatrolInspectLog patrolInspectLog) {
        return super.exportXls(request, patrolInspectLog, PatrolInspectLog.class, "巡检模块记录");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PatrolInspectLog.class);
    }

}
