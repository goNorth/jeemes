package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.MesMaterielCheckprojrct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 质检中心-物料检测项目
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface MesMaterielCheckprojrctMapper extends BaseMapper<MesMaterielCheckprojrct> {

}
