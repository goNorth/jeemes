package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesMaterielChecktype;
import org.jeecg.modules.mes.inspect.entity.MesCheckprojectType;
import org.jeecg.modules.mes.inspect.mapper.MesCheckprojectTypeMapper;
import org.jeecg.modules.mes.inspect.mapper.MesMaterielChecktypeMapper;
import org.jeecg.modules.mes.inspect.service.IMesMaterielChecktypeService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 质检中心-物料检测类型
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesMaterielChecktypeServiceImpl extends ServiceImpl<MesMaterielChecktypeMapper, MesMaterielChecktype> implements IMesMaterielChecktypeService {

	@Autowired
	private MesMaterielChecktypeMapper mesMaterielChecktypeMapper;
	@Autowired
	private MesCheckprojectTypeMapper mesCheckprojectTypeMapper;
	
	@Override
	@Transactional
	public void delMain(String id) {
		mesCheckprojectTypeMapper.deleteByMainId(id);
		mesMaterielChecktypeMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesCheckprojectTypeMapper.deleteByMainId(id.toString());
			mesMaterielChecktypeMapper.deleteById(id);
		}
	}
	
}
