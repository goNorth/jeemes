package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesSampleCheck;
import org.jeecg.modules.mes.inspect.mapper.MesSampleCheckMapper;
import org.jeecg.modules.mes.inspect.service.IMesSampleCheckService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 品质报表—抽测
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
@Service
public class MesSampleCheckServiceImpl extends ServiceImpl<MesSampleCheckMapper, MesSampleCheck> implements IMesSampleCheckService {

}
