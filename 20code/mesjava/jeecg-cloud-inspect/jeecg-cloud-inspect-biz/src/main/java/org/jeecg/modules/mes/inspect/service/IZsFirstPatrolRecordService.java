package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.ZsFirstPatrolRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 宗申首、巡、末件检验记录表
 * @Author: jeecg-boot
 * @Date:   2021-05-15
 * @Version: V1.0
 */
public interface IZsFirstPatrolRecordService extends IService<ZsFirstPatrolRecord> {

}
