package org.jeecg.modules.mes.inspect.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.mes.inspect.entity.MaterielRetreatReason;
import org.jeecg.modules.mes.inspect.entity.MaterielRetreatRecord;
import org.jeecg.modules.mes.inspect.mapper.MaterielRetreatReasonMapper;
import org.jeecg.modules.mes.inspect.mapper.MaterielRetreatRecordMapper;
import org.jeecg.modules.mes.inspect.service.IMaterielRetreatRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;

/**
 * @Description: 物料退料记录表
 * @Author: jeecg-boot
 * @Date:   2021-04-25
 * @Version: V1.0
 */
@Service
public class MaterielRetreatRecordServiceImpl extends ServiceImpl<MaterielRetreatRecordMapper, MaterielRetreatRecord> implements IMaterielRetreatRecordService {

	@Autowired
	private MaterielRetreatRecordMapper materielRetreatRecordMapper;
	@Autowired
	private MaterielRetreatReasonMapper materielRetreatReasonMapper;
	
	@Override
	@Transactional
	public void delMain(String id) {
		materielRetreatReasonMapper.deleteByMainId(id);
		materielRetreatRecordMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			materielRetreatReasonMapper.deleteByMainId(id.toString());
			materielRetreatRecordMapper.deleteById(id);
		}
	}
	@Override
	@Transactional
	public boolean saveAll(MaterielRetreatRecord retreatRecord){
		if (StringUtils.isBlank(retreatRecord.getState())){
			retreatRecord.setState("1");
		}
		System.out.println(retreatRecord);
		if("0".equals(retreatRecord.getState())){//质检前
			/*if(*//*StringUtils.isBlank(retreatRecord.getClientCode())||*//*
					StringUtils.isBlank(retreatRecord.getOrderCode())||
					*//*StringUtils.isBlank(retreatRecord.getMaterielName())||
					StringUtils.isBlank(retreatRecord.getMaterielSpecs())||
					StringUtils.isBlank(retreatRecord.getUnit())||*//*
					StringUtils.isBlank(retreatRecord.getRetreatNum())||
					StringUtils.isBlank(retreatRecord.getUretreatNum())){
				throw new RuntimeException("质检前，订单号、客户料号、品名、规格、单位、退料数量、实收数量 不能为空！");
			}*/
			materielRetreatRecordMapper.insert(retreatRecord);
			return true;
		}
		if("1".equals(retreatRecord.getState())){//质检后
			/*if(StringUtils.isBlank(retreatRecord.getClientCode())||
					StringUtils.isBlank(retreatRecord.getMaterielName())||
					StringUtils.isBlank(retreatRecord.getMaterielCode())||
					StringUtils.isBlank(retreatRecord.getMaterielSpecs())||
					StringUtils.isBlank(retreatRecord.getUnit())||
					StringUtils.isBlank(retreatRecord.getRetreatNum())||
					StringUtils.isBlank(retreatRecord.getUretreatNum())){
				throw new RuntimeException("质检后，订单号、物料料号、品名、规格、单位、退料数量、实收数量 不能为空！");
			}*/
			materielRetreatRecordMapper.insert(retreatRecord);
			if (StringUtils.isNotBlank(retreatRecord.getReason1())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("破损");
				materielRetreatReason.setNumber(retreatRecord.getReason1());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("破损");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason2())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("断裂");
				materielRetreatReason.setNumber(retreatRecord.getReason2());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("断裂");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason3())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("划伤");
				materielRetreatReason.setNumber(retreatRecord.getReason3());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("划伤");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason4())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("来料损件");
				materielRetreatReason.setNumber(retreatRecord.getReason4());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("来料损件");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason5())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("尺寸不符");
				materielRetreatReason.setNumber(retreatRecord.getReason5());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("尺寸不符");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason6())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("功能不良");
				materielRetreatReason.setNumber(retreatRecord.getReason6());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("功能不良");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason7())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("实物与焊盘不符");
				materielRetreatReason.setNumber(retreatRecord.getReason7());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("实物与焊盘不符");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason8())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("料号不符");
				materielRetreatReason.setNumber(retreatRecord.getReason8());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("料号不符");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason9())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("线束不良");
				materielRetreatReason.setNumber(retreatRecord.getReason9());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("线束不良");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason10())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("型号不符");
				materielRetreatReason.setNumber(retreatRecord.getReason10());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("型号不符");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason11())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("其他");
				materielRetreatReason.setNumber(retreatRecord.getReason11());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("其他");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			return true;
		}
		return false;

	}

	public boolean updateByedit(MaterielRetreatRecord retreatRecord){
		if (StringUtils.isBlank(retreatRecord.getState())){
			retreatRecord.setState("1");
		}
		if("0".equals(retreatRecord.getState())){//质检前
			if(StringUtils.isBlank(retreatRecord.getClientCode())||
					StringUtils.isBlank(retreatRecord.getOrderCode())||
					StringUtils.isBlank(retreatRecord.getMaterielName())||
					StringUtils.isBlank(retreatRecord.getMaterielSpecs())||
					StringUtils.isBlank(retreatRecord.getUnit())||
					StringUtils.isBlank(retreatRecord.getRetreatNum())||
					StringUtils.isBlank(retreatRecord.getUretreatNum())){
				throw new RuntimeException("质检前，订单号、客户料号、品名、规格、单位、退料数量、实收数量 不能为空！");
			}
			materielRetreatRecordMapper.updateById(retreatRecord);
			return true;
		}
		if("1".equals(retreatRecord.getState())){//质检后
			if(StringUtils.isBlank(retreatRecord.getClientCode())||
					StringUtils.isBlank(retreatRecord.getMaterielName())||
					StringUtils.isBlank(retreatRecord.getMaterielCode())||
					StringUtils.isBlank(retreatRecord.getMaterielSpecs())||
					StringUtils.isBlank(retreatRecord.getUnit())||
					StringUtils.isBlank(retreatRecord.getRetreatNum())||
					StringUtils.isBlank(retreatRecord.getUretreatNum())){
				throw new RuntimeException("质检后，订单号、物料料号、品名、规格、单位、退料数量、实收数量 不能为空！");
			}
			materielRetreatRecordMapper.updateById(retreatRecord);
			materielRetreatReasonMapper.deleteByMainId(retreatRecord.getId());
			if (StringUtils.isNotBlank(retreatRecord.getReason1())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("破损");
				materielRetreatReason.setNumber(retreatRecord.getReason1());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("破损");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason2())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("断裂");
				materielRetreatReason.setNumber(retreatRecord.getReason2());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("断裂");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason3())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("划伤");
				materielRetreatReason.setNumber(retreatRecord.getReason3());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("划伤");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason4())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("来料损件");
				materielRetreatReason.setNumber(retreatRecord.getReason4());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("来料损件");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason5())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("尺寸不符");
				materielRetreatReason.setNumber(retreatRecord.getReason5());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("尺寸不符");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason6())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("功能不良");
				materielRetreatReason.setNumber(retreatRecord.getReason6());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("功能不良");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason7())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("实物与焊盘不符");
				materielRetreatReason.setNumber(retreatRecord.getReason7());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("实物与焊盘不符");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason8())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("料号不符");
				materielRetreatReason.setNumber(retreatRecord.getReason8());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("料号不符");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason9())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("线束不良");
				materielRetreatReason.setNumber(retreatRecord.getReason9());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("线束不良");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason10())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("型号不符");
				materielRetreatReason.setNumber(retreatRecord.getReason10());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("型号不符");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			if (StringUtils.isNotBlank(retreatRecord.getReason11())){
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("其他");
				materielRetreatReason.setNumber(retreatRecord.getReason11());
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}else{
				MaterielRetreatReason materielRetreatReason = new MaterielRetreatReason();
				materielRetreatReason.setMainId(retreatRecord.getId());
				materielRetreatReason.setReason("其他");
				materielRetreatReason.setNumber("0");
				materielRetreatReasonMapper.insert(materielRetreatReason);
			}
			return true;
		}
		return false;
	}
	
}
