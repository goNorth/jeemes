package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.inspect.entity.MesDipsmtFirstpiece;
import org.jeecg.modules.mes.inspect.service.IMesDipsmtFirstpieceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: 品质报表—DIP_SMT首件
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
@Api(tags="品质报表—DIP_SMT首件")
@RestController
@RequestMapping("/inspect/mesDipsmtFirstpiece")
@Slf4j
public class MesDipsmtFirstpieceController extends JeecgController<MesDipsmtFirstpiece, IMesDipsmtFirstpieceService> {
	@Autowired
	private IMesDipsmtFirstpieceService mesDipsmtFirstpieceService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesDipsmtFirstpiece
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "品质报表—DIP_SMT首件-分页列表查询")
	@ApiOperation(value="品质报表—DIP_SMT首件-分页列表查询", notes="品质报表—DIP_SMT首件-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesDipsmtFirstpiece mesDipsmtFirstpiece,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesDipsmtFirstpiece> queryWrapper = QueryGenerator.initQueryWrapper(mesDipsmtFirstpiece, req.getParameterMap());
		Page<MesDipsmtFirstpiece> page = new Page<MesDipsmtFirstpiece>(pageNo, pageSize);
		IPage<MesDipsmtFirstpiece> pageList = mesDipsmtFirstpieceService.page(page, queryWrapper);
		return Result.ok(pageList);
	}


	/**
	 *   添加
	 *
	 * @param mesDipsmtFirstpiece
	 * @return
	 */
	@AutoLog(value = "品质报表—DIP_SMT首件-添加")
	@ApiOperation(value="品质报表—DIP_SMT首件-添加", notes="品质报表—DIP_SMT首件-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesDipsmtFirstpiece mesDipsmtFirstpiece) {
		mesDipsmtFirstpieceService.save(mesDipsmtFirstpiece);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesDipsmtFirstpiece
	 * @return
	 */
	@AutoLog(value = "品质报表—DIP_SMT首件-编辑")
	@ApiOperation(value="品质报表—DIP_SMT首件-编辑", notes="品质报表—DIP_SMT首件-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesDipsmtFirstpiece mesDipsmtFirstpiece) {
		mesDipsmtFirstpieceService.updateById(mesDipsmtFirstpiece);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "品质报表—DIP_SMT首件-通过id删除")
	@ApiOperation(value="品质报表—DIP_SMT首件-通过id删除", notes="品质报表—DIP_SMT首件-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesDipsmtFirstpieceService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "品质报表—DIP_SMT首件-批量删除")
	@ApiOperation(value="品质报表—DIP_SMT首件-批量删除", notes="品质报表—DIP_SMT首件-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesDipsmtFirstpieceService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "品质报表—DIP_SMT首件-通过id查询")
	@ApiOperation(value="品质报表—DIP_SMT首件-通过id查询", notes="品质报表—DIP_SMT首件-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesDipsmtFirstpiece mesDipsmtFirstpiece = mesDipsmtFirstpieceService.getById(id);
		if(mesDipsmtFirstpiece==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesDipsmtFirstpiece);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesDipsmtFirstpiece
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesDipsmtFirstpiece mesDipsmtFirstpiece) {
        return super.exportXls(request, mesDipsmtFirstpiece, MesDipsmtFirstpiece.class, "品质报表—DIP_SMT首件");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesDipsmtFirstpiece.class);
    }

}
