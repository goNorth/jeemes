package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 制造中心-制令单信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesCommandbillInfoService extends IService<MesCommandbillInfo> {

    void recordErrorOperation(String callAddress, String operationSteps, String wrongSteps, String errMeg, String commandBillId);

    List<MesCommandbillInfo> queryMesCommandBillInfoByProduceId(List<String> produceIds);
}
