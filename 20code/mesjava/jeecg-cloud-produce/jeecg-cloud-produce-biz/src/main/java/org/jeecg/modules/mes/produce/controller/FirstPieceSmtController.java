package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.FirstPieceSmt;
import org.jeecg.modules.mes.produce.entity.FirstPieceSmtLog;
import org.jeecg.modules.mes.produce.service.IFirstPieceSmtLogService;
import org.jeecg.modules.mes.produce.service.IFirstPieceSmtService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: SMT首件确认表
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
@Api(tags="SMT首件确认表")
@RestController
@RequestMapping("/produce/firstPieceSmt")
@Slf4j
public class FirstPieceSmtController extends JeecgController<FirstPieceSmt, IFirstPieceSmtService> {
	@Autowired
	private IFirstPieceSmtService firstPieceSmtService;
	@Autowired
	private IFirstPieceSmtLogService firstPieceSmtLogService;
	
	/**
	 * 分页列表查询
	 *
	 * @param firstPieceSmt
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-分页列表查询")
	@ApiOperation(value="SMT首件确认表-分页列表查询", notes="SMT首件确认表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FirstPieceSmt firstPieceSmt,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FirstPieceSmt> queryWrapper = QueryGenerator.initQueryWrapper(firstPieceSmt, req.getParameterMap());
		Page<FirstPieceSmt> page = new Page<FirstPieceSmt>(pageNo, pageSize);
		IPage<FirstPieceSmt> pageList = firstPieceSmtService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param firstPieceSmt
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-添加")
	@ApiOperation(value="SMT首件确认表-添加", notes="SMT首件确认表-添加会同时添加检查内容记录")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FirstPieceSmt firstPieceSmt) {
		firstPieceSmtService.saveMain(firstPieceSmt);
		//根据制令单号去查询，看看首件了几次
		QueryWrapper<FirstPieceSmt> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("product_num",firstPieceSmt.getProductNum());
		List<FirstPieceSmt> list = firstPieceSmtService.list(queryWrapper);
		return Result.ok("添加成功！已首件"+list.size()+"次！");
	}
	
	/**
	 *  编辑
	 *
	 * @param firstPieceSmt
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-编辑")
	@ApiOperation(value="SMT首件确认表-编辑", notes="SMT首件确认表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FirstPieceSmt firstPieceSmt) {
		firstPieceSmtService.updateById(firstPieceSmt);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-通过id删除")
	@ApiOperation(value="SMT首件确认表-通过id删除", notes="SMT首件确认表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		firstPieceSmtService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-批量删除")
	@ApiOperation(value="SMT首件确认表-批量删除", notes="SMT首件确认表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.firstPieceSmtService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-通过id查询")
	@ApiOperation(value="SMT首件确认表-通过id查询", notes="SMT首件确认表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FirstPieceSmt firstPieceSmt = firstPieceSmtService.getById(id);
		if(firstPieceSmt==null) {
			return Result.error("未找到对应数据");
		}else{
			QueryWrapper<FirstPieceSmtLog> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("first_id",firstPieceSmt.getId());
			List<FirstPieceSmtLog> list = firstPieceSmtLogService.list(queryWrapper);
			firstPieceSmt.setFirstPieceSmtlogs(list);

		}
		return Result.ok(firstPieceSmt);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param firstPieceSmt
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FirstPieceSmt firstPieceSmt) {
        return super.exportXls(request, firstPieceSmt, FirstPieceSmt.class, "SMT首件确认表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FirstPieceSmt.class);
    }

}
