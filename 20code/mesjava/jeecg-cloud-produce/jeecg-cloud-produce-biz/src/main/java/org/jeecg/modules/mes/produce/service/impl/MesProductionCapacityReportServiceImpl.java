package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.produce.entity.MesProductionCapacityReport;
import org.jeecg.modules.mes.produce.mapper.MesProductionCapacityReportMapper;
import org.jeecg.modules.mes.produce.service.IMesProductionCapacityReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: NEW生产产能报表
 * @Author: jeecg-boot
 * @Date:   2021-05-25
 * @Version: V1.0
 */
@Service
public class MesProductionCapacityReportServiceImpl extends ServiceImpl<MesProductionCapacityReportMapper, MesProductionCapacityReport> implements IMesProductionCapacityReportService {

    @Autowired
    private MesProductionCapacityReportMapper mesProductionCapacityReportMapper;

    @Override
    public Result<?> mesProductionCapacityReport(MesProductionCapacityReport mesProductionCapacityReport, Integer pageNo, Integer pageSize) {
        Page<MesProductionCapacityReport> page = new Page<>(pageNo, pageSize);
        page.setRecords(mesProductionCapacityReportMapper.mesProductionCapacityReport(page, mesProductionCapacityReport));
        return Result.ok(page);
    }
}
