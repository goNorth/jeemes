package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataScraper;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSteelmesh;
import org.jeecg.modules.mes.client.SystemClient;
import org.jeecg.modules.mes.produce.entity.BindingRecordInfo;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.jeecg.modules.mes.produce.mapper.BindingRecordInfoMapper;
import org.jeecg.modules.mes.produce.service.IBindingRecordInfoService;
import org.jeecg.modules.mes.produce.service.IMesCommandbillInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 钢网、刮刀绑定记录
 * @Author: jeecg-boot
 * @Date: 2021-04-19
 * @Version: V1.0
 */
@Service
public class BindingRecordInfoServiceImpl extends ServiceImpl<BindingRecordInfoMapper, BindingRecordInfo> implements IBindingRecordInfoService {

    @Autowired
    private SystemClient systemClient;

    @Autowired
    private IMesCommandbillInfoService commandbillInfoService;

    @Transactional
    public boolean stateSave(BindingRecordInfo recordInfo) {

        if (StringUtils.isBlank(recordInfo.getBindingSn())) {
            throw new RuntimeException("SN编码不能为空");
        }
        QueryWrapper<MesCommandbillInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("commandbill_code", recordInfo.getCommandbillCode());
        List<MesCommandbillInfo> list = commandbillInfoService.list(queryWrapper);
        if (list.size() == 0) {
            throw new RuntimeException("该制令单编号不存在");
        }
        //验证sn
        boolean mark = false;
        if (StringUtils.isNotBlank(recordInfo.getBindingName())) {
            //验证sn是否存在
            if ("钢网".equals(recordInfo.getBindingName()) && StringUtils.isNotBlank(recordInfo.getBindingSn())) {
                MesChiefdataSteelmesh steelmeshComitem = systemClient.getSteelmeshComitem(recordInfo.getBindingSn());
                if (steelmeshComitem != null) {
                    mark = true;
                }
            }
            if ("刮刀".equals(recordInfo.getBindingName()) && StringUtils.isNotBlank(recordInfo.getBindingSn())) {
                MesChiefdataScraper scraperComitem = systemClient.getScraperComitem(recordInfo.getBindingSn());
                if (scraperComitem != null) {
                    mark = true;
                }
            }
        } else {
            throw new RuntimeException("品名不能为空");
        }
        if (mark) {
            //如果存在状态为0的数据，就更改为1；
            QueryWrapper<BindingRecordInfo> bindingQueryWrapper = new QueryWrapper<>();
            bindingQueryWrapper.eq("binding_name", recordInfo.getBindingName());
            bindingQueryWrapper.eq("commandbill_code", recordInfo.getCommandbillCode());
            bindingQueryWrapper.eq("state", "0");
            List<BindingRecordInfo> list1 = this.list(bindingQueryWrapper);
            for (BindingRecordInfo recordInfo1 : list1) {
                if (recordInfo.getBindingSn().equals(recordInfo1.getBindingSn())) {
                    throw new RuntimeException("请勿重复扫描！");
                }
                recordInfo1.setState("1");
                this.updateById(recordInfo1);
            }
            this.setRecordInfo(recordInfo, list);
            recordInfo.setState("0");
            this.save(recordInfo);//保存
            return true;
        }
        return false;
    }

    @Override
    public Result<?> queryPageList(Page<BindingRecordInfo> page, QueryWrapper<BindingRecordInfo> queryWrapper) {
        IPage<BindingRecordInfo> pageList = this.page(page, queryWrapper);
        if (com.epms.util.ObjectHelper.isNotEmpty(pageList.getRecords())) {
            for (BindingRecordInfo info : pageList.getRecords()) {
                if (StringUtils.isNotEmpty(info.getCommandbillCode()) && StringUtils.isEmpty(info.getMechanismCode())) {
                    List<MesCommandbillInfo> commandBillInfos = commandbillInfoService.lambdaQuery().eq(MesCommandbillInfo::getCommandbillCode, info.getCommandbillCode()).orderByDesc(MesCommandbillInfo::getCreateTime).list();
                    if (com.epms.util.ObjectHelper.isNotEmpty(commandBillInfos)) {
                        MesCommandbillInfo commandBillInfo = commandBillInfos.get(0);
                        if (com.epms.util.ObjectHelper.isNotEmpty(commandBillInfo)) {
                            info.setMechanismCode(commandBillInfo.getMechanismCode());
                            info.setMechanismName(commandBillInfo.getMechanismName());
                            info.setGague(commandBillInfo.getGague());
                            info.setPlantNum(commandBillInfo.getPlantNum());
                        }
                    }
                }
            }
        }
        return Result.ok(pageList);
    }

    private void setRecordInfo(BindingRecordInfo recordInfo, List<MesCommandbillInfo> list) {
        MesCommandbillInfo info = list.get(0);
        if (com.epms.util.ObjectHelper.isNotEmpty(info)) {
            if (StringUtils.isEmpty(recordInfo.getMechanismName())) {//机种名称
                recordInfo.setMechanismName(info.getMechanismName());
            }
            if (StringUtils.isEmpty(recordInfo.getMechanismCode())) {//机种料号
                recordInfo.setMechanismCode(info.getMechanismCode());
            }
            if (StringUtils.isEmpty(recordInfo.getGague())) {//规格
                recordInfo.setGague(info.getGague());
            }
            if (StringUtils.isEmpty(recordInfo.getPlantNum())) {//计划数量
                recordInfo.setPlantNum(info.getPlantNum());
            }
        }
    }

}
