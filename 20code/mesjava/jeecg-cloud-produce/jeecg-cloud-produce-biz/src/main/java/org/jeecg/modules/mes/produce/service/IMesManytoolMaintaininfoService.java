package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesManytoolInfo;
import org.jeecg.modules.mes.produce.entity.MesManytoolMaintaininfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 多个制具维保-保养信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesManytoolMaintaininfoService extends IService<MesManytoolMaintaininfo> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesManytoolMaintaininfo mesManytoolMaintaininfo,List<MesManytoolInfo> mesManytoolInfoList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesManytoolMaintaininfo mesManytoolMaintaininfo,List<MesManytoolInfo> mesManytoolInfoList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
