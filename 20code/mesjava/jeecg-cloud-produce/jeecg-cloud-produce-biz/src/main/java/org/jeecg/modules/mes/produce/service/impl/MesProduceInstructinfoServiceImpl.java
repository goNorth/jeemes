package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesProduceInstructinfo;
import org.jeecg.modules.mes.produce.mapper.MesProduceInstructitemMapper;
import org.jeecg.modules.mes.produce.mapper.MesProduceInstructinfoMapper;
import org.jeecg.modules.mes.produce.service.IMesProduceInstructinfoService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.Collection;

/**
 * @Description: 生产指示单-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesProduceInstructinfoServiceImpl extends ServiceImpl<MesProduceInstructinfoMapper, MesProduceInstructinfo> implements IMesProduceInstructinfoService {

	@Autowired
	private MesProduceInstructinfoMapper mesProduceInstructinfoMapper;
	@Autowired
	private MesProduceInstructitemMapper mesProduceInstructitemMapper;
	
	@Override
	@Transactional
	public void delMain(String id) {
		mesProduceInstructitemMapper.deleteByMainId(id);
		mesProduceInstructinfoMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesProduceInstructitemMapper.deleteByMainId(id.toString());
			mesProduceInstructinfoMapper.deleteById(id);
		}
	}
	
}
