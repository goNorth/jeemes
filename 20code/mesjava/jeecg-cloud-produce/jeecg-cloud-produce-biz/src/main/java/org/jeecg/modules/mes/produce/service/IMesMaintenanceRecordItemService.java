package org.jeecg.modules.mes.produce.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.produce.entity.MesMaintenanceRecordItem;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.vo.MaintenanceRecordVo;

import java.text.ParseException;

/**
 * @Description: 维护保养点检记录子表
 * @Author: jeecg-boot
 * @Date:   2021-06-11
 * @Version: V1.0
 */
public interface IMesMaintenanceRecordItemService extends IService<MesMaintenanceRecordItem> {

    Result<?> addMesMaintenanceRecordItem(MesMaintenanceRecordItem mesMaintenanceRecordItem);

    Result<?> editMesMaintenanceRecordItem(MesMaintenanceRecordItem mesMaintenanceRecordItem);

    Result<?> addMaintenanceRecordItemForApp(MaintenanceRecordVo maintenanceRecordVo)  throws ParseException;

    Result<?> queryList(String tableNameCode, String inspectionDate);

    Result<?> queryListByMainId(String mainId);
}
