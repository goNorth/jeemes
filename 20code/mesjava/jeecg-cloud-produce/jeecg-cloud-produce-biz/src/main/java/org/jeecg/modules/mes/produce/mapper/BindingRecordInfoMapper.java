package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.BindingRecordInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 钢网、刮刀绑定记录
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface BindingRecordInfoMapper extends BaseMapper<BindingRecordInfo> {

}
