package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesAncillarytoolWriteoff;
import org.jeecg.modules.mes.produce.service.IMesAncillarytoolWriteoffService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-辅料报废
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="制造中心-辅料报废")
@RestController
@RequestMapping("/produce/mesAncillarytoolWriteoff")
@Slf4j
public class MesAncillarytoolWriteoffController extends JeecgController<MesAncillarytoolWriteoff, IMesAncillarytoolWriteoffService> {
	@Autowired
	private IMesAncillarytoolWriteoffService mesAncillarytoolWriteoffService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesAncillarytoolWriteoff
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料报废-分页列表查询")
	@ApiOperation(value="制造中心-辅料报废-分页列表查询", notes="制造中心-辅料报废-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesAncillarytoolWriteoff mesAncillarytoolWriteoff,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesAncillarytoolWriteoff> queryWrapper = QueryGenerator.initQueryWrapper(mesAncillarytoolWriteoff, req.getParameterMap());
		Page<MesAncillarytoolWriteoff> page = new Page<MesAncillarytoolWriteoff>(pageNo, pageSize);
		IPage<MesAncillarytoolWriteoff> pageList = mesAncillarytoolWriteoffService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesAncillarytoolWriteoff
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料报废-添加")
	@ApiOperation(value="制造中心-辅料报废-添加", notes="制造中心-辅料报废-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesAncillarytoolWriteoff mesAncillarytoolWriteoff) {
		mesAncillarytoolWriteoffService.save(mesAncillarytoolWriteoff);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesAncillarytoolWriteoff
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料报废-编辑")
	@ApiOperation(value="制造中心-辅料报废-编辑", notes="制造中心-辅料报废-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesAncillarytoolWriteoff mesAncillarytoolWriteoff) {
		mesAncillarytoolWriteoffService.updateById(mesAncillarytoolWriteoff);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料报废-通过id删除")
	@ApiOperation(value="制造中心-辅料报废-通过id删除", notes="制造中心-辅料报废-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesAncillarytoolWriteoffService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料报废-批量删除")
	@ApiOperation(value="制造中心-辅料报废-批量删除", notes="制造中心-辅料报废-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesAncillarytoolWriteoffService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料报废-通过id查询")
	@ApiOperation(value="制造中心-辅料报废-通过id查询", notes="制造中心-辅料报废-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesAncillarytoolWriteoff mesAncillarytoolWriteoff = mesAncillarytoolWriteoffService.getById(id);
		if(mesAncillarytoolWriteoff==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesAncillarytoolWriteoff);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesAncillarytoolWriteoff
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesAncillarytoolWriteoff mesAncillarytoolWriteoff) {
        return super.exportXls(request, mesAncillarytoolWriteoff, MesAncillarytoolWriteoff.class, "制造中心-辅料报废");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesAncillarytoolWriteoff.class);
    }

}
