package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesModelCheckproject;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 模版项目配置-检查项目
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesModelCheckprojectService extends IService<MesModelCheckproject> {

	public List<MesModelCheckproject> selectByMainId(String mainId);
}
