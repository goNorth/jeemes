package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesWorkbillInfo;
import org.jeecg.modules.mes.produce.mapper.MesWorkbillInfoMapper;
import org.jeecg.modules.mes.produce.service.IMesWorkbillInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-工单信息
 * @Author: jeecg-boot
 * @Date:   2020-10-20
 * @Version: V1.0
 */
@Service
public class MesWorkbillInfoServiceImpl extends ServiceImpl<MesWorkbillInfoMapper, MesWorkbillInfo> implements IMesWorkbillInfoService {

}
