package org.jeecg.modules.mes.produce.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.produce.entity.MesOnlineMaterieltableMain;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.Collection;

/**
 * @Description: 制造中心-在线料表-主表
 * @Author: jeecg-boot
 * @Date:   2021-05-13
 * @Version: V1.0
 */
public interface IMesOnlineMaterieltableMainService extends IService<MesOnlineMaterieltableMain> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);

	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	public boolean importMesOnlineMaterieltable(HttpServletRequest request, HttpServletResponse response, String mainId);


	Result<?> add(MesOnlineMaterieltableMain mesOnlineMaterieltableMain);

	public boolean updateMain(String productCode,String lineType);
}
