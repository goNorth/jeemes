package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.FirstPieceSmtLog;
import org.jeecg.modules.mes.produce.mapper.FirstPieceSmtLogMapper;
import org.jeecg.modules.mes.produce.service.IFirstPieceSmtLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: SMT首件确认表-检查内容
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
@Service
public class FirstPieceSmtLogServiceImpl extends ServiceImpl<FirstPieceSmtLogMapper, FirstPieceSmtLog> implements IFirstPieceSmtLogService {

}
