package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesMechanismDeployinfo;
import org.jeecg.modules.mes.produce.entity.MesMechanismCheckproject;
import org.jeecg.modules.mes.produce.mapper.MesMechanismCheckprojectMapper;
import org.jeecg.modules.mes.produce.mapper.MesMechanismDeployinfoMapper;
import org.jeecg.modules.mes.produce.service.IMesMechanismDeployinfoService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 机种项目配置-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesMechanismDeployinfoServiceImpl extends ServiceImpl<MesMechanismDeployinfoMapper, MesMechanismDeployinfo> implements IMesMechanismDeployinfoService {

	@Autowired
	private MesMechanismDeployinfoMapper mesMechanismDeployinfoMapper;
	@Autowired
	private MesMechanismCheckprojectMapper mesMechanismCheckprojectMapper;
	
	@Override
	@Transactional
	public void saveMain(MesMechanismDeployinfo mesMechanismDeployinfo, List<MesMechanismCheckproject> mesMechanismCheckprojectList) {
		mesMechanismDeployinfoMapper.insert(mesMechanismDeployinfo);
		if(mesMechanismCheckprojectList!=null && mesMechanismCheckprojectList.size()>0) {
			for(MesMechanismCheckproject entity:mesMechanismCheckprojectList) {
				//外键设置
				entity.setMechanismId(mesMechanismDeployinfo.getId());
				mesMechanismCheckprojectMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesMechanismDeployinfo mesMechanismDeployinfo,List<MesMechanismCheckproject> mesMechanismCheckprojectList) {
		mesMechanismDeployinfoMapper.updateById(mesMechanismDeployinfo);
		
		//1.先删除子表数据
		mesMechanismCheckprojectMapper.deleteByMainId(mesMechanismDeployinfo.getId());
		
		//2.子表数据重新插入
		if(mesMechanismCheckprojectList!=null && mesMechanismCheckprojectList.size()>0) {
			for(MesMechanismCheckproject entity:mesMechanismCheckprojectList) {
				//外键设置
				entity.setMechanismId(mesMechanismDeployinfo.getId());
				mesMechanismCheckprojectMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesMechanismCheckprojectMapper.deleteByMainId(id);
		mesMechanismDeployinfoMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesMechanismCheckprojectMapper.deleteByMainId(id.toString());
			mesMechanismDeployinfoMapper.deleteById(id);
		}
	}
	
}
