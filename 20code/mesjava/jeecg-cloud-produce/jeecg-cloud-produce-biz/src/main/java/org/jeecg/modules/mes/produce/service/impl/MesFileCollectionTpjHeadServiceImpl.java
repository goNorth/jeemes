package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjHead;
import org.jeecg.modules.mes.produce.mapper.MesFileCollectionTpjHeadMapper;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionTpjHeadService;
import org.springframework.stereotype.Service;

/**
 * @Description: mes_file_collection_tpj_head
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Service
public class MesFileCollectionTpjHeadServiceImpl extends ServiceImpl<MesFileCollectionTpjHeadMapper, MesFileCollectionTpjHead> implements IMesFileCollectionTpjHeadService {

}
