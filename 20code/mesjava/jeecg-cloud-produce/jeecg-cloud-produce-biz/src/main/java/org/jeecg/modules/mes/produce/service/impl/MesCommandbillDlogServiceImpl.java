package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesCommandbillDlog;
import org.jeecg.modules.mes.produce.mapper.MesCommandbillDlogMapper;
import org.jeecg.modules.mes.produce.service.IMesCommandbillDlogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制令单上料记录
 * @Author: jeecg-boot
 * @Date: 2021-03-04
 * @Version: V1.0
 */
@Service
public class MesCommandbillDlogServiceImpl extends ServiceImpl<MesCommandbillDlogMapper, MesCommandbillDlog> implements IMesCommandbillDlogService {

}
