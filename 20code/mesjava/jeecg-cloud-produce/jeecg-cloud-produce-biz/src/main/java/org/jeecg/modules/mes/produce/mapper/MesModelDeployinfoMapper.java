package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesModelDeployinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 模版项目配置-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesModelDeployinfoMapper extends BaseMapper<MesModelDeployinfo> {

}
