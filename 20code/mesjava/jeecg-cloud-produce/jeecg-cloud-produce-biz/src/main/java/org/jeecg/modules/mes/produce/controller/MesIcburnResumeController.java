package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesIcburnResume;
import org.jeecg.modules.mes.produce.service.IMesIcburnResumeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-IC烧录
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="制造中心-IC烧录")
@RestController
@RequestMapping("/produce/mesIcburnResume")
@Slf4j
public class MesIcburnResumeController extends JeecgController<MesIcburnResume, IMesIcburnResumeService> {
	@Autowired
	private IMesIcburnResumeService mesIcburnResumeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesIcburnResume
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-IC烧录-分页列表查询")
	@ApiOperation(value="制造中心-IC烧录-分页列表查询", notes="制造中心-IC烧录-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesIcburnResume mesIcburnResume,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesIcburnResume> queryWrapper = QueryGenerator.initQueryWrapper(mesIcburnResume, req.getParameterMap());
		Page<MesIcburnResume> page = new Page<MesIcburnResume>(pageNo, pageSize);
		IPage<MesIcburnResume> pageList = mesIcburnResumeService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesIcburnResume
	 * @return
	 */
	@AutoLog(value = "制造中心-IC烧录-添加")
	@ApiOperation(value="制造中心-IC烧录-添加", notes="制造中心-IC烧录-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesIcburnResume mesIcburnResume) {
		mesIcburnResumeService.save(mesIcburnResume);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesIcburnResume
	 * @return
	 */
	@AutoLog(value = "制造中心-IC烧录-编辑")
	@ApiOperation(value="制造中心-IC烧录-编辑", notes="制造中心-IC烧录-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesIcburnResume mesIcburnResume) {
		mesIcburnResumeService.updateById(mesIcburnResume);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-IC烧录-通过id删除")
	@ApiOperation(value="制造中心-IC烧录-通过id删除", notes="制造中心-IC烧录-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesIcburnResumeService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-IC烧录-批量删除")
	@ApiOperation(value="制造中心-IC烧录-批量删除", notes="制造中心-IC烧录-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesIcburnResumeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-IC烧录-通过id查询")
	@ApiOperation(value="制造中心-IC烧录-通过id查询", notes="制造中心-IC烧录-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesIcburnResume mesIcburnResume = mesIcburnResumeService.getById(id);
		if(mesIcburnResume==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesIcburnResume);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesIcburnResume
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesIcburnResume mesIcburnResume) {
        return super.exportXls(request, mesIcburnResume, MesIcburnResume.class, "制造中心-IC烧录");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesIcburnResume.class);
    }

}
