package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesMechanismCheckproject;
import org.jeecg.modules.mes.produce.entity.MesMechanismDeployinfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 机种项目配置-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesMechanismDeployinfoService extends IService<MesMechanismDeployinfo> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesMechanismDeployinfo mesMechanismDeployinfo,List<MesMechanismCheckproject> mesMechanismCheckprojectList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesMechanismDeployinfo mesMechanismDeployinfo,List<MesMechanismCheckproject> mesMechanismCheckprojectList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
