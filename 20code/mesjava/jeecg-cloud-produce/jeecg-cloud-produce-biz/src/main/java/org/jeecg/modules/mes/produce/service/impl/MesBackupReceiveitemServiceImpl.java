package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesBackupReceiveitem;
import org.jeecg.modules.mes.produce.mapper.MesBackupReceiveitemMapper;
import org.jeecg.modules.mes.produce.service.IMesBackupReceiveitemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 备品领用-明细信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesBackupReceiveitemServiceImpl extends ServiceImpl<MesBackupReceiveitemMapper, MesBackupReceiveitem> implements IMesBackupReceiveitemService {
	
	@Autowired
	private MesBackupReceiveitemMapper mesBackupReceiveitemMapper;
	
	@Override
	public List<MesBackupReceiveitem> selectByMainId(String mainId) {
		return mesBackupReceiveitemMapper.selectByMainId(mainId);
	}
}
