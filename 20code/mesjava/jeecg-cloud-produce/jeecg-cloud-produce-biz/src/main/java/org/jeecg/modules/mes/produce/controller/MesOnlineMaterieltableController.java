package org.jeecg.modules.mes.produce.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesOnlineMaterieltable;
import org.jeecg.modules.mes.produce.service.IMesOnlineMaterieltableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * @Description: 制造中心-在线料表
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="制造中心-在线料表")
@RestController
@RequestMapping("/produce/mesOnlineMaterieltable")
@Slf4j
public class MesOnlineMaterieltableController extends JeecgController<MesOnlineMaterieltable, IMesOnlineMaterieltableService> {
	@Autowired
	private IMesOnlineMaterieltableService mesOnlineMaterieltableService;

	/**
	 * 分页列表查询
	 *
	 * @param mesOnlineMaterieltable
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-在线料表-分页列表查询")
	@ApiOperation(value="制造中心-在线料表-分页列表查询", notes="制造中心-在线料表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesOnlineMaterieltable mesOnlineMaterieltable,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesOnlineMaterieltable> queryWrapper = QueryGenerator.initQueryWrapper(mesOnlineMaterieltable, req.getParameterMap());
		Page<MesOnlineMaterieltable> page = new Page<MesOnlineMaterieltable>(pageNo, pageSize);
		IPage<MesOnlineMaterieltable> pageList = mesOnlineMaterieltableService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *
	 * @param pcode 成品料号
	 * @param mcode 物料料号
	 * @param lineType 产线编号
	 * @return
	 */
	@AutoLog(value = "制造中心-在线料表-通过料号和线别查询")
	@ApiOperation(value="制造中心-在线料表-通过料号和线别查询", notes="制造中心-在线料表-通过料号和线别查询")
	@GetMapping(value = "/queryBylineType")
	public MesOnlineMaterieltable queryBylineType(@RequestParam(name="pcode",required=true) String pcode,
												  @RequestParam(name="mcode",required=true) String mcode,
												  @RequestParam(name="lineType",required=true) String lineType,
												  @RequestParam(name="passage",required=false) String passage) {
		QueryWrapper<MesOnlineMaterieltable> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("product_code", pcode).eq("materiel_code", mcode).eq("line_type",lineType);
		if(StringUtils.isNotEmpty(passage)){
			queryWrapper.eq("passage",passage);
		}
		MesOnlineMaterieltable mesOnlineMaterieltable = mesOnlineMaterieltableService.getOne(queryWrapper);
		if (mesOnlineMaterieltable != null) {
			String produceLine = mesOnlineMaterieltable.getLineType();
			System.err.println(produceLine);
			String[] pLine = produceLine.split(",");
			for (String s : pLine) {
				if(s.equals(lineType)){
					return mesOnlineMaterieltable;
				}
			}
		}
		return null;
	}


	/**
	 *   添加
	 *
	 * @param mesOnlineMaterieltable
	 * @return
	 */
	@AutoLog(value = "制造中心-在线料表-添加")
	@ApiOperation(value="制造中心-在线料表-添加", notes="制造中心-在线料表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesOnlineMaterieltable mesOnlineMaterieltable) {
		mesOnlineMaterieltableService.save(mesOnlineMaterieltable);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param mesOnlineMaterieltable
	 * @return
	 */
	@AutoLog(value = "制造中心-在线料表-编辑")
	@ApiOperation(value="制造中心-在线料表-编辑", notes="制造中心-在线料表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesOnlineMaterieltable mesOnlineMaterieltable) {
		mesOnlineMaterieltableService.updateById(mesOnlineMaterieltable);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-在线料表-通过id删除")
	@ApiOperation(value="制造中心-在线料表-通过id删除", notes="制造中心-在线料表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesOnlineMaterieltableService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-在线料表-批量删除")
	@ApiOperation(value="制造中心-在线料表-批量删除", notes="制造中心-在线料表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesOnlineMaterieltableService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-在线料表-通过id查询")
	@ApiOperation(value="制造中心-在线料表-通过id查询", notes="制造中心-在线料表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesOnlineMaterieltable mesOnlineMaterieltable = mesOnlineMaterieltableService.getById(id);
		if(mesOnlineMaterieltable==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesOnlineMaterieltable);
	}

	/**
	 * 导出excel
	 *
	 * @param request
	 * @param mesOnlineMaterieltable
	 */
	@RequestMapping(value = "/exportXls")
	public ModelAndView exportXls(HttpServletRequest request, MesOnlineMaterieltable mesOnlineMaterieltable) {
		return super.exportXls(request, mesOnlineMaterieltable, MesOnlineMaterieltable.class, "制造中心-在线料表");
	}

	/**
	 * 通过excel导入数据
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		boolean mark=mesOnlineMaterieltableService.importExcel(request, response);
		if(mark){
			return Result.OK("导入成功！");
		}else {
			return Result.error("导入失败！");
		}
	}

}
