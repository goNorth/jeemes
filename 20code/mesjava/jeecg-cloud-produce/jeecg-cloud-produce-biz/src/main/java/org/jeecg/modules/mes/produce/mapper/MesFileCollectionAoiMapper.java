package org.jeecg.modules.mes.produce.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoi;

/**
 * @Description: mes_file_collection_aoi
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface MesFileCollectionAoiMapper extends BaseMapper<MesFileCollectionAoi> {

}
