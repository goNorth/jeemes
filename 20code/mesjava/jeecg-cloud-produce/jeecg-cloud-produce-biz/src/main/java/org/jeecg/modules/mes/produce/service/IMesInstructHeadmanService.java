package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesInstructHeadman;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 制造中心-检测项目(指示单项目责任人)
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesInstructHeadmanService extends IService<MesInstructHeadman> {

	public List<MesInstructHeadman> selectByMainId(String mainId);
}
