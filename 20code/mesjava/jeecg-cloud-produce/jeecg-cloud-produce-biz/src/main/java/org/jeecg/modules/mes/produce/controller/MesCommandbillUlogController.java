package org.jeecg.modules.mes.produce.controller;

import javax.servlet.http.HttpServletRequest;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesCommandbillUlog;
import org.jeecg.modules.mes.produce.service.IMesCommandbillUlogService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 制令单上料记录
 * @Author: jeecg-boot
 * @Date: 2021-03-04
 * @Version: V1.0
 */
@Api(tags = "制令单上料记录")
@RestController
@RequestMapping("/produce/mesCommandbillUlog")
@Slf4j
public class MesCommandbillUlogController extends JeecgController<MesCommandbillUlog, IMesCommandbillUlogService> {
	@Autowired
	private IMesCommandbillUlogService mesCommandbillUlogService;

	/**
	 * 分页列表查询
	 *
	 * @param mesCommandbillUlog
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制令单上料记录-分页列表查询")
	@ApiOperation(value = "制令单上料记录-分页列表查询", notes = "制令单上料记录-分页列表查询")
	@GetMapping(value = "/list")
    public Result<?> queryPageList(MesCommandbillUlog mesCommandbillUlog,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<MesCommandbillUlog> queryWrapper = QueryGenerator.initQueryWrapper(mesCommandbillUlog, req.getParameterMap());
        Page<MesCommandbillUlog> page = new Page<MesCommandbillUlog>(pageNo, pageSize);
        IPage<MesCommandbillUlog> pageList = mesCommandbillUlogService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 远程调用 发料记录日志添加
     *
     * @param mesCommandbillUlog
     * @return
     */
    @AutoLog(value = "制令单上料记录-添加")
    @ApiOperation(value = "制令单上料记录-添加", notes = "制令单上料记录-添加")
    @PostMapping(value = "/addLog")
    public Boolean add(@RequestBody MesCommandbillUlog mesCommandbillUlog) {
        return mesCommandbillUlogService.save(mesCommandbillUlog);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param mesCommandbillUlog
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesCommandbillUlog mesCommandbillUlog) {
        return super.exportXls(request, mesCommandbillUlog, MesCommandbillUlog.class, "制令单上料记录");
	}


}
