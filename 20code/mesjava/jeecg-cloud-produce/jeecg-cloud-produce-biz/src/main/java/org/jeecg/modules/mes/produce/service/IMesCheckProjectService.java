package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesInstructHeadman;
import org.jeecg.modules.mes.produce.entity.MesCheckProject;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 制造中心-检测项目基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesCheckProjectService extends IService<MesCheckProject> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesCheckProject mesCheckProject,List<MesInstructHeadman> mesInstructHeadmanList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesCheckProject mesCheckProject,List<MesInstructHeadman> mesInstructHeadmanList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
