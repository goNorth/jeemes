package org.jeecg.modules.mes.produce.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
@Data
public class ScanMaterVo implements Serializable {
    @Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String commandbillCode;
    @Excel(name = "物料号", width = 15)
    @ApiModelProperty(value = "物料号")
    private java.lang.String materielCode;
    /**物料名称*/
    @Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materielName;
    /**pcb编号*/
    @Excel(name = "pcb编号", width = 15)
    @ApiModelProperty(value = "pcb编号")
    private java.lang.String pcbId;
    @Excel(name = "二维码编号", width = 15)
    @ApiModelProperty(value = "二维码编号")
    private java.lang.String storageId;
    @Excel(name = "用量", width = 15)
    @ApiModelProperty(value = "用量")
    private java.lang.String quantity;
    /**单位*/
    @Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
    @Excel(name = "产线", width = 15)
    @ApiModelProperty(value = "产线")
    private java.lang.String lineType;
    @Excel(name = "飞达", width = 15)
    @ApiModelProperty(value = "飞达")
    private java.lang.String feederSn;
    @Excel(name = "通道号", width = 15)
    @ApiModelProperty(value = "通道号")
    private java.lang.String passage;

    @Excel(name = "操作人", width = 15)
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
    /**创建日期*/
    @Excel(name = "操作日期", width = 15,format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;

}
