package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesBackupArchive;
import org.jeecg.modules.mes.produce.service.IMesBackupArchiveService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-备品建档
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="制造中心-备品建档")
@RestController
@RequestMapping("/produce/mesBackupArchive")
@Slf4j
public class MesBackupArchiveController extends JeecgController<MesBackupArchive, IMesBackupArchiveService> {
	@Autowired
	private IMesBackupArchiveService mesBackupArchiveService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesBackupArchive
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-备品建档-分页列表查询")
	@ApiOperation(value="制造中心-备品建档-分页列表查询", notes="制造中心-备品建档-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesBackupArchive mesBackupArchive,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesBackupArchive> queryWrapper = QueryGenerator.initQueryWrapper(mesBackupArchive, req.getParameterMap());
		Page<MesBackupArchive> page = new Page<MesBackupArchive>(pageNo, pageSize);
		IPage<MesBackupArchive> pageList = mesBackupArchiveService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesBackupArchive
	 * @return
	 */
	@AutoLog(value = "制造中心-备品建档-添加")
	@ApiOperation(value="制造中心-备品建档-添加", notes="制造中心-备品建档-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesBackupArchive mesBackupArchive) {
		mesBackupArchiveService.save(mesBackupArchive);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesBackupArchive
	 * @return
	 */
	@AutoLog(value = "制造中心-备品建档-编辑")
	@ApiOperation(value="制造中心-备品建档-编辑", notes="制造中心-备品建档-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesBackupArchive mesBackupArchive) {
		mesBackupArchiveService.updateById(mesBackupArchive);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-备品建档-通过id删除")
	@ApiOperation(value="制造中心-备品建档-通过id删除", notes="制造中心-备品建档-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesBackupArchiveService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-备品建档-批量删除")
	@ApiOperation(value="制造中心-备品建档-批量删除", notes="制造中心-备品建档-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesBackupArchiveService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-备品建档-通过id查询")
	@ApiOperation(value="制造中心-备品建档-通过id查询", notes="制造中心-备品建档-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesBackupArchive mesBackupArchive = mesBackupArchiveService.getById(id);
		if(mesBackupArchive==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesBackupArchive);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesBackupArchive
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesBackupArchive mesBackupArchive) {
        return super.exportXls(request, mesBackupArchive, MesBackupArchive.class, "制造中心-备品建档");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesBackupArchive.class);
    }

}
