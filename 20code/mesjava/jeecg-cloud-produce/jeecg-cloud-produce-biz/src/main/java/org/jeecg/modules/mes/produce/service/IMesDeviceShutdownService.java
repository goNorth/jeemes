package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesDeviceShutdown;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-设备停机
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesDeviceShutdownService extends IService<MesDeviceShutdown> {

}
