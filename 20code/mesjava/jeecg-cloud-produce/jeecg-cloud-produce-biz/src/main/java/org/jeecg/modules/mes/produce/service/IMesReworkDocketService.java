package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesReworkDocket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-返工单信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesReworkDocketService extends IService<MesReworkDocket> {

}
