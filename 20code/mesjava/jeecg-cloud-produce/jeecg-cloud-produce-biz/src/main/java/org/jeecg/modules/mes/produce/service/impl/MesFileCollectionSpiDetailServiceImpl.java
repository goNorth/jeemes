package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpiDetail;
import org.jeecg.modules.mes.produce.mapper.MesFileCollectionSpiDetailMapper;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionSpiDetailService;
import org.springframework.stereotype.Service;

/**
 * @Description: mes_file_collection_spi_detail
 * @Author: jeecg-boot
 * @Date:   2021-04-16
 * @Version: V1.0
 */
@Service
public class MesFileCollectionSpiDetailServiceImpl extends ServiceImpl<MesFileCollectionSpiDetailMapper, MesFileCollectionSpiDetail> implements IMesFileCollectionSpiDetailService {

}
