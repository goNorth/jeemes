package org.jeecg.modules.mes.produce.controller;

import java.text.ParseException;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesMaintenanceRecordItem;
import org.jeecg.modules.mes.produce.service.IMesMaintenanceRecordItemService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.mes.produce.vo.MaintenanceRecordVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 维护保养点检记录子表
 * @Author: jeecg-boot
 * @Date:   2021-06-11
 * @Version: V1.0
 */
@Api(tags="维护保养点检记录子表")
@RestController
@RequestMapping("/produce/mesMaintenanceRecordItem")
@Slf4j
public class MesMaintenanceRecordItemController extends JeecgController<MesMaintenanceRecordItem, IMesMaintenanceRecordItemService> {
	@Autowired
	private IMesMaintenanceRecordItemService mesMaintenanceRecordItemService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesMaintenanceRecordItem
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "维护保养点检记录子表-分页列表查询")
	@ApiOperation(value="维护保养点检记录子表-分页列表查询", notes="维护保养点检记录子表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesMaintenanceRecordItem mesMaintenanceRecordItem,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesMaintenanceRecordItem> queryWrapper = QueryGenerator.initQueryWrapper(mesMaintenanceRecordItem, req.getParameterMap());
		Page<MesMaintenanceRecordItem> page = new Page<MesMaintenanceRecordItem>(pageNo, pageSize);
		IPage<MesMaintenanceRecordItem> pageList = mesMaintenanceRecordItemService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	 @AutoLog(value = "维护保养点检记录子表-列表查询")
	 @ApiOperation(value="维护保养点检记录子表-列表查询", notes="维护保养点检记录子表-列表查询")
	 @GetMapping(value = "/queryList")
	 public Result<?> queryList(@RequestParam(name="tableNameCode") String tableNameCode,
								@RequestParam(name="inspectionDate") String inspectionDate) {
		 return mesMaintenanceRecordItemService.queryList(tableNameCode, inspectionDate);
	 }

	 @AutoLog(value = "维护保养点检记录子表-根据主表ID查询子表列表")
	 @ApiOperation(value="维护保养点检记录子表-根据主表ID查询子表列表", notes="维护保养点检记录子表-根据主表ID查询子表列表")
	 @GetMapping(value = "/queryListByMainId")
	 public Result<?> queryListByMainId(@RequestParam(name="mainId") String mainId) {
		 return mesMaintenanceRecordItemService.queryListByMainId(mainId);
	 }
	
	/**
	 *   添加
	 *
	 * @param mesMaintenanceRecordItem
	 * @return
	 */
	@AutoLog(value = "维护保养点检记录子表-添加")
	@ApiOperation(value="维护保养点检记录子表-添加", notes="维护保养点检记录子表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesMaintenanceRecordItem mesMaintenanceRecordItem) {
		return mesMaintenanceRecordItemService.addMesMaintenanceRecordItem(mesMaintenanceRecordItem);
	}
	
	/**
	 *  编辑
	 *
	 * @param mesMaintenanceRecordItem
	 * @return
	 */
	@AutoLog(value = "维护保养点检记录子表-编辑")
	@ApiOperation(value="维护保养点检记录子表-编辑", notes="维护保养点检记录子表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesMaintenanceRecordItem mesMaintenanceRecordItem) {
		return mesMaintenanceRecordItemService.editMesMaintenanceRecordItem(mesMaintenanceRecordItem);
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "维护保养点检记录子表-通过id删除")
	@ApiOperation(value="维护保养点检记录子表-通过id删除", notes="维护保养点检记录子表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesMaintenanceRecordItemService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "维护保养点检记录子表-批量删除")
	@ApiOperation(value="维护保养点检记录子表-批量删除", notes="维护保养点检记录子表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesMaintenanceRecordItemService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "维护保养点检记录子表-通过id查询")
	@ApiOperation(value="维护保养点检记录子表-通过id查询", notes="维护保养点检记录子表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesMaintenanceRecordItem mesMaintenanceRecordItem = mesMaintenanceRecordItemService.getById(id);
		if(mesMaintenanceRecordItem==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesMaintenanceRecordItem);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesMaintenanceRecordItem
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesMaintenanceRecordItem mesMaintenanceRecordItem) {
        return super.exportXls(request, mesMaintenanceRecordItem, MesMaintenanceRecordItem.class, "维护保养点检记录子表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesMaintenanceRecordItem.class);
    }

	 /**
	  *   APP添加
	  *
	  * @param maintenanceRecordVo
	  * @return
	  */
	 @AutoLog(value = "维护保养点检记录子表-APP添加")
	 @ApiOperation(value="维护保养点检记录子表-APP添加", notes="维护保养点检记录子表-APP添加")
	 @PostMapping(value = "/addMaintenanceRecordItemForApp")
	 public Result<?> addMaintenanceRecordItemForApp(@RequestBody MaintenanceRecordVo maintenanceRecordVo) {
	 	 try{
			 return mesMaintenanceRecordItemService.addMaintenanceRecordItemForApp(maintenanceRecordVo);
		 }catch (ParseException e){
	 	 	 e.printStackTrace();
			 return Result.error("出现异常，请稍后再试！");
		 }
	 }

}
