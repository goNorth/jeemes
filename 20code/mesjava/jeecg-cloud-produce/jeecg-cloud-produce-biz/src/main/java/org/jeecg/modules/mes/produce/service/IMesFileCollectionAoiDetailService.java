package org.jeecg.modules.mes.produce.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoiDetail;

/**
 * @Description: mes_file_collection_aoi_detail
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface IMesFileCollectionAoiDetailService extends IService<MesFileCollectionAoiDetail> {

}
