package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.FirstPieceDipLog;
import org.jeecg.modules.mes.produce.mapper.FirstPieceDipLogMapper;
import org.jeecg.modules.mes.produce.service.IFirstPieceDipLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: DIP首件确认表-检查内容
 * @Author: jeecg-boot
 * @Date:   2021-03-23
 * @Version: V1.0
 */
@Service
public class FirstPieceDipLogServiceImpl extends ServiceImpl<FirstPieceDipLogMapper, FirstPieceDipLog> implements IFirstPieceDipLogService {

}
