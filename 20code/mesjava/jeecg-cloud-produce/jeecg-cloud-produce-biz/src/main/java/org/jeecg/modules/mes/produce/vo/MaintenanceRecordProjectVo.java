package org.jeecg.modules.mes.produce.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
public class MaintenanceRecordProjectVo implements Serializable {
    @ApiModelProperty(value = "检查项目")
    private String inspectionItems;

    @ApiModelProperty(value = "衡量标准/UP")
    private String measureStandard;

    @ApiModelProperty(value = "检查结果/LOW")
    private String inspectionResults;

    @ApiModelProperty(value = "Zone")
    private String zoneOrTime;
}
