package org.jeecg.modules.mes.produce.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpiDetail;

/**
 * @Description: mes_file_collection_spi_detail
 * @Author: jeecg-boot
 * @Date:   2021-04-16
 * @Version: V1.0
 */
public interface MesFileCollectionSpiDetailMapper extends BaseMapper<MesFileCollectionSpiDetail> {

}
