package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesBackupWriteoff;
import org.jeecg.modules.mes.produce.service.IMesBackupWriteoffService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-备品报废
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="制造中心-备品报废")
@RestController
@RequestMapping("/produce/mesBackupWriteoff")
@Slf4j
public class MesBackupWriteoffController extends JeecgController<MesBackupWriteoff, IMesBackupWriteoffService> {
	@Autowired
	private IMesBackupWriteoffService mesBackupWriteoffService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesBackupWriteoff
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-备品报废-分页列表查询")
	@ApiOperation(value="制造中心-备品报废-分页列表查询", notes="制造中心-备品报废-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesBackupWriteoff mesBackupWriteoff,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesBackupWriteoff> queryWrapper = QueryGenerator.initQueryWrapper(mesBackupWriteoff, req.getParameterMap());
		Page<MesBackupWriteoff> page = new Page<MesBackupWriteoff>(pageNo, pageSize);
		IPage<MesBackupWriteoff> pageList = mesBackupWriteoffService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesBackupWriteoff
	 * @return
	 */
	@AutoLog(value = "制造中心-备品报废-添加")
	@ApiOperation(value="制造中心-备品报废-添加", notes="制造中心-备品报废-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesBackupWriteoff mesBackupWriteoff) {
		mesBackupWriteoffService.save(mesBackupWriteoff);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesBackupWriteoff
	 * @return
	 */
	@AutoLog(value = "制造中心-备品报废-编辑")
	@ApiOperation(value="制造中心-备品报废-编辑", notes="制造中心-备品报废-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesBackupWriteoff mesBackupWriteoff) {
		mesBackupWriteoffService.updateById(mesBackupWriteoff);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-备品报废-通过id删除")
	@ApiOperation(value="制造中心-备品报废-通过id删除", notes="制造中心-备品报废-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesBackupWriteoffService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-备品报废-批量删除")
	@ApiOperation(value="制造中心-备品报废-批量删除", notes="制造中心-备品报废-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesBackupWriteoffService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-备品报废-通过id查询")
	@ApiOperation(value="制造中心-备品报废-通过id查询", notes="制造中心-备品报废-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesBackupWriteoff mesBackupWriteoff = mesBackupWriteoffService.getById(id);
		if(mesBackupWriteoff==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesBackupWriteoff);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesBackupWriteoff
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesBackupWriteoff mesBackupWriteoff) {
        return super.exportXls(request, mesBackupWriteoff, MesBackupWriteoff.class, "制造中心-备品报废");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesBackupWriteoff.class);
    }

}
