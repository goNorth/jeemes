package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.FirstPieceDipLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: DIP首件确认表-检查内容
 * @Author: jeecg-boot
 * @Date:   2021-03-23
 * @Version: V1.0
 */
public interface FirstPieceDipLogMapper extends BaseMapper<FirstPieceDipLog> {

}
