package org.jeecg.modules.mes.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 订单管理—生产订单子表
 * @Author: jeecg-boot
 * @Date:   2020-11-06
 * @Version: V1.0
 */
@Data
@TableName("mes_produce_item")
@ApiModel(value="mes_produce_item对象", description="订单管理—生产订单")
public class MesProduceItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**物料id*/
	@Excel(name = "物料id", width = 15)
	@ApiModelProperty(value = "物料id")
	private java.lang.String materielId;
	/**物料类型*/
	@Excel(name = "物料类型", width = 15)
	@ApiModelProperty(value = "物料类型")
	private java.lang.String materielType;
	/**料号*/
	@Excel(name = "料号", width = 15)
	@ApiModelProperty(value = "料号")
	private java.lang.String materielCode;
	/**物料*/
	@Excel(name = "物料", width = 15)
	@ApiModelProperty(value = "物料")
	private java.lang.String materielName;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
	@ApiModelProperty(value = "物料规格")
	private java.lang.String materielGauge;
	/**物料阶别*/
	@Excel(name = "物料阶别", width = 15)
	@ApiModelProperty(value = "物料阶别")
	private java.lang.String materielGrade;
	/**需求数量*/
	@Excel(name = "需求数量", width = 15)
	@ApiModelProperty(value = "需求数量")
	private java.lang.String requireNum;
	/**计量单位*/
	@Excel(name = "计量单位", width = 15)
	@ApiModelProperty(value = "计量单位")
	private java.lang.String countUnit;
	/**项目类别*/
	@Excel(name = "项目类别", width = 15)
	@ApiModelProperty(value = "项目类别")
	private java.lang.String projectSort;
	/**工序*/
	@Excel(name = "工序", width = 15)
	@ApiModelProperty(value = "工序")
	private java.lang.String process;
	/**顺序*/
	@Excel(name = "顺序", width = 15)
	@ApiModelProperty(value = "顺序")
	private java.lang.String sequence;
	/**提货数量*/
	@Excel(name = "提货数量", width = 15)
	@ApiModelProperty(value = "提货数量")
	private java.lang.String pickupNum;
	/**承诺数量*/
	@Excel(name = "承诺数量", width = 15)
	@ApiModelProperty(value = "承诺数量")
	private java.lang.String commitNum;
	/**工厂id*/
	@Excel(name = "工厂id", width = 15)
	@ApiModelProperty(value = "工厂id")
	private java.lang.String factoryId;
	/**工厂编号*/
	@Excel(name = "工厂编号", width = 15)
	@ApiModelProperty(value = "工厂编号")
	private java.lang.String factoryCode;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
	@ApiModelProperty(value = "工厂名称")
	private java.lang.String factoryName;
	/**库存地点*/
	@Excel(name = "库存地点", width = 15)
	@ApiModelProperty(value = "库存地点")
	private java.lang.String storageSite;
	/**库存地点*/
	@Excel(name = "库存数量", width = 15)
	@ApiModelProperty(value = "库存数量")
	private java.lang.String stockNum;
	/**批次*/
	@Excel(name = "批次", width = 15)
	@ApiModelProperty(value = "批次")
	private java.lang.String batchNum;
	/**反冲*/
	@Excel(name = "反冲", width = 15)
    @Dict(dicCode = "yn")
	@ApiModelProperty(value = "反冲")
	private java.lang.String inverseCharge;
	/**已删除*/
	@Excel(name = "已删除", width = 15)
    @Dict(dicCode = "yn")
	@ApiModelProperty(value = "已删除")
	private java.lang.String haveDelete;
	/**最后发货*/
	@Excel(name = "最后发货", width = 15)
    @Dict(dicCode = "yn")
	@ApiModelProperty(value = "最后发货")
	private java.lang.String lastDelivery;
	/**需求日期*/
	@Excel(name = "需求日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "需求日期")
	private java.util.Date requireDate;
	/**生产订单id*/
	@ApiModelProperty(value = "生产订单id")
	private java.lang.String proorderId;
	/**行号*/
	@Excel(name = "行号", width = 15)
	@ApiModelProperty(value = "行号")
	private java.lang.String rowNum;
	/**点位*/
	@Excel(name = "点位", width = 15)
	@ApiModelProperty(value = "点位")
	private java.lang.String spot;
	/**用量*/
	@Excel(name = "用量", width = 15)
	@ApiModelProperty(value = "用量")
	private java.lang.String quantity;
	/**反面点位*/
	@Excel(name = "反面点位", width = 15)
	@ApiModelProperty(value = "反面点位")
	private java.lang.String inverseSpot;
	/**反面用量*/
	@Excel(name = "反面用量", width = 15)
	@ApiModelProperty(value = "反面用量")
	private java.lang.String inverseQuantity;
	/**正面点位*/
	@Excel(name = "正面点位", width = 15)
	@ApiModelProperty(value = "正面点位")
	private java.lang.String frontSpot;
	/**正面用量*/
	@Excel(name = "正面用量", width = 15)
	@ApiModelProperty(value = "正面用量")
	private java.lang.String frontQuantity;
	/**已发料数量*/
	@Excel(name = "已发料数量", width = 15)
	@ApiModelProperty(value = "已发料数量")
	private java.lang.String deliveryNum;
	/**未上料数量*/
	@Excel(name = "未上料数量", width = 15)
	@ApiModelProperty(value = "未上料数量")
	private java.lang.String unglazeNum;
	/**发料状态*/
	@Excel(name = "发料状态", width = 15)
	@ApiModelProperty(value = "发料状态")
	private java.lang.String ifDelivery;
	/**上料状态*/
	@Excel(name = "上料状态", width = 15)
	@ApiModelProperty(value = "上料状态")
	private java.lang.String ifGlaze;
	/**真实数量*/
	@Excel(name = "真实数量", width = 15)
	@ApiModelProperty(value = "真实数量")
	private java.lang.String realNum;
	/**所差数量*/
	@Excel(name = "所差数量", width = 15)
	@ApiModelProperty(value = "所差数量")
	@TableField(exist = false)
	private java.lang.String poorNum;


}
