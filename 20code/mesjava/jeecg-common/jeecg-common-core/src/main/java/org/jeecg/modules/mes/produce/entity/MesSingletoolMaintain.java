package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 制造中心-单个制具维保
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Data
@TableName("mes_singletool_maintain")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_singletool_maintain对象", description="制造中心-单个制具维保")
public class MesSingletoolMaintain implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**制具SN*/
	@Excel(name = "制具SN", width = 15)
    @ApiModelProperty(value = "制具SN")
    private java.lang.String maketoolSn;
	/**制具料号*/
	@Excel(name = "制具料号", width = 15)
    @ApiModelProperty(value = "制具料号")
    private java.lang.String maketoolCode;
	/**制具名称*/
	@Excel(name = "制具名称", width = 15)
    @ApiModelProperty(value = "制具名称")
    private java.lang.String maketoolName;
	/**操作类型*/
	@Excel(name = "操作类型", width = 15)
    @ApiModelProperty(value = "操作类型")
    private java.lang.String operationType;
	/**不良代码*/
	@Excel(name = "不良代码", width = 15)
    @ApiModelProperty(value = "不良代码")
    private java.lang.String badCode;
	/**不良描述*/
	@Excel(name = "不良描述", width = 15)
    @ApiModelProperty(value = "不良描述")
    private java.lang.String badDescription;
	/**保养/维修人*/
	@Excel(name = "保养/维修人", width = 15)
    @ApiModelProperty(value = "保养/维修人")
    private java.lang.String maintainPerson;
	/**确认人*/
	@Excel(name = "确认人", width = 15)
    @ApiModelProperty(value = "确认人")
    private java.lang.String confirmPerson;
	/**报修人*/
	@Excel(name = "报修人", width = 15)
    @ApiModelProperty(value = "报修人")
    private java.lang.String reportFixperson;
	/**左上张力*/
	@Excel(name = "左上张力", width = 15)
    @ApiModelProperty(value = "左上张力")
    private java.lang.String leftupPower;
	/**右上张力*/
	@Excel(name = "右上张力", width = 15)
    @ApiModelProperty(value = "右上张力")
    private java.lang.String rightupPower;
	/**中间张力*/
	@Excel(name = "中间张力", width = 15)
    @ApiModelProperty(value = "中间张力")
    private java.lang.String middlePower;
	/**左下张力*/
	@Excel(name = "左下张力", width = 15)
    @ApiModelProperty(value = "左下张力")
    private java.lang.String leftdownPower;
	/**右下张力*/
	@Excel(name = "右下张力", width = 15)
    @ApiModelProperty(value = "右下张力")
    private java.lang.String rightdownPower;
	/**张力差异*/
	@Excel(name = "张力差异", width = 15)
    @ApiModelProperty(value = "张力差异")
    private java.lang.String powerGap;
	/**厚度*/
	@Excel(name = "厚度", width = 15)
    @ApiModelProperty(value = "厚度")
    private java.lang.String thickDegree;
	/**平整度*/
	@Excel(name = "平整度", width = 15)
    @ApiModelProperty(value = "平整度")
    private java.lang.String flattenDegree;
	/**保养/维修内容*/
	@Excel(name = "保养/维修内容", width = 15)
    @ApiModelProperty(value = "保养/维修内容")
    private java.lang.String maintainContent;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
