package org.jeecg.modules.mes.produce.vo;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesManytoolMaintaininfo;
import org.jeecg.modules.mes.produce.entity.MesManytoolInfo;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 多个制具维保-保养信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_manytool_maintaininfoPage对象", description="多个制具维保-保养信息")
public class MesManytoolMaintaininfoPage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**操作类型*/
	@Excel(name = "操作类型", width = 15)
	@ApiModelProperty(value = "操作类型")
	private java.lang.String operationType;
	/**报修人*/
	@Excel(name = "报修人", width = 15)
	@ApiModelProperty(value = "报修人")
	private java.lang.String reportFixperson;
	/**保养/维修人*/
	@Excel(name = "保养/维修人", width = 15)
	@ApiModelProperty(value = "保养/维修人")
	private java.lang.String maintainPerson;
	/**确认人*/
	@Excel(name = "确认人", width = 15)
	@ApiModelProperty(value = "确认人")
	private java.lang.String confirmPerson;
	/**不良代码*/
	@Excel(name = "不良代码", width = 15)
	@ApiModelProperty(value = "不良代码")
	private java.lang.String badCode;
	/**不良描述*/
	@Excel(name = "不良描述", width = 15)
	@ApiModelProperty(value = "不良描述")
	private java.lang.String badDescription;
	/**保养/维修内容*/
	@Excel(name = "保养/维修内容", width = 15)
	@ApiModelProperty(value = "保养/维修内容")
	private java.lang.String maintainContent;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="多个制具维保-制具信息")
	@ApiModelProperty(value = "多个制具维保-制具信息")
	private List<MesManytoolInfo> mesManytoolInfoList;
	
}
