package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 维护保养点检记录子表
 * @Author: jeecg-boot
 * @Date:   2021-06-11
 * @Version: V1.0
 */
@Data
@TableName("mes_maintenance_record_item")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_maintenance_record_item对象", description="维护保养点检记录子表")
public class MesMaintenanceRecordItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**主表ID*/
	@Excel(name = "主表ID", width = 15)
    @ApiModelProperty(value = "主表ID")
    private java.lang.String mainId;
	/**表类型*/
	@Excel(name = "表类型", width = 15)
    @ApiModelProperty(value = "表类型")
    private java.lang.String tableType;
	/**表名*/
	@Excel(name = "表名", width = 15)
    @ApiModelProperty(value = "表名")
    private java.lang.String tableName;
	/**检查日期*/
	@Excel(name = "检查日期", width = 15, format = "yyyy-MM")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM")
    @DateTimeFormat(pattern="yyyy-MM")
    @ApiModelProperty(value = "检查日期")
    private java.util.Date inspectionDate;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**设备名称*/
	@Excel(name = "设备名称", width = 15)
    @ApiModelProperty(value = "设备名称")
    private java.lang.String equipmentName;
	/**设备型号*/
	@Excel(name = "设备型号", width = 15)
    @ApiModelProperty(value = "设备型号")
    private java.lang.String equipmentModel;
	/**设备SN*/
	@Excel(name = "设备SN", width = 15)
    @ApiModelProperty(value = "设备SN")
    private java.lang.String equipmentSn;
	/**购置日期*/
	@Excel(name = "购置日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "购置日期")
    private java.util.Date datePurchase;
	/**资产编码*/
	@Excel(name = "资产编码", width = 15)
    @ApiModelProperty(value = "资产编码")
    private java.lang.String documentNumber;
	/**检查方法*/
	@Excel(name = "检查方法", width = 15)
    @ApiModelProperty(value = "检查方法")
    private java.lang.String inspectionMethod;
	/**检查周期*/
	@Excel(name = "检查周期", width = 15)
    @ApiModelProperty(value = "检查周期")
    private java.lang.String inspectionCycle;
	/**计入方式*/
	@Excel(name = "计入方式", width = 15)
    @ApiModelProperty(value = "计入方式")
    private java.lang.String accountingMethod;
	/**注意事项*/
	@Excel(name = "注意事项", width = 15)
    @ApiModelProperty(value = "注意事项")
    private java.lang.String beCareful;
	/**记录类型*/
	@Excel(name = "记录类型", width = 15, dicCode = "maintenance_cycle")
	@Dict(dicCode = "maintenance_cycle")
    @ApiModelProperty(value = "记录类型")
    private java.lang.String recordType;
	/**类型值*/
	@Excel(name = "类型值", width = 15)
    @ApiModelProperty(value = "类型值")
    private java.lang.String typeValue;
	/**检查项目*/
	@Excel(name = "检查项目", width = 15)
    @ApiModelProperty(value = "检查项目")
    private java.lang.String inspectionItems;
	/**衡量标准*/
	@Excel(name = "衡量标准", width = 15)
    @ApiModelProperty(value = "衡量标准")
    private java.lang.String measureStandard;
	/**检查结果*/
	@Excel(name = "检查结果", width = 15)
    @ApiModelProperty(value = "检查结果")
    private java.lang.String inspectionResults;
	/**保养人*/
	@Excel(name = "保养人", width = 15)
    @ApiModelProperty(value = "保养人")
    private java.lang.String maintainer;
	/**审核人*/
	@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String reviewer;
	/**表类型编码*/
	@Excel(name = "表类型编码", width = 15, dicCode = "facility_temp_type")
	@Dict(dicCode = "facility_temp_type")
    @ApiModelProperty(value = "表类型编码")
    private java.lang.String tableTypeCode;
	/**表名编码*/
	@Excel(name = "表名编码", width = 15, dicCode = "facility_name_temp")
	@Dict(dicCode = "facility_name_temp")
    @ApiModelProperty(value = "表名编码")
    private java.lang.String tableNameCode;
}
