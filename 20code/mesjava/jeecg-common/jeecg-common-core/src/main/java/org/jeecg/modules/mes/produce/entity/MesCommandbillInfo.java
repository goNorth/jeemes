package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 制造中心-制令单信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Data
@TableName("mes_commandbill_info")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_commandbill_info对象", description="制造中心-制令单信息")
public class MesCommandbillInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**制令单号*/
	@Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String commandbillCode;
	/**工单号*/
	@Excel(name = "工单号", width = 15)
    @ApiModelProperty(value = "工单号")
    private java.lang.String workbillCode;
	/**生产阶别*/
    @Excel(name = "生产阶别", width = 15, dicCode = "produce_grade")
    @Dict(dicCode = "produce_grade")
    @ApiModelProperty(value = "生产阶别")
    private java.lang.String produceGrade;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**加工面别*/
	@Excel(name = "加工面别", width = 15)
    @ApiModelProperty(value = "加工面别")
    private java.lang.String processFace;
	/**PCB料号*/
	@Excel(name = "PCB料号", width = 15)
    @ApiModelProperty(value = "PCB料号")
    private java.lang.String pcbCode;
	/**机种料号*/
	@Excel(name = "机种料号", width = 15)
    @ApiModelProperty(value = "机种料号")
    private java.lang.String mechanismCode;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
    @ApiModelProperty(value = "机种名称")
    private java.lang.String mechanismName;
	/**规格*/
	@Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private java.lang.String gague;
	/**工艺*/
	@Excel(name = "工艺", width = 15)
    @ApiModelProperty(value = "工艺")
    private java.lang.String progress;
	/**起始工序*/
	@Excel(name = "起始工序", width = 15)
    @ApiModelProperty(value = "起始工序")
    private java.lang.String startProgress;
	/**结束工序*/
	@Excel(name = "结束工序", width = 15)
    @ApiModelProperty(value = "结束工序")
    private java.lang.String endProgress;
	/**预计开工时间*/
	@Excel(name = "预计开工时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预计开工时间")
    private java.util.Date expectBegintime;
	/**预计完工时间*/
	@Excel(name = "预计完工时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预计完工时间")
    private java.util.Date expectFinishtime;
	/**下达时间*/
	@Excel(name = "下达时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "下达时间")
    private java.util.Date arriveTime;
	/**计划数量*/
	@Excel(name = "计划数量", width = 15)
    @ApiModelProperty(value = "计划数量")
    private java.lang.String plantNum;
	/**联板数*/
	@Excel(name = "联板数", width = 15)
    @ApiModelProperty(value = "联板数")
    private java.lang.String linkBoardnum;
	/**条码拼板数*/
	@Excel(name = "条码拼板数", width = 15)
    @ApiModelProperty(value = "条码拼板数")
    private java.lang.String barcodeBoardnum;
	/**产品条码规则*/
	@Excel(name = "产品条码规则", width = 15)
    @ApiModelProperty(value = "产品条码规则")
    private java.lang.String productCoderule;
	/**小板条码规则*/
	@Excel(name = "小板条码规则", width = 15)
    @ApiModelProperty(value = "小板条码规则")
    private java.lang.String smallCoderule;
	/**中转条码规则*/
	@Excel(name = "中转条码规则", width = 15)
    @ApiModelProperty(value = "中转条码规则")
    private java.lang.String transitCoderule;
	/**不良报废*/
	@Excel(name = "不良报废", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "不良报废")
    private java.lang.String badWriteoff;
	/**管控类型*/
	@Excel(name = "管控类型", width = 15)
    @ApiModelProperty(value = "管控类型")
    private java.lang.String controlType;
	/**维修上限次数*/
	@Excel(name = "维修上限次数", width = 15)
    @ApiModelProperty(value = "维修上限次数")
    private java.lang.String repairLimit;
	/**客户名称*/
	@Excel(name = "客户名称", width = 15)
    @ApiModelProperty(value = "客户名称")
    private java.lang.String clientName;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
    /**生产订单id*/
    @Excel(name = "生产订单id", width = 15)
    @ApiModelProperty(value = "生产订单id")
    private java.lang.String produceId;
	/**工单id*/
	@Excel(name = "工单id", width = 15)
    @ApiModelProperty(value = "工单id")
    private java.lang.String workbillId;
    /**状态*/
    @Excel(name = "状态", width = 15)
    @Dict(dicCode = "state")
    @ApiModelProperty(value = "状态")
    private java.lang.String state;
	/**发料状态*/
	@Excel(name = "发料状态", width = 15)
    @ApiModelProperty(value = "发料状态")
    private java.lang.String deliveryState;
	/**上料状态*/
	@Excel(name = "上料状态", width = 15)
    @ApiModelProperty(value = "上料状态")
    private java.lang.String glazeState;
	/**入库状态*/
	@Excel(name = "入库状态", width = 15)
    @ApiModelProperty(value = "入库状态")
    private java.lang.String inputState;
	/**首件状态*/
	@Excel(name = "首件状态", width = 15)
    @ApiModelProperty(value = "首件状态")
    private java.lang.String firstState;
    /**抽检状态*/
    @Excel(name = "抽检状态", width = 15)
    @ApiModelProperty(value = "抽检状态")
    private java.lang.String checkState;
    /**转产状态*/
    @Excel(name = "转产状态", width = 15)
    @ApiModelProperty(value = "转产状态")
    private java.lang.String changeState;
	/**未入库数量*/
	@Excel(name = "未入库数量", width = 15)
    @ApiModelProperty(value = "未入库数量")
    private java.lang.String uninputNum;
    /**上料提醒*/
    @Excel(name = "上料提醒", width = 15)
    @ApiModelProperty(value = "上料提醒")
    private java.lang.String glazeRemind;
    /**打印时间*/
    @TableField(exist = false)
    private java.lang.String printTime;
    @Excel(name = "生产状态", width = 15)
    @ApiModelProperty(value = "生产状态(0:未生产  1:生产中)")
    private java.lang.String produceState;
    /**制令单bom*/
    @TableField(exist = false)
    private List<MesCommandbillPitem> mesCommandbillPitemList;
    /**转产编号*/
    @TableField(exist = false)
    private String mobileType;
    /**需要转的制令单code*/
    @TableField(exist = false)
    private String newCode;
    @ApiModelProperty(value = "客户料号")
    @TableField(exist = false)
    private java.lang.String clientCode;
}
