package org.jeecg.modules.mes.storage.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 仓库管理—调拨移动子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@ApiModel(value="mes_storage_transfer对象", description="仓库管理—调拨移动")
@Data
@TableName("mes_transfer_item")
public class MesTransferItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**行号*/
	@Excel(name = "行号", width = 15)
	@ApiModelProperty(value = "行号")
	private java.lang.String rowCode;
	/**产品编号*/
	@Excel(name = "产品编号", width = 15)
	@ApiModelProperty(value = "产品编号")
	private java.lang.String productCode;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
	@ApiModelProperty(value = "产品名称")
	private java.lang.String productName;
	/**货主编号*/
	@Excel(name = "货主编号", width = 15)
	@ApiModelProperty(value = "货主编号")
	private java.lang.String shipperCode;
	/**批号*/
	@Excel(name = "批号", width = 15)
	@ApiModelProperty(value = "批号")
	private java.lang.String batchCode;
	/**包装编码*/
	@Excel(name = "包装编码", width = 15)
	@ApiModelProperty(value = "包装编码")
	private java.lang.String packageCode;
	/**包装单位*/
	@Excel(name = "包装单位", width = 15)
	@ApiModelProperty(value = "包装单位")
	private java.lang.String packageUnit;
	/**单位*/
	@Excel(name = "单位", width = 15)
	@ApiModelProperty(value = "单位")
	private java.lang.String unit;
	/**移动数量*/
	@Excel(name = "移动数量", width = 15)
	@ApiModelProperty(value = "移动数量")
	private java.lang.String mobileNum;
	/**源仓库*/
	@Excel(name = "源仓库", width = 15)
	@ApiModelProperty(value = "源仓库")
	private java.lang.String sourceWare;
	/**源区域码*/
	@Excel(name = "源区域码", width = 15)
	@ApiModelProperty(value = "源区域码")
	private java.lang.String sourceRegioncode;
	/**源库位*/
	@Excel(name = "源库位", width = 15)
	@ApiModelProperty(value = "源库位")
	private java.lang.String sourceWaresite;
	/**目标仓库*/
	@Excel(name = "目标仓库", width = 15)
	@ApiModelProperty(value = "目标仓库")
	private java.lang.String targetWare;
	/**目标区域码*/
	@Excel(name = "目标区域码", width = 15)
	@ApiModelProperty(value = "目标区域码")
	private java.lang.String targetRegioncode;
	/**目标库位*/
	@Excel(name = "目标库位", width = 15)
	@ApiModelProperty(value = "目标库位")
	private java.lang.String targetWaresite;
	/**调拨id*/
	@ApiModelProperty(value = "调拨id")
	private java.lang.String transferId;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
