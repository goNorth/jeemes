package org.jeecg.modules.mes.storage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 仓库管理-库存管理
 * @Author: jeecg-boot
 * @Date:   2020-11-11
 * @Version: V1.0
 */
@Data
@TableName("mes_stock_manage")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_stock_manage对象", description="仓库管理-库存管理")
public class MesStockManage implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**物料id*/
//	@Excel(name = "物料id", width = 15)
    @ApiModelProperty(value = "物料id")
    private java.lang.String materielId;
	/**物料料号*/
	@Excel(name = "物料料号", width = 15)
    @ApiModelProperty(value = "物料料号")
    private java.lang.String materielCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materielName;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
    @ApiModelProperty(value = "物料规格")
    private java.lang.String materielGague;
	/**工厂id*/
//	@Excel(name = "工厂id", width = 15)
    @ApiModelProperty(value = "工厂id")
    private java.lang.String factoryId;
	/**工厂编号*/
	@Excel(name = "工厂编号", width = 15)
    @ApiModelProperty(value = "工厂编号")
    private java.lang.String factoryCode;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
    @ApiModelProperty(value = "工厂名称")
    private java.lang.String factoryName;
	/**仓库id*/
//	@Excel(name = "仓库id", width = 15)
    @ApiModelProperty(value = "仓库id")
    private java.lang.String storeId;
	/**仓库编号*/
	@Excel(name = "仓库编号", width = 15)
    @ApiModelProperty(value = "仓库编号")
    private java.lang.String storeCode;
	/**仓库名称*/
	@Excel(name = "仓库名称", width = 15)
    @ApiModelProperty(value = "仓库名称")
    private java.lang.String storeName;
	/**库存数量*/
	@Excel(name = "库存数量", width = 15)
    @ApiModelProperty(value = "库存数量")
    private java.lang.String stockNum;
	/**单位*/
	@Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
	/**库存临界点*/
	@Excel(name = "库存临界点", width = 15)
    @ApiModelProperty(value = "库存临界点")
    private java.lang.String minimum;
	/**期初库存*/
	@Excel(name = "期初库存", width = 15)
    @ApiModelProperty(value = "期初库存")
    private java.lang.String query3;
	/**期初时间*/
	@Excel(name = "期初时间", width = 15)
    @ApiModelProperty(value = "期初时间")
    private java.lang.String query4;
	/**物料类型*/
	@Excel(name = "物料类型", width = 15)
    @ApiModelProperty(value = "物料类型")
    private java.lang.String query5;
	/**客户料号*/
	@Excel(name = "客户料号", width = 15)
    @ApiModelProperty(value = "客户料号")
    private java.lang.String query6;
    /**区域编码*/
    @Excel(name = "区域编码", width = 15)
    @ApiModelProperty(value = "区域编码")
    private java.lang.String areaCode;
    /**位置编码*/
    @Excel(name = "位置编码", width = 15)
    @ApiModelProperty(value = "位置编码")
    private java.lang.String locationCode;
}
