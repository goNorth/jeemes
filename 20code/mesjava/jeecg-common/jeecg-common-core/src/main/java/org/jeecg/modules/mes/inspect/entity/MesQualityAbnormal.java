package org.jeecg.modules.mes.inspect.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 质检中心-品质异常单
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Data
@TableName("mes_quality_abnormal")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_quality_abnormal对象", description="质检中心-品质异常单")
public class MesQualityAbnormal implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**异常单号*/
	@Excel(name = "异常单号", width = 15)
    @ApiModelProperty(value = "异常单号")
    private java.lang.String abnormalCode;
	/**制令单*/
	@Excel(name = "制令单", width = 15)
    @ApiModelProperty(value = "制令单")
    private java.lang.String commandBill;
	/**机种料号*/
	@Excel(name = "机种料号", width = 15)
    @ApiModelProperty(value = "机种料号")
    private java.lang.String machineCode;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
    @ApiModelProperty(value = "机种名称")
    private java.lang.String machineName;
	/**机种规格*/
	@Excel(name = "机种规格", width = 15)
    @ApiModelProperty(value = "机种规格")
    private java.lang.String machineGague;
	/**客户*/
	@Excel(name = "客户", width = 15)
    @ApiModelProperty(value = "客户")
    private java.lang.String client;
	/**检验批号*/
	@Excel(name = "检验批号", width = 15)
    @ApiModelProperty(value = "检验批号")
    private java.lang.String inspectBatch;
	/**送检数量*/
	@Excel(name = "送检数量", width = 15)
    @ApiModelProperty(value = "送检数量")
    private java.lang.String sendNum;
	/**检验数量*/
	@Excel(name = "检验数量", width = 15)
    @ApiModelProperty(value = "检验数量")
    private java.lang.String inspectNum;
	/**不良数量*/
	@Excel(name = "不良数量", width = 15)
    @ApiModelProperty(value = "不良数量")
    private java.lang.String badNum;
	/**生产区域*/
	@Excel(name = "生产区域", width = 15)
    @ApiModelProperty(value = "生产区域")
    private java.lang.String produceRegion;
	/**异常类型*/
	@Excel(name = "异常类型", width = 15)
    @ApiModelProperty(value = "异常类型")
    private java.lang.String abnormalType;
	/**附件*/
	@Excel(name = "附件", width = 15)
    @ApiModelProperty(value = "附件")
    private java.lang.String appendix;
	/**异常描述*/
	@Excel(name = "异常描述", width = 15)
    @ApiModelProperty(value = "异常描述")
    private java.lang.String abnormalContent;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
