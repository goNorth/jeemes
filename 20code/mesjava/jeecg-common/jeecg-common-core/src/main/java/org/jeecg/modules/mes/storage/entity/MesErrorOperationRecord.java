package org.jeecg.modules.mes.storage.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 错误操作记录
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
@Data
@TableName("mes_error_operation_record")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_error_operation_record对象", description="错误操作记录")
public class MesErrorOperationRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**操作人ID*/
	@Excel(name = "操作人ID", width = 15)
    @ApiModelProperty(value = "操作人ID")
    private java.lang.String operatorId;
	/**操作人账号*/
	@Excel(name = "操作人账号", width = 15)
    @ApiModelProperty(value = "操作人账号")
    private java.lang.String operatorAccount;
	/**操作人电话*/
	@Excel(name = "操作人电话", width = 15)
    @ApiModelProperty(value = "操作人电话")
    private java.lang.String phone;
	/**用户状态*/
	@Excel(name = "用户状态", width = 15)
    @ApiModelProperty(value = "用户状态")
    private java.lang.String userStatus;
	/**操作步骤*/
	@Excel(name = "操作步骤", width = 15)
    @ApiModelProperty(value = "操作步骤")
    private java.lang.String operationSteps;
	/**错误步骤*/
	@Excel(name = "错误步骤", width = 15)
    @ApiModelProperty(value = "错误步骤")
    private java.lang.String wrongSteps;
	/**错误原因*/
	@Excel(name = "错误原因", width = 15)
    @ApiModelProperty(value = "错误原因")
    private java.lang.String errorCause;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**阶别*/
	@Excel(name = "阶别", width = 15)
    @ApiModelProperty(value = "阶别")
    private java.lang.String produceGrade;
	/**调用地址*/
	@Excel(name = "调用地址", width = 15)
    @ApiModelProperty(value = "调用地址")
    private java.lang.String callAddress;
	/**排查方案*/
	@Excel(name = "排查方案", width = 15)
    @ApiModelProperty(value = "排查方案")
    private java.lang.String investigationScheme;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
}
