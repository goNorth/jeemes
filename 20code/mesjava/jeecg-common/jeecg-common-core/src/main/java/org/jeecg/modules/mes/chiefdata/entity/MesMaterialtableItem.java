package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 主数据—料站表明细
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@ApiModel(value="mes_chiefdata_materialtable对象", description="主数据—料表管理")
@Data
@TableName("mes_materialtable_item")
public class MesMaterialtableItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**贴片机型*/
	@Excel(name = "贴片机型", width = 15)
	@ApiModelProperty(value = "贴片机型")
	private java.lang.String pieceMachine;
	/**程式名称*/
	@Excel(name = "程式名称", width = 15)
	@ApiModelProperty(value = "程式名称")
	private java.lang.String formulaName;
	/**设备序号*/
	@Excel(name = "设备序号", width = 15)
	@ApiModelProperty(value = "设备序号")
	private java.lang.String deviceRank;
	/**TABLE*/
	@Excel(name = "TABLE", width = 15)
	@ApiModelProperty(value = "TABLE")
	private java.lang.String mtable;
	/**料站*/
	@Excel(name = "料站", width = 15)
	@ApiModelProperty(value = "料站")
	private java.lang.String materielStation;
	/**通道*/
	@Excel(name = "通道", width = 15)
	@ApiModelProperty(value = "通道")
	private java.lang.String passage;
	/**物料料号*/
	@Excel(name = "物料料号", width = 15)
	@ApiModelProperty(value = "物料料号")
	private java.lang.String materielCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
	@ApiModelProperty(value = "物料名称")
	private java.lang.String materielName;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
	@ApiModelProperty(value = "物料规格")
	private java.lang.String materielGague;
	/**FEEDER规格*/
	@Excel(name = "FEEDER规格", width = 15)
	@ApiModelProperty(value = "FEEDER规格")
	private java.lang.String feederGague;
	/**TRAY盘物料*/
	@Excel(name = "TRAY盘物料", width = 15)
	@ApiModelProperty(value = "TRAY盘物料")
	private java.lang.String trayPlate;
	/**跳过标志*/
	@Excel(name = "跳过标志", width = 15, dicCode = "yn")
	@ApiModelProperty(value = "跳过标志")
	private java.lang.String skipToken;
	/**点数*/
	@Excel(name = "点数", width = 15)
	@ApiModelProperty(value = "点数")
	private java.lang.String pointNum;
	/**点位*/
	@Excel(name = "点位", width = 15)
	@ApiModelProperty(value = "点位")
	private java.lang.String pointLocation;
	/**BOM范围*/
	@Excel(name = "BOM范围", width = 15)
	@ApiModelProperty(value = "BOM范围")
	private java.lang.String bomRange;
	/**料表id*/
	@ApiModelProperty(value = "料表id")
	private java.lang.String mtableId;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
