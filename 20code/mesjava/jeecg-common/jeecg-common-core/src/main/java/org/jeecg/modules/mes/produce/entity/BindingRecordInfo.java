package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 钢网、刮刀绑定记录
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Data
@TableName("binding_record_info")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="binding_record_info对象", description="钢网、刮刀绑定记录")
public class BindingRecordInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**SN编码*/
	@Excel(name = "SN编码", width = 15)
    @ApiModelProperty(value = "SN编码")
    private java.lang.String bindingSn;
	/**品名*/
	@Excel(name = "品名", width = 15)
    @ApiModelProperty(value = "品名：钢网、刮刀")
    private java.lang.String bindingName;
	/**制令单编号*/
	@Excel(name = "制令单编号", width = 15)
    @ApiModelProperty(value = "制令单编号")
    private java.lang.String commandbillCode;
	/**制令单名称*/
	@Excel(name = "制令单名称", width = 15)
    @ApiModelProperty(value = "制令单名称")
    private java.lang.String commandbillName;
	/**机种料号*/
	@Excel(name = "机种料号", width = 15)
    @ApiModelProperty(value = "机种料号")
    private java.lang.String mechanismCode;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
    @ApiModelProperty(value = "机种名称")
    private java.lang.String mechanismName;
	/**规格*/
	@Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private java.lang.String gague;
	/**计划数量*/
	@Excel(name = "计划数量", width = 15)
    @ApiModelProperty(value = "计划数量")
    private java.lang.String plantNum;
	/**状态 1 被替换 0当前使用*/
	@Excel(name = "计划数量", width = 15)
    @ApiModelProperty(value = "状态 1 被替换 0当前使用")
    private java.lang.String state;
}
