package org.jeecg.modules.mes.storage.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 物料追溯表
 * @Author: jeecg-boot
 * @Date:   2021-06-02
 * @Version: V1.0
 */
@Data
@TableName("mes_materiel_traceability")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_materiel_traceability对象", description="物料追溯表")
public class MesMaterielTraceability implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**物料编码*/
	@Excel(name = "物料编码", width = 15)
    @ApiModelProperty(value = "物料编码")
    private java.lang.String productCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String productName;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
    @ApiModelProperty(value = "物料规格")
    private java.lang.String materielGauge;
	/**物料来源*/
	@Excel(name = "物料来源", width = 15)
    @ApiModelProperty(value = "物料来源")
    private java.lang.String supplierName;
	/**打印时间*/
	@Excel(name = "打印时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "打印时间")
    private java.util.Date createTimeDy;
	/**打印人*/
	@Excel(name = "打印人", width = 15)
    @ApiModelProperty(value = "打印人")
    private java.lang.String createByDy;
	/**收货时间*/
	@Excel(name = "收货时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "收货时间")
    private java.util.Date createTimeSh;
	/**收货人*/
	@Excel(name = "收货人", width = 15)
    @ApiModelProperty(value = "收货人")
    private java.lang.String createBySh;
	/**收货数量*/
	@Excel(name = "收货数量", width = 15)
    @ApiModelProperty(value = "收货数量")
    private java.lang.String inwareNumSh;
	/**入库时间*/
	@Excel(name = "入库时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "入库时间")
    private java.util.Date createTimeRk;
	/**入库人*/
	@Excel(name = "入库人", width = 15)
    @ApiModelProperty(value = "入库人")
    private java.lang.String createByRk;
	/**入库数量*/
	@Excel(name = "入库数量", width = 15)
    @ApiModelProperty(value = "入库数量")
    private java.lang.String inwareNumRk;
	/**发料时间*/
	@Excel(name = "发料时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发料时间")
    private java.util.Date createTimeFl;
	/**发料人*/
	@Excel(name = "发料人", width = 15)
    @ApiModelProperty(value = "发料人")
    private java.lang.String createByFl;
	/**发料数量*/
	@Excel(name = "发料数量", width = 15)
    @ApiModelProperty(value = "发料数量")
    private java.lang.String outwareNum;
	/**物料结余*/
	@Excel(name = "物料结余", width = 15)
    @ApiModelProperty(value = "物料结余")
    private java.lang.String productJc;

    @ApiModelProperty(value = "开始时间")
    private String createTimeDy_begin;
    @ApiModelProperty(value = "结束时间")
    private String createTimeDy_end;

    @ApiModelProperty(value = "开始时间")
    private String createTimeSh_begin;
    @ApiModelProperty(value = "结束时间")
    private String createTimeSh_end;

    @ApiModelProperty(value = "开始时间")
    private String createTimeRk_begin;
    @ApiModelProperty(value = "结束时间")
    private String createTimeRk_end;

    @ApiModelProperty(value = "开始时间")
    private String createTimeFl_begin;
    @ApiModelProperty(value = "结束时间")
    private String createTimeFl_end;
}
