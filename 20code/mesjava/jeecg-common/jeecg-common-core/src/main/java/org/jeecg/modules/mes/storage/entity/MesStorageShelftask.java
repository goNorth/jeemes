package org.jeecg.modules.mes.storage.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 仓库管理—上架/下架任务
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@TableName("mes_storage_shelftask")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_storage_shelftask对象", description="仓库管理—上架/下架任务")
public class MesStorageShelftask implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**上架任务ID*/
	@Excel(name = "上架任务ID", width = 15)
    @ApiModelProperty(value = "上架任务ID")
    private java.lang.String upshelfTaskid;
	/**上架任务行号*/
	@Excel(name = "上架任务行号", width = 15)
    @ApiModelProperty(value = "上架任务行号")
    private java.lang.String upshelfRownum;
	/**状态*/
	@Excel(name = "状态", width = 15)
    @ApiModelProperty(value = "状态")
    private java.lang.String state;
	/**基本行号*/
	@Excel(name = "基本行号", width = 15)
    @ApiModelProperty(value = "基本行号")
    private java.lang.String baseRownum;
	/**基本单号*/
	@Excel(name = "基本单号", width = 15)
    @ApiModelProperty(value = "基本单号")
    private java.lang.String baseCode;
	/**基本单据类型*/
	@Excel(name = "基本单据类型", width = 15)
    @ApiModelProperty(value = "基本单据类型")
    private java.lang.String baseDockettype;
	/**产品编号*/
	@Excel(name = "产品编号", width = 15)
    @ApiModelProperty(value = "产品编号")
    private java.lang.String productCode;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private java.lang.String productName;
	/**仓库编号*/
	@Excel(name = "仓库编号", width = 15)
    @ApiModelProperty(value = "仓库编号")
    private java.lang.String wareCode;
	/**货主编码*/
	@Excel(name = "货主编码", width = 15)
    @ApiModelProperty(value = "货主编码")
    private java.lang.String shipperCode;
	/**批次号*/
	@Excel(name = "批次号", width = 15)
    @ApiModelProperty(value = "批次号")
    private java.lang.String wholeNum;
	/**源库位编码*/
	@Excel(name = "源库位编码", width = 15)
    @ApiModelProperty(value = "源库位编码")
    private java.lang.String resourceSitecode;
	/**源跟踪号*/
	@Excel(name = "源跟踪号", width = 15)
    @ApiModelProperty(value = "源跟踪号")
    private java.lang.String resourceNum;
	/**目标跟踪号*/
	@Excel(name = "目标跟踪号", width = 15)
    @ApiModelProperty(value = "目标跟踪号")
    private java.lang.String targetNum;
	/**推荐库位*/
	@Excel(name = "推荐库位", width = 15)
    @ApiModelProperty(value = "推荐库位")
    private java.lang.String recommendWaresite;
	/**包装编码*/
	@Excel(name = "包装编码", width = 15)
    @ApiModelProperty(value = "包装编码")
    private java.lang.String parcelCode;
	/**包装单位*/
	@Excel(name = "包装单位", width = 15)
    @ApiModelProperty(value = "包装单位")
    private java.lang.String parcelUnit;
	/**上架包装数量*/
	@Excel(name = "上架包装数量", width = 15)
    @ApiModelProperty(value = "上架包装数量")
    private java.lang.String upshelfParcelnum;
	/**上架数量*/
	@Excel(name = "上架数量", width = 15)
    @ApiModelProperty(value = "上架数量")
    private java.lang.String upshelfNum;
	/**上架操作人*/
	@Excel(name = "上架操作人", width = 15)
    @ApiModelProperty(value = "上架操作人")
    private java.lang.String upshelfOperator;
	/**上架时间*/
	@Excel(name = "上架时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "上架时间")
    private java.util.Date upshelfTime;
	/**打印次数*/
	@Excel(name = "打印次数", width = 15)
    @ApiModelProperty(value = "打印次数")
    private java.lang.String printNum;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
