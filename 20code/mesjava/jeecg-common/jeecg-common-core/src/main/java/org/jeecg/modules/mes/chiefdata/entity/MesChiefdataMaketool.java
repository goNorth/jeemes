package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—制具信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_maketool")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_maketool对象", description="主数据—制具信息")
public class MesChiefdataMaketool implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**新增方式*/
	@Excel(name = "新增方式", width = 15, dicCode = "gain_manner")
	@Dict(dicCode = "gain_manner")
    @ApiModelProperty(value = "新增方式")
    private java.lang.String gainManner;
	/**制具SN*/
	@Excel(name = "制具SN", width = 15)
    @ApiModelProperty(value = "制具SN")
    private java.lang.String maketoolSn;
	/**批次数量*/
	@Excel(name = "批次数量", width = 15)
    @ApiModelProperty(value = "批次数量")
    private java.lang.String batchNum;
	/**制具料号*/
	@Excel(name = "制具料号", width = 15)
    @ApiModelProperty(value = "制具料号")
    private java.lang.String maketoolCode;
	/**入库单号*/
	@Excel(name = "入库单号", width = 15)
    @ApiModelProperty(value = "入库单号")
    private java.lang.String inputwareCode;
	/**供应商*/
	@Excel(name = "供应商", width = 15)
    @ApiModelProperty(value = "供应商")
    private java.lang.String supplier;
	/**制具类型*/
	@Excel(name = "制具类型", width = 15)
    @ApiModelProperty(value = "制具类型")
    private java.lang.String maketoolType;
	/**制具名称*/
	@Excel(name = "制具名称", width = 15)
    @ApiModelProperty(value = "制具名称")
    private java.lang.String maketoolName;
	/**制具规格*/
	@Excel(name = "制具规格", width = 15)
    @ApiModelProperty(value = "制具规格")
    private java.lang.String maketoolGague;
	/**最大使用次数*/
	@Excel(name = "最大使用次数", width = 15)
    @ApiModelProperty(value = "最大使用次数")
    private java.lang.String maxUsednum;
	/**保养次数*/
	@Excel(name = "保养次数", width = 15)
    @ApiModelProperty(value = "保养次数")
    private java.lang.String maintainNum;
	/**保养提醒次数*/
	@Excel(name = "保养提醒次数", width = 15)
    @ApiModelProperty(value = "保养提醒次数")
    private java.lang.String remindNum;
	/**周期上限*/
	@Excel(name = "周期上限", width = 15)
    @ApiModelProperty(value = "周期上限")
    private java.lang.String cycleLimit;
	/**保养周期*/
	@Excel(name = "保养周期", width = 15)
    @ApiModelProperty(value = "保养周期")
    private java.lang.String maintainCycle;
	/**保养提醒天数*/
	@Excel(name = "保养提醒天数", width = 15)
    @ApiModelProperty(value = "保养提醒天数")
    private java.lang.String remindDays;
	/**累积使用次数*/
	@Excel(name = "累积使用次数", width = 15)
    @ApiModelProperty(value = "累积使用次数")
    private java.lang.String rackupNum;
	/**左上张力*/
	@Excel(name = "左上张力", width = 15)
    @ApiModelProperty(value = "左上张力")
    private java.lang.String leftupPower;
	/**右上张力*/
	@Excel(name = "右上张力", width = 15)
    @ApiModelProperty(value = "右上张力")
    private java.lang.String rightupPower;
	/**中间张力*/
	@Excel(name = "中间张力", width = 15)
    @ApiModelProperty(value = "中间张力")
    private java.lang.String middlePower;
	/**左下张力*/
	@Excel(name = "左下张力", width = 15)
    @ApiModelProperty(value = "左下张力")
    private java.lang.String leftdownPower;
	/**右下张力*/
	@Excel(name = "右下张力", width = 15)
    @ApiModelProperty(value = "右下张力")
    private java.lang.String rightdownPower;
	/**张力差异*/
	@Excel(name = "张力差异", width = 15)
    @ApiModelProperty(value = "张力差异")
    private java.lang.String powerGap;
	/**厚度*/
	@Excel(name = "厚度", width = 15)
    @ApiModelProperty(value = "厚度")
    private java.lang.String thickDegree;
	/**平整度*/
	@Excel(name = "平整度", width = 15)
    @ApiModelProperty(value = "平整度")
    private java.lang.String flattenDegree;
	/**库位*/
	@Excel(name = "库位", width = 15)
    @ApiModelProperty(value = "库位")
    private java.lang.String wareSite;
	/**RoHs标志*/
	@Excel(name = "RoHs标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "RoHs标志")
    private java.lang.String rohsToken;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
