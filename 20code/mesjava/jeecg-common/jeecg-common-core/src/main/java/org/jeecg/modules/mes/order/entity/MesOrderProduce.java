package org.jeecg.modules.mes.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 订单管理—生产订单
 * @Author: jeecg-boot
 * @Date:   2020-11-06
 * @Version: V1.0
 */
@Data
@TableName("mes_order_produce")
@ApiModel(value="mes_order_produce对象", description="订单管理—生产订单")
public class MesOrderProduce implements Serializable {
    private static final long serialVersionUID = 1L;

    /**id*/
    @TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
    /**创建日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
    /**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
    /**更新日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
    /**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
    /**订单编号*/
    @Excel(name = "订单编号", width = 15)
    @ApiModelProperty(value = "订单编号")
    private java.lang.String orderCode;
    /**订单名称*/
    @Excel(name = "订单名称", width = 15)
    @ApiModelProperty(value = "订单名称")
    private java.lang.String orderName;
    /**订单类型*/
    @Excel(name = "订单类型", width = 15)
    @ApiModelProperty(value = "订单类型")
    private java.lang.String orderType;
    /**物料id*/
    @Excel(name = "物料id", width = 15)
    @ApiModelProperty(value = "物料id")
    private java.lang.String materielId;
    /**物料编号*/
    @Excel(name = "物料编号", width = 15)
    @ApiModelProperty(value = "物料编号")
    private java.lang.String materielCode;
    /**物料名称*/
    @Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materielName;
    /**工厂id*/
    @Excel(name = "工厂id", width = 15)
    @ApiModelProperty(value = "工厂id")
    private java.lang.String factoryId;
    /**
     * 工厂编号
     */
    @Excel(name = "工厂编号", width = 15)
    @ApiModelProperty(value = "工厂编号")
    private java.lang.String factoryCode;
    /**
     * 工厂名称
     */
    @Excel(name = "工厂名称", width = 15)
    @ApiModelProperty(value = "工厂名称")
    private java.lang.String factoryName;
    /**
     * 状态  缺料、领料完成、
     */
    @Excel(name = "状态", width = 15, dicCode = "produce_state")
    @Dict(dicCode = "produce_state")
    private java.lang.String state;
    /**
     * 总数量
     */
    @Excel(name = "总数量", width = 15)
    @ApiModelProperty(value = "总数量")
    private java.lang.String grossAccount;
    /**
     * 入库数
     */
    @Excel(name = "入库数", width = 15)
    @ApiModelProperty(value = "入库数")
    private java.lang.String inputNum;
    /**剩余数*/
    @Excel(name = "剩余数", width = 15)
    @ApiModelProperty(value = "剩余数")
    private java.lang.String remainNum;
    /**单位*/
    @Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
    /**已交货*/
    @Excel(name = "已交货", width = 15)
    @ApiModelProperty(value = "已交货")
    private java.lang.String haveDelivery;
    /**基本结束日期*/
    @Excel(name = "基本结束日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "基本结束日期")
    private java.util.Date baseEnd;
    /**计划结束日期*/
    @Excel(name = "计划结束日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "计划结束日期")
    private java.util.Date planEnd;
    /**确认结束日期*/
    @Excel(name = "确认结束日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "确认结束日期")
    private java.util.Date confirmEnd;
    /**基本开始日期*/
    @Excel(name = "基本开始日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "基本开始日期")
    private java.util.Date baseStart;
    /**计划开始日期*/
    @Excel(name = "计划开始日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "计划开始日期")
    private java.util.Date planStart;
    /**确认开始日期*/
    @Excel(name = "确认开始日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "确认开始日期")
    private java.util.Date confirmStart;
    /**计划下达日期*/
    @Excel(name = "计划下达日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "计划下达日期")
    private java.util.Date planDownreach;
    /**确认下达日期*/
    @Excel(name = "确认下达日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "确认下达日期")
    private java.util.Date confirmDownreach;
    /**销售订单id*/
    @Excel(name = "销售订单id", width = 15)
    @ApiModelProperty(value = "销售订单id")
    private java.lang.String saleorderId;
    /**销售订单编号*/
    @Excel(name = "销售订单编号", width = 15)
    @ApiModelProperty(value = "销售订单编号")
    private java.lang.String saleorderCode;
    /**销售订单名称*/
    @Excel(name = "销售订单名称", width = 15)
    @ApiModelProperty(value = "销售订单名称")
    private java.lang.String saleorderName;
    /**MRP管理员id*/
    @Excel(name = "MRP管理员id", width = 15)
    @ApiModelProperty(value = "MRP管理员id")
    private java.lang.String mrpadminId;
    /**MRP管理员编号*/
    @Excel(name = "MRP管理员编号", width = 15)
    @ApiModelProperty(value = "MRP管理员编号")
    private java.lang.String mrpadminCode;
    /**MRP管理员名称*/
    @Excel(name = "MRP管理员名称", width = 15)
    @ApiModelProperty(value = "MRP管理员名称")
    private java.lang.String mrpadminName;
    /**生产管理员id*/
    @Excel(name = "生产管理员id", width = 15)
    @ApiModelProperty(value = "生产管理员id")
    private java.lang.String proadminId;
    /**生产管理员编号*/
    @Excel(name = "生产管理员编号", width = 15)
    @ApiModelProperty(value = "生产管理员编号")
    private java.lang.String proadminCode;
    /**生产管理员名称*/
    @Excel(name = "生产管理员名称", width = 15)
    @ApiModelProperty(value = "生产管理员名称")
    private java.lang.String proadminName;
    /**规格*/
    @Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private java.lang.String gauge;
    /**备注*/
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
    /**库存地点*/
    @Excel(name = "库存地点", width = 15)
    @ApiModelProperty(value = "库存地点")
    private java.lang.String storageSite;
    /**备用4*/
    @Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
    /**备用5*/
    @Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
    /**备用6*/
    @Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
    /**客户*/
    @Excel(name = "客户", width = 15)
    @ApiModelProperty(value = "客户")
    private java.lang.String client;
    /**线别*/
    @Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineSort;
    /**生产阶别*/
    @Excel(name = "生产阶别", width = 15, dicCode = "produce_grade")
    @Dict(dicCode = "produce_grade")
    @ApiModelProperty(value = "生产阶别")
    private java.lang.String produceGrade;
    /**上料提醒*/
    @Excel(name = "上料提醒", width = 15, dicCode = "glaze_remind")
    @Dict(dicCode = "glaze_remind")
    @ApiModelProperty(value = "上料提醒")
    private java.lang.String glazeRemind;
    /**打印时间*/
    @TableField(exist = false)
    private java.lang.String printTime ;

    @TableField(exist = false)
    private List<MesProduceItem> produceItems;
}
