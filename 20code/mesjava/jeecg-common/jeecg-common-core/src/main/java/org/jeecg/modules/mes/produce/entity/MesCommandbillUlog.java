package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 制令单上料记录
 * @Author: jeecg-boot
 * @Date: 2021-03-04
 * @Version: V1.0
 */
@Data
@TableName("mes_commandbill_ulog")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "mes_commandbill_ulog对象", description = "制令单上料记录")
public class MesCommandbillUlog implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 制令单bom编号
     */
    @Excel(name = "制令单bom编号", width = 15)
    @ApiModelProperty(value = "制令单bom编号")
    private java.lang.String pitemId;
    /**
     * 上料数量
     */
    @Excel(name = "上料数量", width = 15)
    @ApiModelProperty(value = "上料数量")
    private java.math.BigDecimal materielNum;
    /**
     * 物料名称
     */
    @Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materielName;
    /**
     * 上料类型
     */
    @Excel(name = "上料类型", width = 15)
    @ApiModelProperty(value = "上料类型")
    private java.lang.String materielType;
    /**
     * 制令单id
     */
    @Excel(name = "制令单id", width = 15)
    @ApiModelProperty(value = "制令单id")
    private java.lang.String commandbillId;
    /**
     * 制令单号
     */
    @Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String commandbillCode;
    /**
     * 上料者
     */
    @ApiModelProperty(value = "上料者")
    private java.lang.String createBy;
    /**
     * 上料时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "上料时间")
    private java.util.Date createTime;
}
