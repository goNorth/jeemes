package org.jeecg.modules.mes.inspect.vo;

import java.util.List;
import org.jeecg.modules.mes.inspect.entity.MesIpqcDocket;
import org.jeecg.modules.mes.inspect.entity.MesIpqcdocketItem;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 质检中心-IPQC单据
 * @Author: jeecg-boot
 * @Date:   2020-10-23
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_ipqc_docketPage对象", description="质检中心-IPQC单据")
public class MesIpqcDocketPage {

	/**id*/
	@ApiModelProperty(value = "id")
	private String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private String sysOrgCode;
	/**IPQC编号*/
	@Excel(name = "IPQC编号", width = 15)
	@ApiModelProperty(value = "IPQC编号")
	private String ipqcCode;
	/**记录人*/
	@Excel(name = "记录人", width = 15)
	@ApiModelProperty(value = "记录人")
	private String recordPerson;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private String query6;

	@ExcelCollection(name="单据明细")
	@ApiModelProperty(value = "单据明细")
	private List<MesIpqcdocketItem> mesIpqcdocketItemList;

}
