package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 物料凭证项目
 * @Author: jeecg-boot
 * @Date:   2020-10-12
 * @Version: V1.0
 */
@Data
@TableName("mes_certificate_item")
@ApiModel(value="mes_certificate_perk对象", description="物料凭证抬头")
public class MesCertificateItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**移动编号*/
	@Excel(name = "移动编号", width = 15)
	@ApiModelProperty(value = "移动编号")
	private java.lang.String mobileCode;
	/**移动类型*/
	@Excel(name = "移动类型", width = 15)
	@ApiModelProperty(value = "移动类型")
	private java.lang.String mobileType;
	/**物料编号*/
	@Excel(name = "物料编号", width = 15)
	@ApiModelProperty(value = "物料编号")
	private java.lang.String materielCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
	@ApiModelProperty(value = "物料名称")
	private java.lang.String materielName;
	/**录入数量*/
	@Excel(name = "录入数量", width = 15)
	@ApiModelProperty(value = "录入数量")
	private java.lang.String inputNum;
	/**录入项单位*/
	@Excel(name = "录入项单位", width = 15)
	@ApiModelProperty(value = "录入项单位")
	private java.lang.String inputUnit;
	/**工厂*/
	@Excel(name = "工厂", width = 15)
	@ApiModelProperty(value = "工厂")
	private java.lang.String factory;
	/**存储位置*/
	@Excel(name = "存储位置", width = 15)
	@ApiModelProperty(value = "存储位置")
	private java.lang.String storageSite;
	/**预留编号*/
	@Excel(name = "预留编号", width = 15)
	@ApiModelProperty(value = "预留编号")
	private java.lang.String reserveCode;
	/**抬头id*/
	@ApiModelProperty(value = "抬头id")
	private java.lang.String perkId;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
