package org.jeecg.modules.mes.storage.entity;

public class MesShelfData {
    private java.lang.String cell_num;
    private java.lang.Integer index;
    private java.lang.String color;
    private java.lang.Integer blink;

    public String getCell_num() {
        return cell_num;
    }

    public void setCell_num(String cell_num) {
        this.cell_num = cell_num;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getBlink() {
        return blink;
    }

    public void setBlink(Integer blink) {
        this.blink = blink;
    }
}
