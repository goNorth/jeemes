package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 制造中心-SPI测试信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Data
@TableName("mes_spitest_info")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_spitest_info对象", description="制造中心-SPI测试信息")
public class MesSpitestInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**测试结果*/
	@Excel(name = "测试结果", width = 15)
    @ApiModelProperty(value = "测试结果")
    private java.lang.String testResult;
	/**程序名*/
	@Excel(name = "程序名", width = 15)
    @ApiModelProperty(value = "程序名")
    private java.lang.String procedureName;
	/**操作人*/
	@Excel(name = "操作人", width = 15)
    @ApiModelProperty(value = "操作人")
    private java.lang.String operator;
	/**不良数量*/
	@Excel(name = "不良数量", width = 15)
    @ApiModelProperty(value = "不良数量")
    private java.lang.String badNum;
	/**机器名称*/
	@Excel(name = "机器名称", width = 15)
    @ApiModelProperty(value = "机器名称")
    private java.lang.String machineName;
	/**测试时间*/
	@Excel(name = "测试时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "测试时间")
    private java.util.Date testTime;
	/**产品SN*/
	@Excel(name = "产品SN", width = 15)
    @ApiModelProperty(value = "产品SN")
    private java.lang.String productSn;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**制令单号*/
	@Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String commandBill;
	/**焊点数*/
	@Excel(name = "焊点数", width = 15)
    @ApiModelProperty(value = "焊点数")
    private java.lang.String weldPointnum;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
