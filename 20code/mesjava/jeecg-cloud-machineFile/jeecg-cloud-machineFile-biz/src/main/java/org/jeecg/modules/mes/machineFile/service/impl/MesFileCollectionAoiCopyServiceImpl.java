package org.jeecg.modules.mes.machineFile.service.impl;

import org.jeecg.modules.mes.machineFile.mapper.MesFileCollectionAoiCopyMapper;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionAoiCopyService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoiCopy;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: mes_file_collection_aoi_copy
 * @Author: jeecg-boot
 * @Date:   2021-06-01
 * @Version: V1.0
 */
@Service
public class MesFileCollectionAoiCopyServiceImpl extends ServiceImpl<MesFileCollectionAoiCopyMapper, MesFileCollectionAoiCopy> implements IMesFileCollectionAoiCopyService {

}
