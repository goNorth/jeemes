package org.jeecg.modules.mes.order.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.order.entity.ImpApplyBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 进口申请单
 * @Author: jeecg-boot
 * @Date:   2021-04-06
 * @Version: V1.0
 */
public interface ImpApplyBillMapper extends BaseMapper<ImpApplyBill> {
    public boolean deleteByMainId(@Param("mainId") String mainId);

    public List<ImpApplyBill> selectByMainId(@Param("mainId") String mainId);
}
