package org.jeecg.modules.mes.order.controller;

import cn.hutool.core.util.IdUtil;
import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.order.entity.ImpApplyBill;
import org.jeecg.modules.mes.order.entity.ImpApplyMain;
import org.jeecg.modules.mes.order.service.IImpApplyMainService;
import org.jeecg.modules.mes.order.service.IImpApplyBillService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 进口申请单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
@Api(tags="进口申请单主表")
@RestController
@RequestMapping("/order/impApplyMain")
@Slf4j
public class ImpApplyMainController extends JeecgController<ImpApplyMain, IImpApplyMainService> {

	@Autowired
	private IImpApplyMainService impApplyMainService;

	@Autowired
	private IImpApplyBillService impApplyBillService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param impApplyMain
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "进口申请单主表-分页列表查询")
	@ApiOperation(value="进口申请单主表-分页列表查询", notes="进口申请单主表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ImpApplyMain impApplyMain,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ImpApplyMain> queryWrapper = QueryGenerator.initQueryWrapper(impApplyMain, req.getParameterMap());
		Page<ImpApplyMain> page = new Page<ImpApplyMain>(pageNo, pageSize);
		IPage<ImpApplyMain> pageList = impApplyMainService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
     *   添加
     * @param impApplyMain
     * @return
     */
    @AutoLog(value = "进口申请单主表-添加")
    @ApiOperation(value="进口申请单主表-添加", notes="进口申请单主表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ImpApplyMain impApplyMain) {
        impApplyMainService.save(impApplyMain);
        return Result.ok("添加成功！");
    }

    /**
     *  编辑
     * @param impApplyMain
     * @return
     */
    @AutoLog(value = "进口申请单主表-编辑")
    @ApiOperation(value="进口申请单主表-编辑", notes="进口申请单主表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ImpApplyMain impApplyMain) {
        impApplyMainService.updateById(impApplyMain);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "进口申请单主表-通过id删除")
    @ApiOperation(value="进口申请单主表-通过id删除", notes="进口申请单主表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        impApplyMainService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "进口申请单主表-批量删除")
    @ApiOperation(value="进口申请单主表-批量删除", notes="进口申请单主表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.impApplyMainService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ImpApplyMain impApplyMain) {
        return super.exportXls(request, impApplyMain, ImpApplyMain.class, "进口申请单主表");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ImpApplyMain.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/
	

    /*--------------------------------子表处理-进口申请单子表-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "进口申请单子表-通过主表ID查询")
	@ApiOperation(value="进口申请单子表-通过主表ID查询", notes="进口申请单子表-通过主表ID查询")
	@GetMapping(value = "/listImpApplyBillByMainId")
    public Result<?> listImpApplyBillByMainId(ImpApplyBill impApplyBill,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<ImpApplyBill> queryWrapper = QueryGenerator.initQueryWrapper(impApplyBill, req.getParameterMap());
        Page<ImpApplyBill> page = new Page<ImpApplyBill>(pageNo, pageSize);
        IPage<ImpApplyBill> pageList = impApplyBillService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param impApplyBill
	 * @return
	 */
	@AutoLog(value = "进口申请单子表-添加")
	@ApiOperation(value="进口申请单子表-添加", notes="进口申请单子表-添加")
	@PostMapping(value = "/addImpApplyBill")
	public Result<?> addImpApplyBill(@RequestBody ImpApplyBill impApplyBill) {
		ImpApplyMain main = impApplyMainService.getById(impApplyBill.getImpMainId());
		impApplyBill.setInvoiceNo(main.getOrderCode());
		impApplyBill.setCurr(main.getCurrency());
		impApplyBill.setCompanyCode("500666002U");
		impApplyBill.setCompanyMergerKey("");
		impApplyBill.setGwId(IdUtil.simpleUUID());
		impApplyBill.setOptLock(0);
		impApplyBill.setImpApplyState("未上传");
		impApplyBillService.save(impApplyBill);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param impApplyBill
	 * @return
	 */
	@AutoLog(value = "进口申请单子表-编辑")
	@ApiOperation(value="进口申请单子表-编辑", notes="进口申请单子表-编辑")
	@PutMapping(value = "/editImpApplyBill")
	public Result<?> editImpApplyBill(@RequestBody ImpApplyBill impApplyBill) {
		impApplyBillService.updateById(impApplyBill);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "进口申请单子表-通过id删除")
	@ApiOperation(value="进口申请单子表-通过id删除", notes="进口申请单子表-通过id删除")
	@DeleteMapping(value = "/deleteImpApplyBill")
	public Result<?> deleteImpApplyBill(@RequestParam(name="id",required=true) String id) {
		impApplyBillService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "进口申请单子表-批量删除")
	@ApiOperation(value="进口申请单子表-批量删除", notes="进口申请单子表-批量删除")
	@DeleteMapping(value = "/deleteBatchImpApplyBill")
	public Result<?> deleteBatchImpApplyBill(@RequestParam(name="ids",required=true) String ids) {
	    this.impApplyBillService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportImpApplyBill")
    public ModelAndView exportImpApplyBill(HttpServletRequest request, ImpApplyBill impApplyBill) {
		 // Step.1 组装查询条件
		 QueryWrapper<ImpApplyBill> queryWrapper = QueryGenerator.initQueryWrapper(impApplyBill, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<ImpApplyBill> pageList = impApplyBillService.list(queryWrapper);
		 List<ImpApplyBill> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "进口申请单子表"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, ImpApplyBill.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("进口申请单子表报表", "导出人:" + sysUser.getRealname(), "进口申请单子表"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importImpApplyBill/{mainId}")
    public Result<?> importImpApplyBill(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<ImpApplyBill> list = ExcelImportUtil.importExcel(file.getInputStream(), ImpApplyBill.class, params);
				 for (ImpApplyBill temp : list) {
                    temp.setImpMainId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 impApplyBillService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-进口申请单子表-end----------------------------------------------*/

	 /*--------------------------------上传关务-进口申请单子表-begin----------------------------------------------*/

	 @AutoLog(value = "进口申请单-上传关务")
	 @ApiOperation(value="进口申请单-上传关务", notes="根据进口申请单主表id获取子表信息，调用上传关务系统")
	 @GetMapping(value = "/insertSqlServer")
	 public Result<?> importPurchaseSqlServer(@RequestParam(name="id",required=true) String id) {
		 ImpApplyMain impmain = impApplyMainService.getById(id);
		 if(impmain!=null){
		 	if("已上传".equals(impmain.getState())){
				return Result.error("该进口申请单已上传关务，请重新创建新的申请单！");
			}
		 	//上传关务系统
			 impApplyMainService.insertSqlServer(impmain);
		 }else {
			 Result.error("未找到该进口申请单");
		 }

		 return Result.OK("上传完成");
	 }

	 /*--------------------------------上传关务-进口申请单子表-end----------------------------------------------*/


}
