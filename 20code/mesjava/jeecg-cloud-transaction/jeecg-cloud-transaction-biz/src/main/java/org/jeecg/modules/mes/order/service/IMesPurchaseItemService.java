package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.MesPurchaseItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 订单管理—采购订单子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesPurchaseItemService extends IService<MesPurchaseItem> {

	public List<MesPurchaseItem> selectByMainId(String mainId);
	/**
	 * 更新采购订单子表未收货数量
	 * @param id
	 * @return
	 */
	public boolean uploadUnreceiveNum(String id);
}
