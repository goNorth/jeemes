package org.jeecg.modules.mes.order.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.jeecg.modules.mes.order.entity.MesProduceItem;

import java.io.Serializable;
import java.util.Collection;

/**
 * @Description: 订单管理—生产订单
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesOrderProduceService extends IService<MesOrderProduce> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 新增
	 * @param mesOrderProduce
	 * @return
	 */
	public boolean add(MesOrderProduce mesOrderProduce);

	/**
	 * 库存充足，进行物料领用
	 * @param produceItem  生产订单子表
	 * @param mesOrderProduce 生产订单
	 * @return
	 */
	public boolean updateMaterielOccupy(MesProduceItem produceItem, MesOrderProduce mesOrderProduce);

    Result<?> queryPageList(Page<MesOrderProduce> page, QueryWrapper<MesOrderProduce> queryWrapper);
}
