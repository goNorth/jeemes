package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.MesProduceItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 订单管理—生产订单子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesProduceItemService extends IService<MesProduceItem> {

	public List<MesProduceItem> selectByMainId(String mainId);
}
