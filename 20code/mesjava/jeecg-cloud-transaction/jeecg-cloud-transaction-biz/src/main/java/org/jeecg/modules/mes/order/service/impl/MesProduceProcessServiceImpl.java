package org.jeecg.modules.mes.order.service.impl;

import org.jeecg.modules.mes.order.entity.MesProduceProcess;
import org.jeecg.modules.mes.order.mapper.MesProduceProcessMapper;
import org.jeecg.modules.mes.order.service.IMesProduceProcessService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 订单管理—生产订单工序子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesProduceProcessServiceImpl extends ServiceImpl<MesProduceProcessMapper, MesProduceProcess> implements IMesProduceProcessService {
	
	@Autowired
	private MesProduceProcessMapper mesProduceProcessMapper;
	
	@Override
	public List<MesProduceProcess> selectByMainId(String mainId) {
		return mesProduceProcessMapper.selectByMainId(mainId);
	}
}
