package org.jeecg.modules.mes.order.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.order.entity.ExpApplyMain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 出口申请单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
public interface ExpApplyMainMapper extends BaseMapper<ExpApplyMain> {

}
