package org.jeecg.modules.mes.order.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.util.ApplyBillDBUtil;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.mes.order.entity.ImpApplyBill;
import org.jeecg.modules.mes.order.entity.ImpApplyMain;
import org.jeecg.modules.mes.order.mapper.ImpApplyBillMapper;
import org.jeecg.modules.mes.order.mapper.ImpApplyMainMapper;
import org.jeecg.modules.mes.order.service.IImpApplyMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 进口申请单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
@Service
public class ImpApplyMainServiceImpl extends ServiceImpl<ImpApplyMainMapper, ImpApplyMain> implements IImpApplyMainService {

	@Autowired
	private ImpApplyMainMapper impApplyMainMapper;
	@Autowired
	private ImpApplyBillMapper impApplyBillMapper;

	@Override
	@Transactional
	public void delMain(String id) {
		impApplyBillMapper.deleteByMainId(id);
		impApplyMainMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			impApplyBillMapper.deleteByMainId(id.toString());
			impApplyMainMapper.deleteById(id);
		}
	}

	@Override
	@Transactional
	public void insertSqlServer(ImpApplyMain impApplyMain) {

		List<ImpApplyBill> impApplyBills = impApplyBillMapper.selectByMainId(impApplyMain.getId());
		for (ImpApplyBill impApplyBill:impApplyBills) {
			if(StringUtils.isBlank(impApplyBill.getInvoiceNo())||StringUtils.isBlank(impApplyBill.getImgExgFlag())||
					StringUtils.isBlank(impApplyBill.getMaterial())||StringUtils.isBlank(impApplyBill.getSectionNo())){
				throw new RuntimeException("进货单号、物料标记、批次号、料号不能为空");
			}
			if(StringUtils.isBlank(impApplyBill.getCompanyMergerKey())){
				impApplyBill.setCompanyMergerKey("");
			}
			impApplyBill.setUploadDate(DateUtils.getDate());//上传时间
			if(StringUtils.isBlank(impApplyBill.getCompanyCode())){
				impApplyBill.setCompanyCode("500666002U");
			}
			if (StringUtils.isBlank(impApplyBill.getGwId())){
				impApplyBill.setGwId(IdUtil.simpleUUID());
			}
			if (impApplyBill.getPalletNum()==null){
				impApplyBill.setPalletNum(0);
			}
			if(impApplyBill.getOptLock()==null){
				impApplyBill.setOptLock(0);
			}
			if(StringUtils.isBlank(impApplyBill.getInvoiceDate())){
				impApplyBill.setInvoiceDate(DateUtils.date2Str(new SimpleDateFormat("yyyy/MM/dd")));
			}
			//如果数量未0，则不上传关务系统
			BigDecimal b = new BigDecimal(impApplyBill.getQty());
			if(!(b.compareTo(BigDecimal.ZERO)==0)){
				try{
					//关务系统能连接上的时候打开
					ApplyBillDBUtil.insertSqlServer(impApplyBill);
				}catch(Exception e){
					//仅仅捕捉 SQLException
					throw new RuntimeException("关务系统连接失败，请检查！！！"+e);
				}
				impApplyBill.setImpApplyState("已上传");
				impApplyBillMapper.updateById(impApplyBill);
			}else {
				throw new RuntimeException("料号为："+impApplyBill.getMaterial()+"的企业数量为零，请变更！！！");
			}
		}

		//修改状态为已上传
		impApplyMain.setState("已上传");
		impApplyMainMapper.updateById(impApplyMain);
	}
}
