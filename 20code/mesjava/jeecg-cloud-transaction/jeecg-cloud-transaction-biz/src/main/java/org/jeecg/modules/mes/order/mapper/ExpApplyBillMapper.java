package org.jeecg.modules.mes.order.mapper;

import java.util.List;
import org.jeecg.modules.mes.order.entity.ExpApplyBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 出口申请单
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
public interface ExpApplyBillMapper extends BaseMapper<ExpApplyBill> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<ExpApplyBill> selectByMainId(@Param("mainId") String mainId);

}
