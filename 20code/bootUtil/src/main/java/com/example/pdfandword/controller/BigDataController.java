package com.example.pdfandword.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO 大数据获取接口
 *
 * @author xuhow
 * @version 1.0.0
 * @date 2020/6/10 14:31
 */
@Controller
public class BigDataController {


    @ApiOperation(value="大数据查询接口", notes="所有接口数据一次性查询")
    @ResponseBody
    @PostMapping(value = "/bigdata/ms")
    public Result<?> bigData(@RequestBody conHead conHead, HttpServletResponse response, HttpServletRequest request) {
        List<bigDataRes>  resbig = new ArrayList<>();
        String name  = conHead.getConBy1();
        String id_number = conHead.getConBy2();
        String mobile  = conHead.getConBy3();
        String card  = conHead.getConBy4();
//        //法院通

        System.out.println("resbig="+resbig);
        return Result.ok(resbig);
    }
}
