package com.example.pdfandword.controller;


/**
 * TODO
 *
 * @author xuhow
 * @version 1.0.0
 * @date 2020/6/10 11:46
 */

public class fhBase {

        private boolean success;
        private String message;
        private int code;
        private String result;
        private long timestamp;
        public void setSuccess(boolean success) {
            this.success = success;
        }
        public boolean getSuccess() {
            return success;
        }

        public void setMessage(String message) {
            this.message = message;
        }
        public String getMessage() {
            return message;
        }

        public void setCode(int code) {
            this.code = code;
        }
        public int getCode() {
            return code;
        }

        public void setResult(String result) {
            this.result = result;
        }
        public String getResult() {
            return result;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }
        public long getTimestamp() {
            return timestamp;
        }
}
