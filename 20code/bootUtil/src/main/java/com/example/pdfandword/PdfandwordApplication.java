package com.example.pdfandword;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
//@EnableSwagger2
//@ComponentScan(basePackages = {"com.example.pdfandword"})
public class PdfandwordApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdfandwordApplication.class, args);
	}

}
